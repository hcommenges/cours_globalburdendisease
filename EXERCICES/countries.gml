<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     gml:id="aFeatureCollection"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ countries.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml/3.2">
  <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-55.997252745 -179.958548764</gml:lowerCorner><gml:upperCorner>83.61347077 179.9958605798</gml:upperCorner></gml:Envelope></gml:boundedBy>
                                                                                                                              
  <ogr:featureMember>
    <ogr:countries gml:id="countries.0">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>0.9983539399 8.2340380942</gml:lowerCorner><gml:upperCorner>3.8005714793 11.336341187</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.0"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.0.0"><gml:exterior><gml:LinearRing><gml:posList>2.165760396 11.322078491 0.999164937 11.336341187 0.9983539399 9.8043711812 1.181708075 9.3466903 2.341742255 9.7995711595 2.165760396 11.322078491</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.0.1"><gml:exterior><gml:LinearRing><gml:posList>3.8005714793 8.716263572 3.730741899 9.0540947754 3.1151094157 8.6959638551 3.3118323379 8.2340380942 3.8005714793 8.716263572</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GQ</ogr:ISO2>
      <ogr:ISO3>GNQ</ogr:ISO3>
      <ogr:NAMEen>Equatorial Guinea</ogr:NAMEen>
      <ogr:GBD_ID>172</ogr:GBD_ID>
      <ogr:GBD_NAME>Equatorial Guinea</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.1">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-8.6477270928 178.8209218136</gml:lowerCorner><gml:upperCorner>-7.7935851716 179.3306840882</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.1"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.1.0"><gml:exterior><gml:LinearRing><gml:posList>-8.3563170615 179.0838195273 -8.2617439936 179.1691805941 -8.4408104337 179.3104495364 -8.6477270928 179.3306840882 -8.646872982 179.2130708087 -8.3563170615 179.0838195273</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.1.1"><gml:exterior><gml:LinearRing><gml:posList>-7.9959168913 179.1780959084 -8.1843378625 178.9887678387 -8.0140406535 178.8209218136 -7.7935851716 179.0235435121 -7.9959168913 179.1780959084</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>TV</ogr:ISO2>
      <ogr:ISO3>TUV</ogr:ISO3>
      <ogr:NAMEen>Tuvalu</ogr:NAMEen>
      <ogr:GBD_ID>416</ogr:GBD_ID>
      <ogr:GBD_NAME>Tuvalu</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.2">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>3.1914339324 73.2933922534</gml:lowerCorner><gml:upperCorner>5.2409183546 73.8005501999</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.2"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.2.0"><gml:exterior><gml:LinearRing><gml:posList>4.2257058893 73.7071080231 4.1246443555 73.6958421474 4.048564614 73.5915375146 4.0513634912 73.4060566471 4.1331953919 73.3071616111 4.245199387 73.3256411766 4.3245631485 73.4553617467 4.3205949097 73.6090490593 4.2257058893 73.7071080231</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.2.1"><gml:exterior><gml:LinearRing><gml:posList>5.1886020614 73.411064328 5.2409183546 73.5911101793 5.1644569623 73.7877062354 5.0135625307 73.8005501999 4.8799965744 73.6532236937 4.9041353175 73.4344273044 5.0489788987 73.3562955002 5.1886020614 73.411064328</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.2.2"><gml:exterior><gml:LinearRing><gml:posList>3.5254680386 73.3720334941 3.5410412615 73.6027862706 3.4445902601 73.7180696098 3.2999247321 73.7643965528 3.2155423951 73.6774451452 3.1914339324 73.4739831569 3.2637603518 73.32398382 3.3722558772 73.2933922534 3.5254680386 73.3720334941</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MV</ogr:ISO2>
      <ogr:ISO3>MDV</ogr:ISO3>
      <ogr:NAMEen>Maldives</ogr:NAMEen>
      <ogr:GBD_ID>14</ogr:GBD_ID>
      <ogr:GBD_NAME>Maldives</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Seven seas (open ocean)</ogr:CONTINENT>
      <ogr:WB_REGION>South Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.3">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>42.3671623299 1.2877274765</gml:lowerCorner><gml:upperCorner>42.6567593274 1.7203321516</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.3"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.3.0"><gml:exterior><gml:LinearRing><gml:posList>42.4696252858 1.7203321516 42.3671623299 1.4489628094 42.5447354959 1.2877274765 42.6567593274 1.5608597184 42.4696252858 1.7203321516</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>AD</ogr:ISO2>
      <ogr:ISO3>AND</ogr:ISO3>
      <ogr:NAMEen>Andorra</ogr:NAMEen>
      <ogr:GBD_ID>74</ogr:GBD_ID>
      <ogr:GBD_NAME>Andorra</ogr:GBD_NAME>
      <ogr:WB_ECONOMY xsi:nil="true"/>
      <ogr:WB_GROUP xsi:nil="true"/>
      <ogr:CONTINENT xsi:nil="true"/>
      <ogr:WB_REGION xsi:nil="true"/>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.4">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>16.9872843463 -61.8810143982</gml:lowerCorner><gml:upperCorner>17.126532294 -61.672189908</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.4"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.4.0"><gml:exterior><gml:LinearRing><gml:posList>17.126532294 -61.773019986 17.057562567 -61.672189908 16.9872843463 -61.7082196496 17.007635809 -61.851714648 17.0932510638 -61.8810143982 17.126532294 -61.773019986</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>AG</ogr:ISO2>
      <ogr:ISO3>ATG</ogr:ISO3>
      <ogr:NAMEen>Antigua and Barbuda</ogr:NAMEen>
      <ogr:GBD_ID>105</ogr:GBD_ID>
      <ogr:GBD_NAME>Antigua and Barbuda</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.5">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>46.378643087 9.5086072213</gml:lowerCorner><gml:upperCorner>49.007914124 17.14833785</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.5"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.5.0"><gml:exterior><gml:LinearRing><gml:posList>48.604166158 16.945042766 48.005443014 17.14833785 46.862773743 16.094035278 46.378643087 14.540331665 46.519745586 13.6943058283 46.992998352 12.111177612 46.864427389 10.453811076 47.0620797231 10.0420878003 47.0581958757 9.6984700199 47.3038948207 9.5086072213 47.441565977 9.5394414547 47.5101991364 9.5545057867 47.544405597 9.7139058634 47.400725403 11.258826538 47.705823059 12.176599976 47.4904161682 12.9635225181 47.6693476239 13.0983360746 48.0994331201 12.9379600764 48.766430156 13.815724731 48.589541728 14.69567102 49.007914124 14.982061808 48.604166158 16.945042766</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>AT</ogr:ISO2>
      <ogr:ISO3>AUT</ogr:ISO3>
      <ogr:NAMEen>Austria</ogr:NAMEen>
      <ogr:GBD_ID>75</ogr:GBD_ID>
      <ogr:GBD_NAME>Austria</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.6">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>49.537752585 2.5217999277</gml:lowerCorner><gml:upperCorner>51.474326884 6.117486613</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.6"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.6.0"><gml:exterior><gml:LinearRing><gml:posList>50.749926656 5.994910116 50.120456035 6.117486613 49.537752585 5.790684855 49.97452179 4.139622843 51.0875408835 2.5217999277 51.4675394824 3.37610269 51.452446665 3.7902017123 51.4498969326 3.9028219865 51.474326884 5.012127727 50.749926656 5.994910116</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BE</ogr:ISO2>
      <ogr:ISO3>BEL</ogr:ISO3>
      <ogr:NAMEen>Belgium</ogr:NAMEen>
      <ogr:GBD_ID>76</ogr:GBD_ID>
      <ogr:GBD_NAME>Belgium</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.7">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>25.9153093664 50.3745202016</gml:lowerCorner><gml:upperCorner>26.6263870288 50.965465382</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.7"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.7.0"><gml:exterior><gml:LinearRing><gml:posList>26.5987895465 50.4157260151 26.6263870288 50.965465382 25.9153093664 50.7380839952 26.0348205275 50.3745202016 26.5987895465 50.4157260151</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BH</ogr:ISO2>
      <ogr:ISO3>BHR</ogr:ISO3>
      <ogr:NAMEen>Bahrain</ogr:NAMEen>
      <ogr:GBD_ID>140</ogr:GBD_ID>
      <ogr:GBD_NAME>Bahrain</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.8">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>23.9956434804 -78.4960090135</gml:lowerCorner><gml:upperCorner>25.2566411579 -76.6387718225</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.8"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.8.0"><gml:exterior><gml:LinearRing><gml:posList>25.2566411579 -78.1486516232 24.8909211918 -77.7512350653 24.5341297627 -77.5716100935 24.1741208273 -77.2886852325 23.9956434804 -77.3093210608 23.9958446033 -77.7281281543 24.3790932888 -77.9200278006 24.589514818 -78.2204458619 24.7821763782 -78.4960090135 24.9861697787 -78.2759704562 25.2544009626 -78.3905704186 25.2566411579 -78.1486516232</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.8.1"><gml:exterior><gml:LinearRing><gml:posList>25.1318411946 -77.252622429 25.2075614292 -77.0129133953 25.1583881283 -76.6628548564 25.0046634781 -76.6387718225 24.986554985 -77.0618383243 25.1318411946 -77.252622429</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BS</ogr:ISO2>
      <ogr:ISO3>BHS</ogr:ISO3>
      <ogr:NAMEen>Bahamas</ogr:NAMEen>
      <ogr:GBD_ID>106</ogr:GBD_ID>
      <ogr:GBD_NAME>Bahamas</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.9">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>13.0605164326 -59.6841793753</gml:lowerCorner><gml:upperCorner>13.3100980319 -59.2778915046</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.9"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.9.0"><gml:exterior><gml:LinearRing><gml:posList>13.0605164326 -59.5314659745 13.1797395072 -59.6841793753 13.3100980319 -59.6715791819 13.2743348943 -59.5554311936 13.1787747392 -59.4822268251 13.1563877512 -59.4096088843 13.1675049793 -59.2778915046 13.0719158841 -59.3610101398 13.0605164326 -59.5314659745</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BB</ogr:ISO2>
      <ogr:ISO3>BRB</ogr:ISO3>
      <ogr:NAMEen>Barbados</ogr:NAMEen>
      <ogr:GBD_ID>107</ogr:GBD_ID>
      <ogr:GBD_NAME>Barbados</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.10">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>45.826402893 6.131852661</gml:lowerCorner><gml:upperCorner>47.670424704 10.453811076</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.10"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.10.0"><gml:exterior><gml:LinearRing><gml:posList>47.5101991364 9.5545057867 47.441565977 9.5394414547 47.3038948207 9.5086072213 47.0423307452 9.4440495812 47.0581958757 9.6984700199 47.0620797231 10.0420878003 46.864427389 10.453811076 46.492047018 9.437852417 45.826402893 8.900004109 46.253611959 8.07307784 45.925259909 7.02208256 46.4122156441 6.7872818778 46.2947009103 6.2240956714 46.595606588 6.131852661 47.584618544 7.586028488 47.670424704 9.184506306 47.5101991364 9.5545057867</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CH</ogr:ISO2>
      <ogr:ISO3>CHE</ogr:ISO3>
      <ogr:NAMEen>Switzerland</ogr:NAMEen>
      <ogr:GBD_ID>94</ogr:GBD_ID>
      <ogr:GBD_NAME>Switzerland</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.11">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-12.1977641095 43.2230195978</gml:lowerCorner><gml:upperCorner>-11.324269202 43.8706158961</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.11"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.11.0"><gml:exterior><gml:LinearRing><gml:posList>-12.1977641095 43.8656231314 -12.0590408654 43.4525328031 -11.818047784 43.262868686 -11.351919314 43.2230195978 -11.324269202 43.5217316047 -11.9016345424 43.8706158961 -12.1977641095 43.8656231314</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>KM</ogr:ISO2>
      <ogr:ISO3>COM</ogr:ISO3>
      <ogr:NAMEen>Comoros</ogr:NAMEen>
      <ogr:GBD_ID>176</ogr:GBD_ID>
      <ogr:GBD_NAME>Comoros</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.12">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>14.9393678704 -24.2813631511</gml:lowerCorner><gml:upperCorner>15.8047217656 -22.8484817802</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.12"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.12.0"><gml:exterior><gml:LinearRing><gml:posList>15.8047217656 -23.8964371722 15.6812981018 -23.1309663858 15.306115519 -22.8484817802 14.9393678704 -23.3300065111 15.0404069874 -24.0804637262 15.4194551638 -24.2813631511 15.8047217656 -23.8964371722</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CV</ogr:ISO2>
      <ogr:ISO3>CPV</ogr:ISO3>
      <ogr:NAMEen>Cape Verde</ogr:NAMEen>
      <ogr:GBD_ID>203</ogr:GBD_ID>
      <ogr:GBD_NAME>Cabo Verde</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.13">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>47.400725403 5.928040812</gml:lowerCorner><gml:upperCorner>54.8963039111 14.8103927</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.13"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.13.0"><gml:exterior><gml:LinearRing><gml:posList>54.8104111271 9.4375030265 54.0795824274 11.0700348269 54.473578192 12.501963738 54.1549113173 14.2620399341 52.850651144 14.123922973 52.576921082 14.644821411 50.858447164 14.8103927 50.388760071 12.510223022 49.327842917 12.797750692 48.766430156 13.815724731 48.0994331201 12.9379600764 47.6693476239 13.0983360746 47.4904161682 12.9635225181 47.705823059 12.176599976 47.400725403 11.258826538 47.544405597 9.7139058634 47.5101991364 9.5545057867 47.670424704 9.184506306 47.584618544 7.586028488 48.958563131 8.200305216 49.455348652 6.345307071 49.7598236407 6.4248140639 50.120456035 6.117486613 50.749926656 5.994910116 51.806735535 5.928040812 52.230637309 7.026320027 53.245021877 7.194590691 53.666489976 7.226084832 53.546698309 8.565765821 53.892482815 9.007823113 54.8963039111 8.6608161325 54.8104111271 9.4375030265</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>DE</ogr:ISO2>
      <ogr:ISO3>DEU</ogr:ISO3>
      <ogr:NAMEen>Germany</ogr:NAMEen>
      <ogr:GBD_ID>81</ogr:GBD_ID>
      <ogr:GBD_NAME>Germany</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>1. Developed region: G7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.14">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>15.17265851 -61.5830272399</gml:lowerCorner><gml:upperCorner>15.6330996391 -61.179120309</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.14"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.14.0"><gml:exterior><gml:LinearRing><gml:posList>15.564426858 -61.4475382386 15.50779857 -61.267160611 15.3643293179 -61.1965095105 15.2372712206 -61.179120309 15.17265851 -61.316684508 15.3020802065 -61.4119071911 15.4731177653 -61.5463981908 15.6330996391 -61.5830272399 15.564426858 -61.4475382386</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>DM</ogr:ISO2>
      <ogr:ISO3>DMA</ogr:ISO3>
      <ogr:NAMEen>Dominica</ogr:NAMEen>
      <ogr:GBD_ID>110</ogr:GBD_ID>
      <ogr:GBD_NAME>Dominica</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.15">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>54.6920797189 8.122406446</gml:lowerCorner><gml:upperCorner>57.751166083 12.3715693938</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.15"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.15.0"><gml:exterior><gml:LinearRing><gml:posList>55.9876764718 11.9531588807 55.385210234 12.3715693938 54.9163290596 12.2378432215 54.6920797189 11.6856894522 55.023149927 11.5182746286 55.0928758009 11.0436839189 55.5725118676 10.6391784431 55.9589025985 11.1750292822 55.9876764718 11.9531588807</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.15.1"><gml:exterior><gml:LinearRing><gml:posList>54.8104111271 9.4375030265 54.8963039111 8.6608161325 56.551662502 8.122406446 57.077971536 8.8366045866 57.259875599 9.658851361 57.751166083 10.596039259 56.8828665156 10.4886184808 56.448431708 10.964121941 55.489447333 9.498057488 54.8104111271 9.4375030265</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>DK</ogr:ISO2>
      <ogr:ISO3>DNK</ogr:ISO3>
      <ogr:NAMEen>Denmark</ogr:NAMEen>
      <ogr:GBD_ID>78</ogr:GBD_ID>
      <ogr:GBD_NAME>Denmark</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.16">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>27.9698713432 -16.91605809</gml:lowerCorner><gml:upperCorner>43.763251044 3.5837858728</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.16"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.16.0"><gml:exterior><gml:LinearRing><gml:posList>28.5994090863 -16.134251017 28.3595267762 -15.9983074506 28.1377167349 -16.0740257709 27.9866866907 -16.2590633912 27.9698713432 -16.6078357292 28.1145620705 -16.8740965543 28.354969806 -16.91605809 28.5984608169 -16.8250979933 28.6927378259 -16.4762863347 28.5994090863 -16.134251017</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.16.1"><gml:exterior><gml:LinearRing><gml:posList>39.8811034162 3.2049087278 39.6713560432 3.5837858728 39.3450017866 3.12297625 39.6831839458 2.6683374338 39.8811034162 3.2049087278</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.16.2"><gml:exterior><gml:LinearRing><gml:posList>43.3860153322 -1.7940747449 42.685147604 -0.038933472 42.5447354959 1.2877274765 42.3671623299 1.4489628094 42.4696252858 1.7203321516 42.4314841477 3.1809696567 41.965521552 3.235687696 41.274603583 2.060313347 41.039699611 0.990407748 39.516221421 -0.323353645 38.73456452 0.227712436 36.736395575 -2.126698371 36.710679429 -4.433257616 36.1331436976 -5.3375052378 36.0362434776 -5.8874897447 36.959662177 -6.499582486 37.192816473 -7.414418098 38.022563985 -7.023985149 38.446362407 -7.359210164 41.01599884 -6.942491415 41.570280253 -6.205947225 41.92607249 -6.567630575 41.96898021 -8.750803189 42.922634182 -9.291981575 43.763251044 -7.880848762 43.568630276 -7.230091926 43.383612372 -4.385568814 43.3860153322 -1.7940747449</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>ES</ogr:ISO2>
      <ogr:ISO3>ESP</ogr:ISO3>
      <ogr:NAMEen>Spain</ogr:NAMEen>
      <ogr:GBD_ID>92</ogr:GBD_ID>
      <ogr:GBD_NAME>Spain</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.17">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>61.4205236687 -7.4136184021</gml:lowerCorner><gml:upperCorner>62.5697138837 -6.3573302841</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.17"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.17.0"><gml:exterior><gml:LinearRing><gml:posList>62.1384033079 -7.4136184021 62.5697138837 -6.5943513606 62.2845982211 -6.3573302841 61.4205236687 -6.7810694831 62.1384033079 -7.4136184021</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>FO</ogr:ISO2>
      <ogr:ISO3>FRO</ogr:ISO3>
      <ogr:NAMEen>Faeroe Islands</ogr:NAMEen>
      <ogr:GBD_ID>FRO</ogr:GBD_ID>
      <ogr:GBD_NAME>Faeroe Islands</ogr:GBD_NAME>
      <ogr:WB_ECONOMY xsi:nil="true"/>
      <ogr:WB_GROUP xsi:nil="true"/>
      <ogr:CONTINENT xsi:nil="true"/>
      <ogr:WB_REGION xsi:nil="true"/>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.18">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>6.722854945 158.0102137789</gml:lowerCorner><gml:upperCorner>7.1514821788 158.529157335</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.18"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.18.0"><gml:exterior><gml:LinearRing><gml:posList>6.9811867198 158.0626832156 7.1418931781 158.2002091787 7.1514821788 158.4830777524 6.9794476101 158.529157335 6.7711182331 158.4328636065 6.722854945 158.1635399875 6.8376663714 158.0102137789 6.9811867198 158.0626832156</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>FM</ogr:ISO2>
      <ogr:ISO3>FSM</ogr:ISO3>
      <ogr:NAMEen>Micronesia</ogr:NAMEen>
      <ogr:GBD_ID>25</ogr:GBD_ID>
      <ogr:GBD_NAME>Micronesia (Federated States of)</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.19">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>49.4149612993 -2.7560571432</gml:lowerCorner><gml:upperCorner>49.5394625335 -2.4852277052</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.19"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.19.0"><gml:exterior><gml:LinearRing><gml:posList>49.4149612993 -2.5116463957 49.425582906 -2.7560571432 49.4879485561 -2.6573888179 49.5394625335 -2.4852277052 49.4149612993 -2.5116463957</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GG</ogr:ISO2>
      <ogr:ISO3>GGY</ogr:ISO3>
      <ogr:NAMEen>Guernsey</ogr:NAMEen>
      <ogr:GBD_ID>GGY</ogr:GBD_ID>
      <ogr:GBD_NAME>Guernsey</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.20">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>12.0313327462 -61.749501106</gml:lowerCorner><gml:upperCorner>12.4661015425 -61.3087427047</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.20"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.20.0"><gml:exterior><gml:LinearRing><gml:posList>12.0313327462 -61.55917038 12.036322333 -61.749501106 12.236070054 -61.671457486 12.4661015425 -61.4869910208 12.4540144174 -61.3087427047 12.1981742257 -61.3991670798 12.0313327462 -61.55917038</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GD</ogr:ISO2>
      <ogr:ISO3>GRD</ogr:ISO3>
      <ogr:NAMEen>Grenada</ogr:NAMEen>
      <ogr:GBD_ID>112</ogr:GBD_ID>
      <ogr:GBD_NAME>Grenada</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.21">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>54.0027042272 -4.6047654877</gml:lowerCorner><gml:upperCorner>54.366115627 -4.311919726</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.21"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.21.0"><gml:exterior><gml:LinearRing><gml:posList>54.091050523 -4.6047654877 54.366115627 -4.530995246 54.287176825 -4.311919726 54.0027042272 -4.4615801169 54.091050523 -4.6047654877</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>IM</ogr:ISO2>
      <ogr:ISO3>IMN</ogr:ISO3>
      <ogr:NAMEen>Isle of Man</ogr:NAMEen>
      <ogr:GBD_ID>IMN</ogr:GBD_ID>
      <ogr:GBD_NAME>Isle of Man</ogr:GBD_NAME>
      <ogr:WB_ECONOMY xsi:nil="true"/>
      <ogr:WB_GROUP xsi:nil="true"/>
      <ogr:CONTINENT xsi:nil="true"/>
      <ogr:WB_REGION xsi:nil="true"/>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.22">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>36.659125067 6.602728312</gml:lowerCorner><gml:upperCorner>46.992998352 18.4229102281</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.22"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.22.0"><gml:exterior><gml:LinearRing><gml:posList>38.4224846781 15.6552396558 37.9916449482 15.6546240018 37.4035997858 15.1200772356 37.1685775633 15.3165266615 36.659125067 15.073578321 37.809271552 12.427419467 38.180975653 12.760915561 38.0534967361 14.0690498857 38.4224846781 15.6552396558</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.22.1"><gml:exterior><gml:LinearRing><gml:posList>41.2439368417 9.1189520583 41.2757415416 9.3621724236 40.5195987 9.826670769 39.139715887 9.557790561 38.877997137 8.853282097 39.23061758 8.371755405 39.9464422248 8.4333230546 40.91010163 8.23316491 41.2439368417 9.1189520583</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.22.2"><gml:exterior><gml:LinearRing><gml:posList>46.519745586 13.6943058283 46.2217184825 13.5331741824 45.9008391125 13.7097930591 45.593207098 13.7051558128 45.436224677 12.410329623 44.250677802 12.368825717 43.53131745 13.616953972 42.598822333 14.075368686 41.934271552 15.123383009 41.944077867 16.027110222 41.427801825 16.021657748 41.0583464742 17.2187345428 40.5105601531 18.4229102281 40.1374199424 18.2287518677 40.4607679669 17.2742811976 39.6432838471 16.7712422185 39.3484498289 17.4279990546 37.9377867618 16.5495649617 37.94920482 16.090017123 38.748277085 16.179209832 40.073187567 15.610199415 41.252386786 13.721853061 41.227525132 13.044606967 42.402736721 11.094493035 44.016750393 10.105804884 44.423976955 8.724782748 43.792222398 7.502289259 45.103449606 6.602728312 45.925259909 7.02208256 46.253611959 8.07307784 45.826402893 8.900004109 46.492047018 9.437852417 46.864427389 10.453811076 46.992998352 12.111177612 46.519745586 13.6943058283</gml:posList></gml:LinearRing></gml:exterior><gml:interior><gml:LinearRing><gml:posList>43.9120671524 12.3933641273 43.8846420415 12.4332877042 43.8944818867 12.4950334521 43.9376700816 12.5200114494 43.9799098218 12.5060500845 43.9827698536 12.4568522694 43.9536110312 12.4059826626 43.9120671524 12.3933641273</gml:posList></gml:LinearRing></gml:interior><gml:interior><gml:LinearRing><gml:posList>41.8697747775 12.3937276832 41.8641534942 12.5057550812 41.9411404084 12.5220233075 41.946317302 12.4101733491 41.8697747775 12.3937276832</gml:posList></gml:LinearRing></gml:interior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>IT</ogr:ISO2>
      <ogr:ISO3>ITA</ogr:ISO3>
      <ogr:NAMEen>Italy</ogr:NAMEen>
      <ogr:GBD_ID>86</ogr:GBD_ID>
      <ogr:GBD_NAME>Italy</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>1. Developed region: G7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.23">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>49.1451868387 -2.233631965</gml:lowerCorner><gml:upperCorner>49.258286851 -1.9485517522</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.23"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.23.0"><gml:exterior><gml:LinearRing><gml:posList>49.258286851 -2.121978319 49.221635227 -1.9485517522 49.1451868387 -1.9643268683 49.184515692 -2.233631965 49.258286851 -2.121978319</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>JE</ogr:ISO2>
      <ogr:ISO3>JEY</ogr:ISO3>
      <ogr:NAMEen>Jersey</ogr:NAMEen>
      <ogr:GBD_ID>JEY</ogr:GBD_ID>
      <ogr:GBD_NAME>Jersey</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.24">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>1.7568618106 -158.9586243373</gml:lowerCorner><gml:upperCorner>2.3083846942 -158.1490784501</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.24"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.24.0"><gml:exterior><gml:LinearRing><gml:posList>2.1981997994 -158.9586243373 2.3083846942 -158.7030330396 2.2767693504 -158.3307717147 2.0487082619 -158.1490784501 1.832543978 -158.3367460598 1.7568618106 -158.7143045205 1.9403985389 -158.9553225174 2.1981997994 -158.9586243373</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>KI</ogr:ISO2>
      <ogr:ISO3>KIR</ogr:ISO3>
      <ogr:NAMEen>Kiribati</ogr:NAMEen>
      <ogr:GBD_ID>23</ogr:GBD_ID>
      <ogr:GBD_NAME>Kiribati</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.25">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>17.0856303369 -62.8806497744</gml:lowerCorner><gml:upperCorner>17.4312594929 -62.495969836</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.25"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.25.0"><gml:exterior><gml:LinearRing><gml:posList>17.3403057386 -62.6683249718 17.1813919667 -62.495969836 17.0856303369 -62.5064786859 17.1073605367 -62.6227405958 17.2068590366 -62.6783191871 17.2874612181 -62.8358355218 17.3823110073 -62.8806497744 17.4312594929 -62.8319370543 17.3403057386 -62.6683249718</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>KN</ogr:ISO2>
      <ogr:ISO3>KNA</ogr:ISO3>
      <ogr:NAMEen>Saint Kitts and Nevis</ogr:NAMEen>
      <ogr:GBD_ID>393</ogr:GBD_ID>
      <ogr:GBD_NAME>Saint Kitts and Nevis</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.26">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>13.714667059 -61.078114387</gml:lowerCorner><gml:upperCorner>14.14263803 -60.7863400265</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.26"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.26.0"><gml:exterior><gml:LinearRing><gml:posList>13.714667059 -60.950103319 13.806301174 -61.078114387 14.0163491585 -61.0766232714 14.14263803 -60.8765103686 14.0150009148 -60.7863400265 13.756048721 -60.8039058234 13.714667059 -60.950103319</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>LC</ogr:ISO2>
      <ogr:ISO3>LCA</ogr:ISO3>
      <ogr:NAMEen>Saint Lucia</ogr:NAMEen>
      <ogr:GBD_ID>116</ogr:GBD_ID>
      <ogr:GBD_NAME>Saint Lucia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.27">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>47.0423307452 9.4440495812</gml:lowerCorner><gml:upperCorner>47.3038948207 9.6984700199</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.27"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.27.0"><gml:exterior><gml:LinearRing><gml:posList>47.0581958757 9.6984700199 47.0423307452 9.4440495812 47.3038948207 9.5086072213 47.0581958757 9.6984700199</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>LI</ogr:ISO2>
      <ogr:ISO3>LIE</ogr:ISO3>
      <ogr:NAMEen>Liechtenstein</ogr:NAMEen>
      <ogr:GBD_ID>LIE</ogr:GBD_ID>
      <ogr:GBD_NAME>Liechtenstein</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.28">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>49.455348652 5.790684855</gml:lowerCorner><gml:upperCorner>50.120456035 6.4248140639</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.28"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.28.0"><gml:exterior><gml:LinearRing><gml:posList>50.120456035 6.117486613 49.7598236407 6.4248140639 49.455348652 6.345307071 49.537752585 5.790684855 50.120456035 6.117486613</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>LU</ogr:ISO2>
      <ogr:ISO3>LUX</ogr:ISO3>
      <ogr:NAMEen>Luxembourg</ogr:NAMEen>
      <ogr:GBD_ID>87</ogr:GBD_ID>
      <ogr:GBD_NAME>Luxembourg</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.29">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>43.6545644152 7.1877585717</gml:lowerCorner><gml:upperCorner>43.8089464419 7.4374540321</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.29"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.29.0"><gml:exterior><gml:LinearRing><gml:posList>43.7433605409 7.4374540321 43.6545644152 7.2139083836 43.7436223027 7.1877585717 43.8050969645 7.2986784884 43.8089464419 7.4175644716 43.7433605409 7.4374540321</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MC</ogr:ISO2>
      <ogr:ISO3>MCO</ogr:ISO3>
      <ogr:NAMEen>Monaco</ogr:NAMEen>
      <ogr:GBD_ID>367</ogr:GBD_ID>
      <ogr:GBD_NAME>Monaco</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.30">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>7.0404662342 170.8964373819</gml:lowerCorner><gml:upperCorner>7.2729935475 171.543478994</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.30"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.30.0"><gml:exterior><gml:LinearRing><gml:posList>7.1449828335 171.0509675688 7.100308583 171.1720942043 7.1032488539 171.4183023033 7.1665723347 171.4781228716 7.2729935475 171.4157379169 7.1944702786 171.543478994 7.1182139913 171.5298659447 7.0643874994 171.4511970212 7.0404662342 171.1484027979 7.0922554654 170.9496184951 7.262948467 170.8964373819 7.1449828335 171.0509675688</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MH</ogr:ISO2>
      <ogr:ISO3>MHL</ogr:ISO3>
      <ogr:NAMEen>Marshall Islands</ogr:NAMEen>
      <ogr:GBD_ID>24</ogr:GBD_ID>
      <ogr:GBD_NAME>Marshall Islands</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.31">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>35.755137911 14.0977562381</gml:lowerCorner><gml:upperCorner>36.1589963262 14.6920406145</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.31"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.31.0"><gml:exterior><gml:LinearRing><gml:posList>35.755137911 14.5464265872 35.7648607612 14.2235098399 35.9908011391 14.0977562381 36.1589963262 14.3029488338 35.9888693222 14.6920406145 35.755137911 14.5464265872</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MT</ogr:ISO2>
      <ogr:ISO3>MLT</ogr:ISO3>
      <ogr:NAMEen>Malta</ogr:NAMEen>
      <ogr:GBD_ID>88</ogr:GBD_ID>
      <ogr:GBD_NAME>Malta</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.32">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-20.479668878 57.3062898551</gml:lowerCorner><gml:upperCorner>-20.096123956 57.8036985321</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.32"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.32.0"><gml:exterior><gml:LinearRing><gml:posList>-20.096123956 57.7138778 -20.2845496229 57.8036985321 -20.479668878 57.672211134 -20.4592900076 57.3642983856 -20.2580589153 57.3062898551 -20.1097234945 57.4757681969 -20.096123956 57.7138778</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MU</ogr:ISO2>
      <ogr:ISO3>MUS</ogr:ISO3>
      <ogr:NAMEen>Mauritius</ogr:NAMEen>
      <ogr:GBD_ID>183</ogr:GBD_ID>
      <ogr:GBD_NAME>Mauritius</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Seven seas (open ocean)</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.33">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-0.5777578507 166.8490033066</gml:lowerCorner><gml:upperCorner>-0.4514047362 167.0328127012</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.33"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.33.0"><gml:exterior><gml:LinearRing><gml:posList>-0.5547610896 167.020880478 -0.5777578507 166.9461814001 -0.5602027637 166.8760809164 -0.5119840123 166.8490033066 -0.4656366588 166.8815049746 -0.4514047362 166.9452514445 -0.4686977592 167.014020661 -0.5112265593 167.0328127012 -0.5547610896 167.020880478</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>NR</ogr:ISO2>
      <ogr:ISO3>NRU</ogr:ISO3>
      <ogr:NAMEen>Nauru</ogr:NAMEen>
      <ogr:GBD_ID>369</ogr:GBD_ID>
      <ogr:GBD_NAME>Nauru</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.34">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>6.9843627505 134.2888526393</gml:lowerCorner><gml:upperCorner>7.9702289476 134.9169317921</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.34"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.34.0"><gml:exterior><gml:LinearRing><gml:posList>7.591050523 134.658213738 7.8003962326 134.7464337662 7.9702289476 134.9169317921 7.7073472864 134.8842833585 7.3845331717 134.7413461401 7.0360499678 134.4537427748 6.9843627505 134.2888526393 7.3025965533 134.4204794561 7.591050523 134.658213738</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>PW</ogr:ISO2>
      <ogr:ISO3>PLW</ogr:ISO3>
      <ogr:NAMEen>Palau</ogr:NAMEen>
      <ogr:GBD_ID>380</ogr:GBD_ID>
      <ogr:GBD_NAME>Palau</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.35">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>1.2014001273 103.4583561103</gml:lowerCorner><gml:upperCorner>1.4478529701 104.2775580023</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.35"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.35.0"><gml:exterior><gml:LinearRing><gml:posList>1.3706249219 104.2775580023 1.2377951956 104.1608589388 1.2014001273 103.8968093729 1.274331275 103.6354824244 1.4478529701 103.4583561103 1.3706249219 104.2775580023</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SG</ogr:ISO2>
      <ogr:ISO3>SGP</ogr:ISO3>
      <ogr:NAMEen>Singapore</ogr:NAMEen>
      <ogr:GBD_ID>69</ogr:GBD_ID>
      <ogr:GBD_NAME>Singapore</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.36">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>43.8846420415 12.3933641273</gml:lowerCorner><gml:upperCorner>43.9827698536 12.5200114494</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.36"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.36.0"><gml:exterior><gml:LinearRing><gml:posList>43.9120671524 12.3933641273 43.9536110312 12.4059826626 43.9827698536 12.4568522694 43.9799098218 12.5060500845 43.9376700816 12.5200114494 43.8944818867 12.4950334521 43.8846420415 12.4332877042 43.9120671524 12.3933641273</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SM</ogr:ISO2>
      <ogr:ISO3>SMR</ogr:ISO3>
      <ogr:NAMEen>San Marino</ogr:NAMEen>
      <ogr:GBD_ID>396</ogr:GBD_ID>
      <ogr:GBD_NAME>San Marino</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.37">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>0.0710740386 6.3965889712</gml:lowerCorner><gml:upperCorner>0.5284120205 6.95298923</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.37"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.37.0"><gml:exterior><gml:LinearRing><gml:posList>0.0710740386 6.6852947789 0.1177431675 6.4898965358 0.2809625168 6.3965889712 0.4521969227 6.4508139842 0.5284120205 6.6592619 0.472267007 6.9112099219 0.2905653529 6.95298923 0.1549696689 6.8554024605 0.0710740386 6.6852947789</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>ST</ogr:ISO2>
      <ogr:ISO3>STP</ogr:ISO3>
      <ogr:NAMEen>Sao Tome and Principe</ogr:NAMEen>
      <ogr:GBD_ID>215</ogr:GBD_ID>
      <ogr:GBD_NAME>Sao Tome and Principe</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.38">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-5.002472421 55.3504862374</gml:lowerCorner><gml:upperCorner>-4.5241340002 55.6821588839</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.38"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.38.0"><gml:exterior><gml:LinearRing><gml:posList>-4.5241340002 55.4867642614 -4.6502312735 55.6302425336 -5.002472421 55.6821588839 -4.9289988848 55.4992516562 -4.6944448157 55.3504862374 -4.5241340002 55.4867642614</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SC</ogr:ISO2>
      <ogr:ISO3>SYC</ogr:ISO3>
      <ogr:NAMEen>Seychelles</ogr:NAMEen>
      <ogr:GBD_ID>186</ogr:GBD_ID>
      <ogr:GBD_NAME>Seychelles</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Seven seas (open ocean)</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.39">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-21.3632826847 -175.4099863992</gml:lowerCorner><gml:upperCorner>-20.9885678765 -174.8252064855</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.39"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.39.0"><gml:exterior><gml:LinearRing><gml:posList>-20.9885678765 -175.2494365987 -21.0871759916 -174.8252064855 -21.3632826847 -175.1395160257 -21.1054868538 -175.4099863992 -20.9885678765 -175.2494365987</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>TO</ogr:ISO2>
      <ogr:ISO3>TON</ogr:ISO3>
      <ogr:NAMEen>Tonga</ogr:NAMEen>
      <ogr:GBD_ID>29</ogr:GBD_ID>
      <ogr:GBD_NAME>Tonga</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.40">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>41.8641534942 12.3937276832</gml:lowerCorner><gml:upperCorner>41.946317302 12.5220233075</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.40"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.40.0"><gml:exterior><gml:LinearRing><gml:posList>41.8697747775 12.3937276832 41.946317302 12.4101733491 41.9411404084 12.5220233075 41.8641534942 12.5057550812 41.8697747775 12.3937276832</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>VA</ogr:ISO2>
      <ogr:ISO3>VAT</ogr:ISO3>
      <ogr:NAMEen>Vatican</ogr:NAMEen>
      <ogr:GBD_ID>VAT</ogr:GBD_ID>
      <ogr:GBD_NAME>Vatican</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.41">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>12.9142630687 -61.201902145</gml:lowerCorner><gml:upperCorner>13.3917395978 -60.9837090632</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.41"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.41.0"><gml:exterior><gml:LinearRing><gml:posList>13.0984110531 -61.201902145 13.2720668088 -61.2008444991 13.3734890754 -61.1851649589 13.3917395978 -61.0787470968 13.351563691 -60.9837090632 13.1056974266 -61.0323140432 12.9142630687 -61.1540929572 13.0984110531 -61.201902145</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>VC</ogr:ISO2>
      <ogr:ISO3>VCT</ogr:ISO3>
      <ogr:NAMEen>Saint Vincent and the Grenadines</ogr:NAMEen>
      <ogr:GBD_ID>117</ogr:GBD_ID>
      <ogr:GBD_NAME>Saint Vincent and the Grenadines</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.42">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-14.0561788191 -172.782582161</gml:lowerCorner><gml:upperCorner>-13.4431585193 -171.3635849804</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.42"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.42.0"><gml:exterior><gml:LinearRing><gml:posList>-13.4431585193 -172.2661494317 -13.9799363507 -171.3635849804 -14.0561788191 -171.6783171172 -13.8272676002 -172.5512125976 -13.520277602 -172.782582161 -13.4431585193 -172.2661494317</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>WS</ogr:ISO2>
      <ogr:ISO3>WSM</ogr:ISO3>
      <ogr:NAMEen>Samoa</ogr:NAMEen>
      <ogr:GBD_ID>27</ogr:GBD_ID>
      <ogr:GBD_NAME>Samoa</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.43">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-20.969968371 -61.8975036775</gml:lowerCorner><gml:upperCorner>51.0875408835 56.1787533759</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.43"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.43.0"><gml:exterior><gml:LinearRing><gml:posList>4.039374091 -51.683216926 2.358926901 -52.707708293 2.11067332 -54.134908 2.326267396 -54.615292115 3.455397441 -54.003029745 4.912802022 -54.482121949 5.348374742 -54.170969205 5.744640779 -53.944349617 5.457623554 -52.994580567 4.65228913 -51.996327278 4.039374091 -51.683216926</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.43.1"><gml:exterior><gml:LinearRing><gml:posList>42.772068357 9.4387834537 42.007045496 9.4573334991 41.4315098442 9.3759273836 41.411839059 9.167202308 41.5760008765 8.8892280845 42.2527199557 8.6430622223 42.5406258608 8.7481601196 42.6309601214 9.1636602555 42.8143708824 9.1827991559 42.772068357 9.4387834537</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.43.2"><gml:exterior><gml:LinearRing><gml:posList>49.537752585 5.790684855 49.455348652 6.345307071 48.958563131 8.200305216 47.584618544 7.586028488 46.595606588 6.131852661 46.2947009103 6.2240956714 46.4122156441 6.7872818778 45.925259909 7.02208256 45.103449606 6.602728312 43.792222398 7.502289259 43.7433605409 7.4374540321 43.8089464419 7.4175644716 43.8050969645 7.2986784884 43.7436223027 7.1877585717 43.6545644152 7.2139083836 43.044828515 5.856605081 43.532538153 3.939300977 43.106309312 3.114024285 42.4314841477 3.1809696567 42.4696252858 1.7203321516 42.6567593274 1.5608597184 42.5447354959 1.2877274765 42.685147604 -0.038933472 43.3860153322 -1.7940747449 44.778713283 -1.16535397 46.259222723 -1.110747851 47.052313544 -1.992583788 47.9857204746 -3.7248833867 48.491278387 -4.77464759 48.8824138312 -4.8127083826 48.87128327 -3.091460741 48.509019273 -2.682362434 48.660589911 -1.37230384 49.6043469717 -1.8717756803 49.6699274474 -1.1608496305 49.280096747 -0.219471809 50.217718817 1.555349155 50.868801174 1.580577019 51.0875408835 2.5217999277 49.97452179 4.139622843 49.537752585 5.790684855</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.43.3"><gml:exterior><gml:LinearRing><gml:posList>16.2180692568 -61.8975036775 16.3779277414 -61.7322051452 16.2384829604 -61.4725811365 16.4688079041 -61.3072157417 16.2194433769 -61.186683426 16.0047385139 -61.2178820046 16.08580055 -61.5528692252 15.9658973255 -61.7256684783 16.2180692568 -61.8975036775</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.43.4"><gml:exterior><gml:LinearRing><gml:posList>14.5119612222 -60.6038828663 14.371847246 -60.717339333 14.4254291579 -60.9967951389 14.5572319455 -61.1063417232 14.7571106877 -61.1575829949 14.8522839093 -61.0147834904 14.7080864345 -60.7558931547 14.5119612222 -60.6038828663</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.43.5"><gml:exterior><gml:LinearRing><gml:posList>-20.5767942516 55.7462660265 -20.6270233729 56.0379885494 -20.790398489 56.1787533759 -20.969968371 56.1340769369 -20.9443075143 55.7756466975 -20.8245922062 55.5928076607 -20.6536706996 55.5564453902 -20.5767942516 55.7462660265</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>FR</ogr:ISO2>
      <ogr:ISO3>FRA</ogr:ISO3>
      <ogr:NAMEen>France</ogr:NAMEen>
      <ogr:GBD_ID>80</ogr:GBD_ID>
      <ogr:GBD_NAME>France</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>1. Developed region: G7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.44">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>41.208183492 -179.958548764</gml:lowerCorner><gml:upperCorner>81.6369540059 179.9496244944</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.44"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.0"><gml:exterior><gml:LinearRing><gml:posList>54.4033128549 143.0999692189 52.8367027333 143.6323623568 51.9789536503 143.4228484805 49.2881246936 144.4803734291 49.393377997 143.258636915 48.0504465538 142.7522158267 46.5478492629 143.5785207448 46.7651953368 142.7161129012 46.0597248044 142.5869922633 45.902777411 142.094248894 46.602280992 141.822276238 47.966742255 142.191091342 48.8370793349 142.0779774144 51.1910436195 142.5350219152 51.935616373 142.1056525894 53.5001345718 142.1491667168 53.6575228913 142.598728772 54.1787867014 142.7648082367 54.4033128549 143.0999692189</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.1"><gml:exterior><gml:LinearRing><gml:posList>54.356269837 22.767219686 54.4342658307 19.7579288966 54.588041797 19.3094839524 55.1029148982 19.9262251318 55.3006228883 21.0364811833 54.893756409 22.808870891 54.356269837 22.767219686</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.2"><gml:exterior><gml:LinearRing><gml:posList>65.1301897008 -179.958548764 67.2437077142 -179.9445302059 68.9344873574 -179.9437799227 67.0227912164 -174.4794874161 66.8768826135 -171.5170382335 66.0853214535 -170.0069842591 65.6609711166 -170.5105598867 65.4506563706 -171.9830847145 64.2131184436 -173.0330342958 64.8039157749 -175.2783428403 65.4739647801 -176.026405631 65.4972880748 -178.3820567385 65.1301897008 -179.958548764</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.3"><gml:exterior><gml:LinearRing><gml:posList>69.3313758282 50.5909877184 68.6304180886 49.4827024546 68.6169444119 48.4578647705 69.3932701869 48.3952924921 70.0349239802 49.4169847817 69.8757017211 50.4364632572 69.3313758282 50.5909877184</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.4"><gml:exterior><gml:LinearRing><gml:posList>70.9947039179 -179.9245167468 71.5118955909 -179.9208367433 71.5730525617 -178.6830455647 71.1843227671 -177.2322995786 70.9947039179 -179.9245167468</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.5"><gml:exterior><gml:LinearRing><gml:posList>74.2879934538 143.0241584133 74.1152694886 144.5093127011 73.3453631031 143.4088526844 73.485043916 140.493302439 74.2879934538 143.0241584133</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.6"><gml:exterior><gml:LinearRing><gml:posList>75.48822663 146.7295028 75.3970064777 150.2338641458 75.3243501256 153.0314203177 74.803534247 148.172129754 75.48822663 146.7295028</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.7"><gml:exterior><gml:LinearRing><gml:posList>76.096869208 139.109629754 76.181219794 141.366953972 75.512274481 145.380869988 74.947455145 143.707774285 74.68740469 139.343435092 75.134222723 137.149099155 75.725775458 137.067637566 76.096869208 139.109629754</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.8"><gml:exterior><gml:LinearRing><gml:posList>76.104641018 67.146494988 75.334173895 61.379649285 74.444037177 59.145762566 74.180883573 58.4767142976 73.5623496972 57.0897006824 72.3548362173 55.6065029497 71.258978583 56.105723504 70.729885158 57.638682488 70.553127346 55.162608269 70.8315318517 53.187914248 71.683835144 52.2276395091 73.759588934 53.631114129 75.867621161 58.931000196 76.20693594 62.956797722 76.581735762 65.5288229601 76.5290906758 66.653977997 76.104641018 67.146494988</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.9"><gml:exterior><gml:LinearRing><gml:posList>42.295111395 130.699961785 42.530480042 130.530771119 43.380221456 131.28090621 44.841708476 130.933433879 45.2293538981 131.9919443364 44.527986561 132.3536218555 45.5403816751 133.2575604354 46.258986308 133.90245162 47.257891744 134.154115845 47.710732321 134.772579387 48.381337383 134.386349732 47.707528381 132.524654582 47.682284445 131.023350871 48.333769226 130.822122843 49.41078359 129.502410116 49.54157664 128.092470338 51.063783468 126.931765992 52.152657572 126.531170288 53.1685824973 125.9497198547 53.7331411849 124.5287569115 53.6845503734 122.581510365 53.280159811 120.874254598 52.760656637 120.03296228 52.117595114 120.779169963 50.37661611 119.141233765 50.092654114 119.316210165 49.512741191 117.758837525 49.823264873 116.684277792 49.895405172 115.368699179 50.276880595 114.286284628 49.588602194 113.043673137 49.137673646 110.731359497 49.327429505 108.538056681 49.933490703 107.946516561 50.476454977 105.328640178 50.131204733 103.668069703 50.545546366 102.327634726 51.32389679 102.18387089 51.731727193 100.005967652 52.128550517 98.886397746 51.001125794 97.806360311 50.518622946 98.293462362 49.725544739 97.301688274 50.015191142 94.624539022 50.565235087 94.237585897 50.843357646 92.301683391 49.165837301 87.816324097 49.085273743 87.323796021 49.58720693 86.591799357 49.581522522 85.26309493 50.992650859 83.433955933 50.730341289 81.460226277 51.30201182 80.682754354 50.78811554 79.990083456 53.272330832 77.866802206 53.8425845084 76.9598554934 54.461172995 76.85239384 53.432553609 73.424075968 53.874852193 73.485260864 54.101556702 71.182300659 55.294145406 70.820358928 55.434550273 68.943160848 54.956129863 68.166050659 54.34304067 65.212949259 53.950248108 61.60050826 53.635745342 61.007572876 52.922171326 61.058422485 52.334997864 61.039922323 51.888875224 60.138065226 51.258396912 61.66365686 50.782482808 61.37209843 50.678148092 58.92594283 51.150031434 58.308409465 51.066263937 56.508002564 50.763168457 55.1045814792 51.503033142 53.346335083 51.635298971 50.581697632 50.57954946 48.67339034 50.1074706112 48.3054787052 50.452580465 47.556921021 49.820319316 46.899906861 49.201494039 46.756988397 48.410224508 46.479312378 47.819769592 47.119480021 47.769695129 48.044488159 46.327879184 49.2271305193 45.758937893 47.619476759 44.446193752 46.700938347 44.211981512 47.245941602 43.020819403 47.461924675 41.845282294 48.578949415 41.208183492 47.871113729 41.89044159 46.430891561 42.75950999 44.859359579 42.552209982 43.939725789 43.228990377 42.422867473 43.538971456 40.651916138 43.3889896097 39.9859763554 44.148260809 38.978282097 45.0942448337 36.5123596731 45.3476870797 36.6451264137 45.310695705 37.738291863 46.676336981 37.737559441 47.008612372 39.261892123 47.103257554 38.216644727 47.848243306 38.830942017 47.832947083 39.759050741 49.020316468 39.681122681 49.245780741 40.141663045 49.858508199 39.182755168 50.424933573 37.435264933 50.345145162 35.696869751 51.183181865 35.099180135 51.248914286 34.185954224 52.35460907 33.804065389 52.100567729 31.76434493 52.9736195622 31.6751584926 53.0397164888 32.3261551137 53.439478252 32.719532104 53.794857076 31.744656209 54.786011048 30.770607137 55.79352061 30.468816773 56.142414043 28.148906697 57.52760081 27.352934611 58.0020489524 27.6414384538 58.9957844089 27.7152305574 59.1616595139 27.8426052139 59.3566509454 28.1800455088 59.481756903 28.019053582 59.9935499801 29.2632363827 60.3911417293 28.7447407901 60.553045966 27.807871941 61.245900981 29.203157593 62.905928854 31.56952478 63.741536764 29.980784546 64.219698792 30.558475382 64.991434632 29.588147013 66.108058981 29.900169311 66.837549337 29.089159383 67.685843812 30.00941329 68.204003398 28.663758993 69.027260641 28.954077189 69.8058419025 30.8409538907 69.327704169 33.620453321 69.176825262 35.966970248 67.697902736 41.016612175 66.7455336249 40.9430036112 66.109290685 39.9561877874 66.088771877 37.927093946 66.73041413 33.48324629 66.116156317 34.496836785 64.556301174 34.773203972 63.818182684 37.291026238 64.034898179 38.088063998 64.917059637 36.439952019 65.172976826 37.007677058 64.541449286 40.517100457 65.401841539 39.712087436 66.532171942 42.200938347 65.885443427 44.167165561 67.177452523 43.815329312 67.927810549 44.1113696142 68.613674221 43.325694207 68.446234442 45.943858269 67.844916083 46.722015821 67.4901692004 45.5240108432 66.887030341 45.853200717 67.000921942 47.72559655 67.560288804 47.824717644 68.91815827 53.437022332 68.185126044 54.864024285 69.003119208 58.878754102 69.849107164 60.770274285 69.550116278 64.12761478 68.333889065 68.258636915 68.8789737 69.109060092 69.692775783 67.101817254 71.298407294 66.927012566 71.83616771 68.478851759 72.705877997 68.989105665 73.5170400517 71.6039799322 72.709906317 72.836924675 71.7861913638 72.3825613902 71.108954169 72.67074629 69.1248265901 72.9449137254 68.167710609 73.2301597037 66.7659510143 71.5289834806 66.618841864 69.112640821 66.0107031512 70.3035194364 66.230780341 71.990082227 67.598078205 74.2929902501 68.9091686526 75.1509061618 68.6391399 76.6101015863 68.885809637 77.6826278 69.494702453 76.2993586863 69.4863594474 74.2068641182 70.520331122 74.316172722 72.2396363085 74.2442771997 71.9653289822 75.4407253428 71.3046567933 75.7261072156 71.222044046 76.7730445119 71.893570582 77.0993026917 72.0603155091 78.2686231865 72.385565497 79.391449415 71.699896552 81.736664259 71.610907294 83.628184441 72.545355536 80.704112175 73.571763414 80.832204623 73.90224844 86.783213738 74.262518622 86.020030144 75.502671617 89.22925866 76.138128973 95.872894727 75.888983466 96.835215691 77.2205538264 102.0267307832 77.135484117 104.345713738 76.737290757 107.946055535 76.746812242 111.137054884 76.14838288 113.45972741 74.992905992 112.879649285 73.194281317 106.359873894 73.685858466 110.898610873 73.510646877 113.493662957 73.588771877 118.450205925 73.209418036 118.462412957 72.978041673 121.0378629959 73.806382554 124.383067254 73.470851955 127.987559441 73.028944249 129.443508491 71.984408341 128.3912924296 71.346828518 129.392100457 70.9621340428 130.5696359456 71.883612372 132.539317254 71.436509507 133.650075717 71.634833075 135.985118035 71.224920966 138.067230665 71.4257782764 139.6647456711 72.497626044 139.54175866 72.774318752 141.550629102 72.3543957903 146.9448884785 71.70311107 149.005218946 70.880560614 152.210948113 71.047512111 157.989105665 70.670965887 159.689463738 69.89077383 159.688975457 69.3604361918 160.5416702042 69.676581122 162.407969597 69.775213934 167.802012566 68.9891346981 169.228464248 69.0259692762 170.2384575196 70.116278387 170.610850457 69.856304017 175.529736586 68.9672499244 179.9496244944 67.0809314552 179.9429154177 65.0800371578 179.9326637524 64.353989976 178.282562696 63.7393021534 179.2014805566 62.705633856 179.609060092 62.366034247 178.938161655 62.2667522013 176.471503713 61.8440731948 174.2124413602 60.8706096153 171.6469551684 60.1297338596 170.8907963679 59.965643622 170.400645379 60.567328192 169.247813347 60.084173895 165.129649285 59.6170492671 163.3935466464 59.007594079 163.077757778 58.087392175 161.993228165 57.708944701 163.329510112 56.763028714 162.783500266 56.180658663 163.366068623 56.12905508 162.142100457 54.849920966 162.153168165 54.17572663 160.052989129 53.257147528 159.632497592 52.899359442 158.64714603 51.543983029 157.759192434 50.886453751 156.719697509 52.935420328 156.11119584 55.284204852 155.551177167 56.765090664 156.047615999 57.803941148 157.469899936 58.414780992 159.023773634 59.14057038 159.864431186 61.346625067 164.028819207 62.673065416 164.597937066 62.521994642 163.26705047 61.6758487 162.417246941 60.576336133 160.154392683 61.944728908 160.345713738 61.8208221376 158.1504341365 60.263258053 155.4195593624 59.17576732 155.19410241 58.8456798105 153.4499014 59.226648758 152.311827469 59.8364875625 150.5897616213 59.233272922 148.934522469 59.7141737141 146.3323713657 59.353908596 143.159678582 58.38275788 140.904307488 56.977942647 138.661615027 56.116522528 137.571950717 54.894120841 135.219571451 54.591376044 136.883311394 53.80849844 136.684743686 54.315985419 138.738536004 54.2430027649 140.6610366822 53.1756116405 141.5280492826 51.941961981 141.429535352 50.544378973 140.442881707 50.058010158 140.660655144 48.44940827 140.165049675 47.787909247 139.261729363 46.227972723 138.10035241 44.401516018 135.888682488 43.508368231 135.139903191 42.688421942 133.157725457 43.174139716 131.730723504 42.295111395 130.699961785</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.10"><gml:exterior><gml:LinearRing><gml:posList>79.275702216 102.929942254 78.676522481 104.8062975842 77.5564509416 98.8895640611 79.238755601 101.309906446 79.275702216 102.929942254</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.11"><gml:exterior><gml:LinearRing><gml:posList>80.131415106 97.774261915 79.6236628453 100.2967188249 78.5263716346 99.4546147914 78.592612045 94.8708261545 79.552476304 93.28028405 80.131415106 97.774261915</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.12"><gml:exterior><gml:LinearRing><gml:posList>80.813267381 48.8288806845 80.676947333 51.700938347 80.7798773664 54.9575882137 80.9491227208 57.1032293396 80.9655980592 59.4208915401 79.361318318 54.4423985438 79.0886497484 51.7338589338 79.5506232797 49.8977433695 80.813267381 48.8288806845</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.44.13"><gml:exterior><gml:LinearRing><gml:posList>81.6369540059 96.4888923392 80.9369897721 98.4557510585 80.2204272268 92.9561828674 81.6369540059 96.4888923392</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>RU</ogr:ISO2>
      <ogr:ISO3>RUS</ogr:ISO3>
      <ogr:NAMEen>Russia</ogr:NAMEen>
      <ogr:GBD_ID>62</ogr:GBD_ID>
      <ogr:GBD_NAME>Russian Federation</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>3. Emerging region: BRIC</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.45">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>29.386605326 60.511789185</gml:lowerCorner><gml:upperCorner>38.443520203 74.892306763</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.45"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.45.0"><gml:exterior><gml:LinearRing><gml:posList>37.231113587 74.892306763 37.021669006 74.542353963 36.820596008 72.565213664 36.045707906 71.16591923 35.203123678 71.633694296 34.054253235 71.062618042 34.043788758 70.002837769 33.31894928 70.294447876 33.075010682 69.547516724 31.937689922 69.298849731 31.825913798 68.159126018 30.922868144 66.366263876 29.835337627 66.195628296 29.386605326 64.086092977 29.407818502 62.477508993 29.85817861 60.844378703 30.831400859 61.785199829 31.381909892 61.661176391 31.494667868 60.821744425 33.638387146 60.511789185 35.61849884 61.269675741 35.147519837 62.302015828 35.856262106 63.342934204 36.249984843 64.444572388 37.233232321 65.063087606 37.578379212 65.761442912 37.3641804 66.51958785 37.188868103 67.780544475 36.930873516 68.011124715 37.586027324 69.528654826 37.541430562 70.157867065 38.443520203 70.76108606 37.898359681 71.597727499 36.7048408 71.611060018 37.459471741 73.276074667 37.231113587 74.892306763</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>AF</ogr:ISO2>
      <ogr:ISO3>AFG</ogr:ISO3>
      <ogr:NAMEen>Afghanistan</ogr:NAMEen>
      <ogr:GBD_ID>160</ogr:GBD_ID>
      <ogr:GBD_NAME>Afghanistan</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>South Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.46">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-18.031404724 11.751800977</gml:lowerCorner><gml:upperCorner>-4.403089295 24.000632772</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.46"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.46.0"><gml:exterior><gml:LinearRing><gml:posList>-10.8770496782 23.9676076164 -13.001479187 24.000632772 -13.001479187 21.979877563 -16.165885519 21.983804972 -16.543950297 22.108138469 -17.641144307 23.381652466 -18.031404724 20.806202433 -17.747701111 18.761986124 -17.389893494 18.453581177 -17.408186951 13.942745402 -16.951057231 13.166307006 -17.252699477 11.766123894 -15.799086196 11.751800977 -14.750583592 12.272715691 -13.442071222 12.512705925 -12.248711847 13.639821811 -10.646172784 13.7529403 -9.09384531 12.996348504 -8.323500258 13.369395379 -6.904554946 12.828786655 -6.114769138 12.275060383 -5.861165405 13.1838927169 -5.8593347537 16.3157818715 -8.0894963807 17.5443642728 -8.0067101558 19.3556307093 -7.006661305 19.5219148285 -6.9206712009 20.6115674262 -7.2910901553 20.520620974 -7.2880929033 21.7850451703 -8.5046678436 21.9400907528 -9.622541295 21.8542185417 -10.3733030991 22.3135312065 -11.2542913211 22.2377848658 -10.8770496782 23.9676076164</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.46.1"><gml:exterior><gml:LinearRing><gml:posList>-4.635323181 13.07370284 -5.055090841 12.444387248 -5.763441665 12.210541212 -5.019630836 12.009607691 -4.403089295 12.854284709 -4.635323181 13.07370284</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>AO</ogr:ISO2>
      <ogr:ISO3>AGO</ogr:ISO3>
      <ogr:NAMEen>Angola</ogr:NAMEen>
      <ogr:GBD_ID>168</ogr:GBD_ID>
      <ogr:GBD_NAME>Angola</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.47">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>39.693508205 19.309825066</gml:lowerCorner><gml:upperCorner>42.546758118 21.0212289216</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.47"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.47.0"><gml:exterior><gml:LinearRing><gml:posList>42.546758118 20.064955688 41.873181662 20.567147258 41.0826440979 20.633525624 40.9084800643 20.7245659003 40.906252751 20.9410466833 40.841981383 20.9645643201 40.6872807501 21.0212289216 39.693508205 19.999847852 40.644354559 19.309825066 41.852362372 19.365082227 42.0884163441 19.3710404475 42.3263247431 19.4004744743 42.546758118 20.064955688</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>AL</ogr:ISO2>
      <ogr:ISO3>ALB</ogr:ISO3>
      <ogr:NAMEen>Albania</ogr:NAMEen>
      <ogr:GBD_ID>43</ogr:GBD_ID>
      <ogr:GBD_NAME>Albania</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.48">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>22.703576559 51.56934655</gml:lowerCorner><gml:upperCorner>25.884154699 56.383311394</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.48"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.48.0"><gml:exterior><gml:LinearRing><gml:posList>25.6274456978 56.2790552346 24.978216864 56.383311394 24.855202332 55.788666626 24.230330912 55.755697062 22.703576559 55.186842895 22.93110789 52.583074178 24.256170966 51.56934655 24.0208899771 51.9608567642 24.14472077 54.122569207 25.884154699 55.8863373911 25.6215553345 56.1159005342 25.6274456978 56.2790552346</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>AE</ogr:ISO2>
      <ogr:ISO3>ARE</ogr:ISO3>
      <ogr:NAMEen>United Arab Emirates</ogr:NAMEen>
      <ogr:GBD_ID>156</ogr:GBD_ID>
      <ogr:GBD_NAME>United Arab Emirates</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.49">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-55.05104603 -73.586588908</gml:lowerCorner><gml:upperCorner>-21.792415466 -53.666719523</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.49"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.49.0"><gml:exterior><gml:LinearRing><gml:posList>-52.639571699 -68.6276167765 -53.297717399 -68.451350729 -54.465800355 -66.482213314 -54.64055755 -65.155588345 -55.05104603 -66.540507726 -54.8862445646 -68.6541354319 -54.799167576 -68.641997851 -54.7829713158 -68.6364837741 -52.639571699 -68.6276167765</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.49.1"><gml:exterior><gml:LinearRing><gml:posList>-22.234455668 -62.650357219 -23.805470886 -61.00634904 -24.007008972 -60.033669393 -25.45984019 -57.556921346 -27.156274109 -58.653288534 -27.487313334 -57.180096802 -27.443698425 -55.754731608 -26.644987488 -54.793059042 -25.574944884 -54.600202613 -25.629235534 -53.90996049 -26.219173686 -53.666719523 -27.16350881 -53.842005981 -28.231970724 -55.772534139 -30.182962748 -57.611698365 -32.4471299123 -58.2001118521 -34.288181248 -58.570423957 -35.484200289 -57.144908527 -36.170342706 -57.248036262 -36.851006769 -56.664865689 -38.152764581 -57.593576627 -38.693780206 -59.063221809 -39.01148854 -61.518055793 -38.814711196 -62.390207486 -39.364841404 -62.023915168 -40.307875258 -62.490061002 -40.872735284 -62.337798632 -41.158786717 -63.779408332 -40.705987238 -64.890451627 -41.985772394 -65.069569465 -42.070174455 -63.809959888 -42.751234633 -63.620269335 -42.506931248 -64.437733528 -42.97633229 -64.433461067 -43.672621352 -65.334055142 -45.027686553 -65.602642605 -44.992919197 -66.200366619 -45.613491405 -67.331924501 -46.163750909 -67.622466601 -47.006605727 -66.78644772 -47.06617604 -65.979562955 -48.047168108 -65.968445 -49.015816864 -67.556548698 -49.954540456 -67.833207006 -50.330572019 -68.876429324 -50.978224848 -69.167778305 -52.3468881602 -68.4484404191 -52.007418722 -69.952753866 -51.99005544 -71.917698528 -51.552666118 -72.450069133 -50.648896994 -72.302842978 -50.74945933 -73.177724976 -49.529998881 -73.586588908 -48.333276062 -72.295091512 -47.914800313 -72.543913534 -46.690068868 -71.687169963 -45.739945984 -71.798532675 -45.299456075 -71.311533977 -44.754786478 -72.047819784 -43.926309916 -71.659884807 -42.998717956 -72.148537151 -40.720355733 -71.955577353 -38.935348409 -71.41576534 -38.691435649 -70.873834595 -37.706069437 -71.18503007 -36.484335225 -71.043281616 -36.046015727 -70.380324667 -34.585329691 -70.227827515 -34.24323171 -69.83276119 -32.464942322 -70.172482057 -31.549597676 -70.591371216 -29.363064474 -70.042619181 -28.397852071 -69.653030559 -27.112762553 -68.801092896 -27.044963073 -68.330475627 -24.769856466 -68.572373007 -24.030366719 -67.362369345 -22.822223409 -67.193904175 -21.792415466 -66.240008911 -22.114049581 -65.744612794 -22.212751567 -64.586879842 -22.863667907 -64.343897258 -22.00759613 -63.947590699 -22.234455668 -62.650357219</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>AR</ogr:ISO2>
      <ogr:ISO3>ARG</ogr:ISO3>
      <ogr:NAMEen>Argentina</ogr:NAMEen>
      <ogr:GBD_ID>97</ogr:GBD_ID>
      <ogr:GBD_NAME>Argentina</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>South America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.50">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>38.863701274 43.4404281</gml:lowerCorner><gml:upperCorner>41.290452373 46.51403894</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.50"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.50.0"><gml:exterior><gml:LinearRing><gml:posList>41.290452373 45.002399944 40.223592428 45.979239543 40.02541331 45.639570353 39.5303916442 46.2729202574 38.882175599 46.51403894 38.863701274 46.135870809 39.548956604 45.78808842 39.702797343 44.774558553 40.110162659 43.665427287 41.106587626 43.4404281 41.290452373 45.002399944</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>AM</ogr:ISO2>
      <ogr:ISO3>ARM</ogr:ISO3>
      <ogr:NAMEen>Armenia</ogr:NAMEen>
      <ogr:GBD_ID>33</ogr:GBD_ID>
      <ogr:GBD_NAME>Armenia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.51">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-43.639580988 112.8220127462</gml:lowerCorner><gml:upperCorner>-10.708184503 153.605967644</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.51"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.51.0"><gml:exterior><gml:LinearRing><gml:posList>-40.4584400306 144.3974099254 -41.0635348418 146.1287870521 -40.6022008794 148.023682957 -40.9823390545 148.4845676019 -42.2716457328 148.3085495567 -43.22242604 147.964040561 -43.2771388133 147.3677334474 -43.639580988 146.855723504 -43.552504165 146.024180535 -42.604261977 145.265472852 -40.4584400306 144.3974099254</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.51.1"><gml:exterior><gml:LinearRing><gml:posList>-35.6361552112 137.5624963957 -36.2467168365 137.8433733984 -36.1750681684 136.4441444799 -35.5169313328 136.2386093927 -35.6361552112 137.5624963957</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.51.2"><gml:exterior><gml:LinearRing><gml:posList>-11.0948062582 130.2370182802 -10.9897448046 131.2642446173 -11.474786066 131.8686139183 -11.8376049483 130.9866889334 -11.7128625257 129.9668569822 -11.0948062582 130.2370182802</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.51.3"><gml:exterior><gml:LinearRing><gml:posList>-10.708184503 142.54957116 -12.328789972 143.07715905 -12.61305104 143.433929884 -13.750176691 143.53012129 -14.511163019 143.941172722 -14.15992604 144.478851759 -14.942559503 145.345713738 -16.443291925 145.400401238 -17.63258229 146.147715691 -18.874281508 146.276621941 -19.82805755 147.684255405 -20.052666925 148.560069207 -21.0897763 149.231211785 -22.206668352 149.9352652929 -22.697930597 150.826426629 -23.411309503 150.814219597 -24.022556248 151.780446811 -25.934991144 153.205577019 -27.046075128 153.206228061 -28.867445571 153.605967644 -32.442091941 152.539886296 -32.864967129 151.834033071 -34.330418881 150.924918127 -35.178887628 150.759450717 -35.893731378 150.152598504 -37.6982250203 150.0088072624 -37.823418878 148.306407097 -38.6210001307 146.7716308898 -39.136407159 146.425547722 -38.530531508 145.373057488 -38.858330988 143.536957227 -38.36712005 142.397715691 -38.399834894 141.653981967 -37.882094008 140.359873894 -37.180840753 139.741547071 -36.621758722 139.853688998 -35.8741895703 139.260747399 -35.67164479 138.201670769 -34.740899347 138.536306186 -34.1822144647 138.3036465816 -34.967622319 137.6238028303 -33.2230005569 137.1725233184 -33.7800701795 136.1830718592 -34.880140883 135.604014519 -33.8022180276 134.9127910527 -32.5459577704 134.0983598316 -32.032972915 132.233734571 -31.477634373 131.177500847 -31.690524998 128.990000847 -32.27337005 127.288747592 -32.232517185 126.185720248 -33.121514581 124.116547071 -33.938653253 123.526621941 -33.819512628 121.532888217 -33.926039321 120.002207879 -34.908379816 118.385101759 -35.050225519 116.637868686 -34.36337656 115.118825717 -33.521091404 114.988536004 -33.287692967 115.66684004 -31.648614191 115.680349155 -30.214043878 114.974864129 -29.479913019 114.978770379 -28.106703383 114.166026238 -27.315362238 114.012543165 -26.9079239544 113.6935018179 -26.3492933095 113.1790271486 -25.5417143502 112.8220127462 -25.8208940289 113.6514992815 -25.6994925941 114.0469567997 -25.12493255 113.713063998 -24.049411717 113.414561394 -23.5116513 113.749847852 -22.577732029 113.646657748 -21.859958592 114.011485222 -22.0035434043 114.3703324162 -21.843357029 114.646739129 -21.513767185 115.454600457 -20.549899998 116.820323113 -20.727715753 117.353770379 -20.362074477 118.13559004 -19.633721613 120.954356316 -18.451836847 121.823741082 -18.150567316 122.323985222 -17.261163019 122.172048373 -16.580336196 122.95980879 -17.518975519 123.553965691 -17.210381769 123.860850457 -16.265232029 123.539724155 -16.1615150875 124.2162097465 -15.522556248 124.389903191 -15.338148696 124.928884311 -14.504327081 125.356455925 -13.7004677578 127.1817748302 -14.698418878 128.175303582 -14.942559503 129.611827019 -14.408786717 129.35808353 -12.940524998 130.129893425 -12.2351488407 130.9948529241 -12.13681406 132.679047071 -11.4864307505 132.5740499429 -11.3984508268 133.0992212515 -11.858330988 133.524668816 -11.9837488933 134.4270561543 -12.1425515015 135.5802592213 -11.942333451 136.450713101 -12.348983402 136.927816273 -13.013441665 136.652028842 -13.256149132 135.942838503 -14.19524505 135.909434441 -14.712334894 135.378265821 -15.915215753 136.704600457 -16.915622654 139.037282748 -17.706149998 139.999522332 -17.450778904 140.833262566 -16.061781508 141.431162957 -12.526055597 141.594737175 -10.930108331 142.17457116 -10.708184503 142.54957116</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>AU</ogr:ISO2>
      <ogr:ISO3>AUS</ogr:ISO3>
      <ogr:NAMEen>Australia</ogr:NAMEen>
      <ogr:GBD_ID>71</ogr:GBD_ID>
      <ogr:GBD_NAME>Australia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.52">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>38.434068101 44.774558553</gml:lowerCorner><gml:upperCorner>41.89044159 49.89340254</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.52"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.52.0"><gml:exterior><gml:LinearRing><gml:posList>38.863701274 46.135870809 38.9452682836 45.2491653187 39.561142994 44.8346988445 39.702797343 44.774558553 39.548956604 45.78808842 38.863701274 46.135870809</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.52.1"><gml:exterior><gml:LinearRing><gml:posList>41.89044159 46.430891561 41.208183492 47.871113729 41.845282294 48.578949415 40.369045315 49.89340254 40.078436591 49.442637566 38.434068101 48.874278191 38.821559143 48.012965535 39.378941142 48.338940471 39.685279032 47.846877482 38.882175599 46.51403894 39.5303916442 46.2729202574 40.02541331 45.639570353 40.223592428 45.979239543 41.290452373 45.002399944 41.04483429 46.496779012 41.89044159 46.430891561</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>AZ</ogr:ISO2>
      <ogr:ISO3>AZE</ogr:ISO3>
      <ogr:NAMEen>Azerbaijan</ogr:NAMEen>
      <ogr:GBD_ID>34</ogr:GBD_ID>
      <ogr:GBD_NAME>Azerbaijan</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.53">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-4.4505910788 29.015365438</gml:lowerCorner><gml:upperCorner>-2.316911723 30.825487508</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.53"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.53.0"><gml:exterior><gml:LinearRing><gml:posList>-2.400627543 30.55459965 -2.978576761 30.825487508 -4.271934509 30.003005411 -4.4505910788 29.6543531249 -3.3344242464 29.2066786417 -2.720711365 29.015365438 -2.808251241 29.697546021 -2.316911723 29.931691935 -2.400627543 30.55459965</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BI</ogr:ISO2>
      <ogr:ISO3>BDI</ogr:ISO3>
      <ogr:NAMEen>Burundi</ogr:NAMEen>
      <ogr:GBD_ID>175</ogr:GBD_ID>
      <ogr:GBD_NAME>Burundi</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.54">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>6.213893947 0.768769164</gml:lowerCorner><gml:upperCorner>12.2310288425 3.837419067</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.54"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.54.0"><gml:exterior><gml:LinearRing><gml:posList>11.69577301 3.596400187 10.599896749 3.837419067 8.772308045 2.722861776 6.3683516726 2.7038412893 6.213893947 1.619639519 9.049526266 1.601173136 9.996471049 1.331008748 10.367094421 0.768769164 10.992740987 0.901474243 11.473771057 1.439632609 11.426900533 2.010760538 11.89653595 2.390168904 12.2310288425 2.6025122308 12.1491879924 3.1116627147 11.69577301 3.596400187</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BJ</ogr:ISO2>
      <ogr:ISO3>BEN</ogr:ISO3>
      <ogr:NAMEen>Benin</ogr:NAMEen>
      <ogr:GBD_ID>200</ogr:GBD_ID>
      <ogr:GBD_NAME>Benin</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.55">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>9.488724263 -5.522578085</gml:lowerCorner><gml:upperCorner>15.069727274 2.390168904</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.55"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.55.0"><gml:exterior><gml:LinearRing><gml:posList>14.910977275 0.218466838 14.546710103 0.152941121 13.0673172 0.971754191 12.705633851 2.109049113 11.89653595 2.390168904 11.426900533 2.010760538 11.473771057 1.439632609 10.992740987 0.901474243 11.134980367 -0.166109171 10.994032898 -0.695999309 10.985842184 -2.750705933 9.488724263 -2.689210978 9.908233541 -3.202564657 9.682071635 -4.681079061 10.42548879 -5.522578085 11.836436259 -5.28248938 12.149492086 -4.560466268 13.469954122 -3.971613322 13.5175966081 -3.3208293581 14.04299408 -2.840855469 14.198643494 -2.023411825 15.069727274 -0.75289506 14.910977275 0.218466838</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BF</ogr:ISO2>
      <ogr:ISO3>BFA</ogr:ISO3>
      <ogr:NAMEen>Burkina Faso</ogr:NAMEen>
      <ogr:GBD_ID>201</ogr:GBD_ID>
      <ogr:GBD_NAME>Burkina Faso</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.56">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>21.061102606 88.021789592</gml:lowerCorner><gml:upperCorner>26.45389028 92.57587854</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.56"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.56.0"><gml:exterior><gml:LinearRing><gml:posList>21.977574362 92.57587854 21.061102606 92.265147332 22.539555645 91.6170062003 22.575832424 90.954925977 22.856411745 90.6502534028 22.314886786 90.613454623 21.5961373196 89.1214720516 23.183805644 88.954704224 23.649952901 88.540104208 24.223251242 88.74980717 24.645602722 88.021789592 25.167870585 88.924163452 25.908135478 88.0743962 26.45389028 88.461142619 25.931751608 89.83439213 25.374162903 89.79501469 25.14999054 90.364644003 25.182960104 92.001753377 24.405979106 92.107586711 23.612099915 91.140824015 22.944466248 91.586068156 23.731653341 92.150788208 21.977574362 92.57587854</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BD</ogr:ISO2>
      <ogr:ISO3>BGD</ogr:ISO3>
      <ogr:NAMEen>Bangladesh</ogr:NAMEen>
      <ogr:GBD_ID>161</ogr:GBD_ID>
      <ogr:GBD_NAME>Bangladesh</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>South Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.57">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>41.239396057 22.345023234</gml:lowerCorner><gml:upperCorner>44.228434539 28.578379754</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.57"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.57.0"><gml:exterior><gml:LinearRing><gml:posList>43.741278387 28.578379754 42.458644924 27.629405144 41.97256094 28.016774936 42.091747132 27.273352906 41.713036397 26.333358602 41.239396057 25.28572229 41.561650289 24.510162395 41.335772604 22.916977986 41.9077564398 22.8565652991 42.313439026 22.345023234 43.198992208 22.981366822 43.807921448 22.349467407 44.228434539 22.691640373 43.886592281 23.325150732 43.654287415 25.359619588 44.177046204 27.027476441 43.741278387 28.578379754</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BG</ogr:ISO2>
      <ogr:ISO3>BGR</ogr:ISO3>
      <ogr:NAMEen>Bulgaria</ogr:NAMEen>
      <ogr:GBD_ID>45</ogr:GBD_ID>
      <ogr:GBD_NAME>Bulgaria</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.58">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>42.559212138 15.9833981709</gml:lowerCorner><gml:upperCorner>45.284523825 19.3904035405</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.58"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.58.0"><gml:exterior><gml:LinearRing><gml:posList>44.865634664 19.01582076 44.88713206 19.36866744 44.4457522097 19.240471937 44.0055586499 19.3904035405 43.532796122 19.195344686 42.559212138 18.437354777 42.890936591 17.653330925 42.942084052 17.580658399 43.5347114139 17.0680060084 44.5932791617 15.9833981709 45.0689690795 16.0143286918 45.284523825 16.924372193 44.865634664 19.01582076</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BA</ogr:ISO2>
      <ogr:ISO3>BIH</ogr:ISO3>
      <ogr:NAMEen>Bosnia and Herzegovina</ogr:NAMEen>
      <ogr:GBD_ID>44</ogr:GBD_ID>
      <ogr:GBD_NAME>Bosnia and Herzegovina</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.59">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>51.484429627 23.165644979</gml:lowerCorner><gml:upperCorner>56.142414043 32.719532104</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.59"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.59.0"><gml:exterior><gml:LinearRing><gml:posList>56.142414043 28.148906697 55.79352061 30.468816773 54.786011048 30.770607137 53.794857076 31.744656209 53.439478252 32.719532104 53.0397164888 32.3261551137 52.9736195622 31.6751584926 52.100567729 31.76434493 52.074677836 30.959329467 51.484429627 30.148629598 51.595378927 27.25402592 51.928511047 25.76791508 51.880012716 24.390789836 51.517399191 23.606238241 52.289393413 23.165644979 52.670042013 23.868961222 53.939292704 23.485625448 53.969678447 24.788698365 54.869675192 25.782694539 55.2483949451 26.585069889 55.666990866 26.594531291 56.142414043 28.148906697</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BY</ogr:ISO2>
      <ogr:ISO3>BLR</ogr:ISO3>
      <ogr:NAMEen>Belarus</ogr:NAMEen>
      <ogr:GBD_ID>57</ogr:GBD_ID>
      <ogr:GBD_NAME>Belarus</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.60">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>15.8429139169 -89.234135092</gml:lowerCorner><gml:upperCorner>18.481350002 -88.091542121</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.60"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.60.0"><gml:exterior><gml:LinearRing><gml:posList>18.371730861 -88.091542121 17.580267645 -88.283314582 16.955471096 -88.217437304 15.8429139169 -88.6941161154 15.95494456 -89.234135092 17.81431427 -89.160496175 18.481350002 -88.303822395 18.371730861 -88.091542121</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BZ</ogr:ISO2>
      <ogr:ISO3>BLZ</ogr:ISO3>
      <ogr:NAMEen>Belize</ogr:NAMEen>
      <ogr:GBD_ID>108</ogr:GBD_ID>
      <ogr:GBD_NAME>Belize</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.61">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-22.863667907 -69.577634644</gml:lowerCorner><gml:upperCorner>-9.825652364 -57.55108191</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.61"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.61.0"><gml:exterior><gml:LinearRing><gml:posList>-20.165124613 -58.158796753 -19.821372985 -58.175281535 -19.286728617 -59.089540975 -19.298097432 -60.006384237 -19.657765401 -61.761212525 -20.579776306 -62.277305054 -22.234455668 -62.650357219 -22.00759613 -63.947590699 -22.863667907 -64.343897258 -22.212751567 -64.586879842 -22.114049581 -65.744612794 -21.792415466 -66.240008911 -22.822223409 -67.193904175 -22.833592224 -67.87634314 -21.284332784 -68.207537395 -20.089677022 -68.7755389 -19.458397725 -68.496356975 -18.946490987 -68.989608521 -17.506588198 -69.510088752 -16.551258157 -69.0377217651 -16.2128736161 -68.6175986611 -15.5197582472 -69.3689075755 -14.964408467 -69.390669312 -14.211482849 -68.883723511 -12.867947286 -68.980952718 -12.502491557 -68.684252483 -10.952301941 -69.577634644 -10.97896698 -68.293320272 -9.904303894 -66.631871298 -9.825652364 -65.304381267 -11.153426615 -65.394737509 -11.996269226 -64.997449097 -12.457326355 -64.395728719 -12.652663269 -63.07503414 -13.530800882 -61.847977254 -13.464551697 -61.047225098 -13.797864685 -60.472634847 -16.273062439 -60.129839234 -16.331250102 -58.464721233 -17.267213643 -58.381160442 -17.555774841 -57.790757203 -18.183643493 -57.55108191 -20.165124613 -58.158796753</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BO</ogr:ISO2>
      <ogr:ISO3>BOL</ogr:ISO3>
      <ogr:NAMEen>Bolivia</ogr:NAMEen>
      <ogr:GBD_ID>121</ogr:GBD_ID>
      <ogr:GBD_NAME>Bolivia (Plurinational State of)</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>South America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.62">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-33.7406759724 -74.018474691</gml:lowerCorner><gml:upperCorner>5.202138367 -34.797352668</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.62"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.62.0"><gml:exterior><gml:LinearRing><gml:posList>2.326267396 -54.615292115 2.11067332 -54.134908 2.358926901 -52.707708293 4.039374091 -51.683216926 3.9107245895 -51.3314490787 3.376328545 -51.092741743 1.815415757 -50.466542121 1.664262758 -49.910158911 1.165228583 -49.890044726 -0.0653241127 -51.0504846204 -0.9390682876 -52.0829316167 -0.7363431545 -51.2641316296 -0.0730737804 -50.3951131941 -0.0758142792 -49.5737560459 -0.3580036694 -48.548348097 -1.8929388465 -48.8969440887 -1.645114842 -48.421457486 -0.661716404 -48.037912564 -0.920993748 -46.171783007 -1.847263279 -44.521717903 -2.549574477 -44.368072069 -2.346467357 -43.31176641 -2.834161066 -42.23940996 -2.848402602 -39.985707161 -3.697442316 -38.477406379 -4.910902602 -37.182932095 -5.143731378 -35.510568814 -7.158461196 -34.797352668 -7.905857029 -34.811634895 -9.193047784 -35.305083788 -10.502129816 -36.408070442 -10.772881769 -36.903065559 -12.641045831 -38.050892707 -12.622491144 -38.722075976 -13.366631769 -39.039784309 -14.674981378 -39.067128059 -15.843357029 -38.86436927 -17.67962005 -39.137115038 -18.518731378 -39.724680142 -19.416110935 -39.709787564 -21.523338532 -41.071867556 -21.947523696 -40.967274543 -22.535414321 -41.966297981 -22.953789972 -42.058216926 -22.905938409 -43.86436927 -23.107679946 -44.70051022 -23.664727472 -45.433501757 -24.299086196 -46.966460741 -24.894138279 -47.825550911 -25.872816665 -48.578521288 -28.610446873 -48.815337694 -29.443129165 -49.810658332 -30.461521092 -50.315663215 -33.0146309677 -52.4483263127 -32.6674692721 -52.2708278513 -31.4223049168 -51.2356551218 -30.6527551265 -50.841842947 -30.6260118065 -51.2691858126 -31.1245824902 -51.5355557875 -31.6789546911 -51.9598946217 -33.101983331 -52.621693489 -33.7406759724 -53.3790945694 -33.5597688778 -53.5367920278 -32.6592425794 -53.1724546853 -31.471049499 -54.591520956 -30.798222351 -56.011356771 -30.102037455 -56.831280885 -30.182962748 -57.611698365 -28.231970724 -55.772534139 -27.16350881 -53.842005981 -26.219173686 -53.666719523 -25.629235534 -53.90996049 -25.574944884 -54.600202613 -25.4316939044 -54.6094558586 -24.0662345087 -54.2666745135 -23.976829936 -55.398035034 -22.317344665 -55.874388387 -22.035294698 -57.986817586 -20.953603617 -57.81757727 -20.165124613 -58.158796753 -18.183643493 -57.55108191 -17.555774841 -57.790757203 -17.267213643 -58.381160442 -16.331250102 -58.464721233 -16.273062439 -60.129839234 -13.797864685 -60.472634847 -13.464551697 -61.047225098 -13.530800882 -61.847977254 -12.652663269 -63.07503414 -12.457326355 -64.395728719 -11.996269226 -64.997449097 -11.153426615 -65.394737509 -9.825652364 -65.304381267 -9.904303894 -66.631871298 -10.97896698 -68.293320272 -10.952301941 -69.577634644 -11.010799662 -70.641342326 -9.565823263 -70.624418295 -10.006829935 -71.391425741 -10.005589701 -72.195666057 -9.540915222 -72.306770386 -9.409036967 -73.214983684 -9.085645854 -72.95939205 -7.543517761 -74.018474691 -6.90417694 -73.765492717 -6.435264995 -73.131784628 -5.132088725 -72.917947551 -4.179433695 -70.832235067 -4.236484477 -69.964949504 -1.182717387 -69.399454305 -0.124900818 -70.073805908 0.588130595 -70.054246379 0.5928460528 -69.2621331282 0.8297056584 -69.1880284905 1.059419657 -69.852191529 1.707674663 -69.856170614 1.749971823 -67.998247843 1.9598459941 -67.5883731345 1.222510478 -66.875060588 0.733031311 -66.156344767 1.647394104 -64.080864218 2.420576884 -63.384834351 2.47132314 -64.047972169 3.594717102 -64.202975627 4.118871155 -64.58967037 3.968363546 -63.42540035 3.563762919 -62.931011923 4.020711772 -62.76621578 4.248527324 -61.567270875 4.900580546 -60.612626303 5.202138367 -60.739853679 5.085943909 -59.983104005 4.513007304 -60.148778646 3.931905823 -59.5292299 2.694048564 -60.000079712 1.851309306 -59.74968156 1.377798157 -59.24214148 2.021453959 -57.104494182 2.0374662082 -56.4944124319 2.333088684 -56.116802531 2.543050029 -54.978577434 2.326267396 -54.615292115</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BR</ogr:ISO2>
      <ogr:ISO3>BRA</ogr:ISO3>
      <ogr:NAMEen>Brazil</ogr:NAMEen>
      <ogr:GBD_ID>135</ogr:GBD_ID>
      <ogr:GBD_NAME>Brazil</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>3. Emerging region: BRIC</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>South America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.63">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>4.021435242 113.99878991</gml:lowerCorner><gml:upperCorner>4.9900154729 115.6069422041</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.63"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.63.0"><gml:exterior><gml:LinearRing><gml:posList>4.601141669 113.99878991 4.9900154729 115.1117252181 4.6450282549 115.6069422041 4.021435242 114.586628052 4.601141669 113.99878991</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BN</ogr:ISO2>
      <ogr:ISO3>BRN</ogr:ISO3>
      <ogr:NAMEen>Brunei</ogr:NAMEen>
      <ogr:GBD_ID>66</ogr:GBD_ID>
      <ogr:GBD_NAME>Brunei Darussalam</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.64">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>26.69620107 88.89233077</gml:lowerCorner><gml:upperCorner>28.3583989308 92.035859822</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.64"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.64.0"><gml:exterior><gml:LinearRing><gml:posList>27.759443665 91.632887004 26.854847718 92.035859822 26.69620107 89.817287232 26.821413066 89.096504354 27.315543111 88.89233077 28.134640401 89.561488891 28.3583989308 90.2255906943 27.759443665 91.632887004</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BT</ogr:ISO2>
      <ogr:ISO3>BTN</ogr:ISO3>
      <ogr:NAMEen>Bhutan</ogr:NAMEen>
      <ogr:GBD_ID>162</ogr:GBD_ID>
      <ogr:GBD_NAME>Bhutan</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>South Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.65">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-26.891794128 19.978345988</gml:lowerCorner><gml:upperCorner>-17.794106547 29.350073689</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.65"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.65.0"><gml:exterior><gml:LinearRing><gml:posList>-17.794106547 25.259780721 -19.501082459 26.130270223 -20.509082947 27.698133179 -21.577854919 28.032893107 -21.797893168 29.038723186 -22.186706645 29.350073689 -22.584615173 28.338559204 -23.645842387 27.00417037 -24.248131205 26.849709513 -24.748152364 25.868374064 -25.619520365 25.587254272 -25.829223327 24.798620239 -25.310805359 23.006998332 -25.984252625 22.719367309 -26.855207214 21.687182251 -26.891794128 20.690757283 -26.131323751 20.84144576 -24.752493184 19.981446574 -22.000671489 19.978345988 -22.000671489 20.971980428 -18.319345805 20.975081014 -18.009803975 23.31147587 -18.478199157 23.592182251 -18.029441019 24.183050578 -17.794106547 25.259780721</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>BW</ogr:ISO2>
      <ogr:ISO3>BWA</ogr:ISO3>
      <ogr:NAMEen>Botswana</ogr:NAMEen>
      <ogr:GBD_ID>193</ogr:GBD_ID>
      <ogr:GBD_NAME>Botswana</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.66">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>2.236453756 14.584773396</gml:lowerCorner><gml:upperCorner>11.000828349 27.44130131</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.66"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.66.0"><gml:exterior><gml:LinearRing><gml:posList>10.919153747 22.861064087 9.907768453 23.624014934 8.783392639 23.482318156 8.68932733 24.170328031 7.335574036 25.360032999 6.6532901 26.378007039 6.104951477 26.481049846 5.768744608 27.123801311 5.070725199 27.44130131 5.0549382287 26.4625765337 5.3292613374 25.3956123053 4.587266337 23.388370402 4.823582663 22.898374471 4.134787496 22.422641235 4.409731954 20.603114054 5.135966695 19.719550415 4.306069031 18.537090291 3.476868998 18.626387166 3.708276062 17.458913208 3.46438914 16.567701457 2.236453756 16.196664673 2.86328888 16.092381633 3.758970642 15.171300904 4.622018738 14.717271769 5.92341217 14.584773396 7.523262838 15.481049439 7.870011699 16.54868453 7.550237936 16.768619425 7.985198466 17.67977828 8.047881979 18.589283488 9.015264791 19.100570109 9.138125509 20.435165649 11.000828349 22.460468384 10.919153747 22.861064087</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CF</ogr:ISO2>
      <ogr:ISO3>CAF</ogr:ISO3>
      <ogr:NAMEen>Central African Republic</ogr:NAMEen>
      <ogr:GBD_ID>169</ogr:GBD_ID>
      <ogr:GBD_NAME>Central African Republic</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.67">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-55.997252745 -75.645387995</gml:lowerCorner><gml:upperCorner>-17.506588198 -66.843690129</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.67"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.67.0"><gml:exterior><gml:LinearRing><gml:posList>-55.1779219139 -66.843690129 -55.4766025293 -67.6118945404 -55.4959694704 -68.4994971943 -55.8982829182 -68.9697078382 -55.997252745 -69.4795989852 -55.7795137033 -69.8655199333 -55.5791390229 -70.3325811213 -55.0456365648 -70.6871350629 -55.0562101475 -69.5113207763 -55.0723490096 -68.6238718021 -55.1779219139 -66.843690129</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.67.1"><gml:exterior><gml:LinearRing><gml:posList>-53.5154244725 -73.1654244434 -53.9544929742 -71.4502270699 -54.1846769434 -72.2263327604 -53.5154244725 -73.1654244434</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.67.2"><gml:exterior><gml:LinearRing><gml:posList>-52.639571699 -68.6276167765 -54.7829713158 -68.6364837741 -54.799167576 -68.641997851 -54.8862445646 -68.6541354319 -54.840590102 -70.75877845 -54.405450128 -71.725005663 -54.012215763 -70.5504334359 -53.011716396 -70.418992611 -52.451472416 -69.417565116 -52.639571699 -68.6276167765</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.67.3"><gml:exterior><gml:LinearRing><gml:posList>-50.0026617768 -74.6751597742 -50.2674679989 -75.1569460369 -49.2586301822 -75.645387995 -48.1645324733 -75.5985833161 -48.0101735688 -75.0019970894 -48.9973278786 -74.9582310314 -50.0026617768 -74.6751597742</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.67.4"><gml:exterior><gml:LinearRing><gml:posList>-42.060203142 -73.8917242027 -41.932489977 -73.2888344169 -43.7077941156 -73.7703110695 -43.6443667129 -74.471668081 -42.060203142 -73.8917242027</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.67.5"><gml:exterior><gml:LinearRing><gml:posList>-22.822223409 -67.193904175 -24.030366719 -67.362369345 -24.769856466 -68.572373007 -27.044963073 -68.330475627 -27.112762553 -68.801092896 -28.397852071 -69.653030559 -29.363064474 -70.042619181 -31.549597676 -70.591371216 -32.464942322 -70.172482057 -34.24323171 -69.83276119 -34.585329691 -70.227827515 -36.046015727 -70.380324667 -36.484335225 -71.043281616 -37.706069437 -71.18503007 -38.691435649 -70.873834595 -38.935348409 -71.41576534 -40.720355733 -71.955577353 -42.998717956 -72.148537151 -43.926309916 -71.659884807 -44.754786478 -72.047819784 -45.299456075 -71.311533977 -45.739945984 -71.798532675 -46.690068868 -71.687169963 -47.914800313 -72.543913534 -48.333276062 -72.295091512 -49.529998881 -73.586588908 -50.74945933 -73.177724976 -50.648896994 -72.302842978 -51.552666118 -72.450069133 -51.99005544 -71.917698528 -52.007418722 -69.952753866 -52.3468881602 -68.4484404191 -52.252129816 -69.442005989 -52.733982029 -70.856841601 -53.809747003 -71.034250455 -53.240817967 -73.199086067 -52.7836799109 -73.9727083247 -51.6985930301 -74.6765001014 -50.4681482196 -75.0632138951 -50.086114191 -74.347645637 -47.767754816 -74.628407356 -46.8685544548 -73.7827113302 -47.3345643262 -74.890139512 -47.2545679606 -75.4307534253 -46.448458898 -75.3668629008 -45.4953391178 -74.9104289727 -44.4318127862 -74.4142491159 -44.5192910452 -73.8551737575 -45.7205330535 -73.9931477893 -46.3954540527 -74.310517304 -46.322035415 -73.687855598 -44.957777602 -73.348255989 -44.174086196 -73.283599413 -41.985528253 -72.462554491 -41.483575128 -72.957264778 -41.748841349 -73.759946026 -40.972840598 -73.946904238 -39.978448175 -73.714182095 -39.489515883 -73.241566536 -37.719414972 -73.668080207 -37.16025156 -73.209055142 -35.566582941 -72.646595832 -35.116143488 -72.230580207 -33.541436456 -71.624663866 -32.369561456 -71.412464973 -30.2382138 -71.634632942 -29.91025156 -71.286610481 -28.940118097 -71.521962043 -28.360609633 -71.173939582 -25.344903253 -70.444813606 -24.524834894 -70.582834439 -21.433377482 -70.058694938 -18.3377462069 -70.3947027461 -17.506588198 -69.510088752 -18.946490987 -68.989608521 -19.458397725 -68.496356975 -20.089677022 -68.7755389 -21.284332784 -68.207537395 -22.833592224 -67.87634314 -22.822223409 -67.193904175</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CL</ogr:ISO2>
      <ogr:ISO3>CHL</ogr:ISO3>
      <ogr:NAMEen>Chile</ogr:NAMEen>
      <ogr:GBD_ID>98</ogr:GBD_ID>
      <ogr:GBD_NAME>Chile</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>South America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.68">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>4.3528445816 -8.618719849</gml:lowerCorner><gml:upperCorner>10.720742086 -2.506328084</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.68"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.68.0"><gml:exterior><gml:LinearRing><gml:posList>10.42548879 -5.522578085 9.682071635 -4.681079061 9.908233541 -3.202564657 9.488724263 -2.689210978 8.209267477 -2.506328084 6.617142436 -3.262509318 5.149115302 -2.843698697 5.231675523 -3.995757616 5.029974677 -5.851307746 4.3528445816 -7.5406661241 5.845949198 -7.446543336 6.492834778 -8.618719849 7.5579894 -8.48544633 7.544295146 -8.228976196 9.409917705 -7.865380818 10.161990662 -7.989662639 10.185503439 -6.959389608 10.720742086 -6.245841431 10.189870097 -6.015932983 10.42548879 -5.522578085</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CI</ogr:ISO2>
      <ogr:ISO3>CIV</ogr:ISO3>
      <ogr:NAMEen>Ivory Coast</ogr:NAMEen>
      <ogr:GBD_ID>205</ogr:GBD_ID>
      <ogr:GBD_NAME>Côte d'Ivoire</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.69">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>1.73480479 8.594167514</gml:lowerCorner><gml:upperCorner>13.0826216418 16.196664673</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.69"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.69.0"><gml:exterior><gml:LinearRing><gml:posList>7.523262838 15.481049439 5.92341217 14.584773396 4.622018738 14.717271769 3.758970642 15.171300904 2.86328888 16.092381633 2.236453756 16.196664673 1.73480479 16.15904423 2.152427877 14.266963338 2.161057841 13.294567912 2.165760396 11.322078491 2.341742255 9.7995711595 3.228094794 9.919688347 3.960353908 9.618907097 4.100775458 8.971202019 4.815293687 8.594167514 5.847499492 8.855872437 6.99440623 10.119464559 7.058071595 10.602535848 6.437282613 11.31949467 8.831503397 12.805708862 9.382890931 12.862036173 11.276263733 13.982329549 11.496431173 14.593765096 12.167423808 14.669936157 12.385601705 14.179836873 13.0750351902 14.0665556526 13.0826216418 14.4578984438 12.5368490974 14.6935040769 11.530821838 15.135644165 10.793114929 15.065880981 9.991277568 15.681243937 9.978177593 14.181697225 9.637759094 13.947602987 8.479147644 15.183703247 7.523262838 15.481049439</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CM</ogr:ISO2>
      <ogr:ISO3>CMR</ogr:ISO3>
      <ogr:NAMEen>Cameroon</ogr:NAMEen>
      <ogr:GBD_ID>202</ogr:GBD_ID>
      <ogr:GBD_NAME>Cameroon</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.70">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-13.433855896 12.210541212</gml:lowerCorner><gml:upperCorner>5.3292613374 31.2693446377</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.70"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.70.0"><gml:exterior><gml:LinearRing><gml:posList>5.070725199 27.44130131 4.27782786 28.404136596 4.669535217 29.457612345 3.490201518 30.839543498 2.462279765 30.707665243 2.170394737 31.2693446377 1.8187516415 31.1276079405 1.2387726624 30.4783817597 0.785017802 29.92828129 -0.0986732303 29.6887608019 -0.4850430547 29.6410151842 -1.3930817714 29.57793851 -1.6863271024 29.2212526473 -2.4770641795 28.8744457681 -2.720711365 29.015365438 -3.3344242464 29.2066786417 -5.072655832 29.0963633095 -6.7075625339 29.4987881383 -7.3737126287 30.1831990747 -8.2217699874 30.5698657966 -8.481407219 28.893485601 -8.7357104912 28.8001333273 -9.3791892099 28.5160606604 -10.519460144 28.634923543 -11.446948751 28.35700769 -12.376194356 29.030351603 -12.15408905 29.799296915 -13.424244079 29.79764327 -13.433855896 29.168947795 -12.450505066 28.339747762 -12.293615417 27.638188517 -11.569628601 27.180128621 -12.017456563 26.697057333 -11.646109721 25.351971477 -11.199935404 25.278797648 -11.381216329 24.285473266 -10.8770496782 23.9676076164 -11.2542913211 22.2377848658 -10.3733030991 22.3135312065 -9.622541295 21.8542185417 -8.5046678436 21.9400907528 -7.2880929033 21.7850451703 -7.2910901553 20.520620974 -6.9206712009 20.6115674262 -7.006661305 19.5219148285 -8.0067101558 19.3556307093 -8.0894963807 17.5443642728 -5.8593347537 16.3157818715 -5.861165405 13.1838927169 -5.763441665 12.210541212 -5.055090841 12.444387248 -4.635323181 13.07370284 -4.794796651 13.358750041 -4.278445739 14.368559204 -4.815053812 14.831166626 -3.28987559 16.227463827 -2.129016215 16.231287883 -1.262505798 16.839726196 -0.523429463 17.749541463 2.160024312 18.072415812 3.476868998 18.626387166 4.306069031 18.537090291 5.135966695 19.719550415 4.409731954 20.603114054 4.134787496 22.422641235 4.823582663 22.898374471 4.587266337 23.388370402 5.3292613374 25.3956123053 5.0549382287 26.4625765337 5.070725199 27.44130131</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CD</ogr:ISO2>
      <ogr:ISO3>COD</ogr:ISO3>
      <ogr:NAMEen>Congo-Kinshasa</ogr:NAMEen>
      <ogr:GBD_ID>171</ogr:GBD_ID>
      <ogr:GBD_NAME>Democratic Republic of the Congo</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.71">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-5.019630836 11.1140163041</gml:lowerCorner><gml:upperCorner>3.708276062 18.626387166</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.71"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.71.0"><gml:exterior><gml:LinearRing><gml:posList>3.476868998 18.626387166 2.160024312 18.072415812 -0.523429463 17.749541463 -1.262505798 16.839726196 -2.129016215 16.231287883 -3.28987559 16.227463827 -4.815053812 14.831166626 -4.278445739 14.368559204 -4.794796651 13.358750041 -4.635323181 13.07370284 -4.403089295 12.854284709 -5.019630836 12.009607691 -3.9368561899 11.1140163041 -3.30320811 11.944366089 -2.4526288205 11.9007915951 -2.366417745 12.966112508 -2.1680043779 13.5385407334 -2.2887277819 14.0603219595 -1.548689881 14.463127075 -0.630916442 14.498990519 -0.2054127 13.831227661 0.913227031 14.468088013 1.380847066 14.183867635 1.221787008 13.250229533 2.161057841 13.294567912 2.152427877 14.266963338 1.73480479 16.15904423 2.236453756 16.196664673 3.46438914 16.567701457 3.708276062 17.458913208 3.476868998 18.626387166</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CG</ogr:ISO2>
      <ogr:ISO3>COG</ogr:ISO3>
      <ogr:NAMEen>Congo-Brazzaville</ogr:NAMEen>
      <ogr:GBD_ID>170</ogr:GBD_ID>
      <ogr:GBD_NAME>Congo</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.72">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-4.236484477 -78.828684049</gml:lowerCorner><gml:upperCorner>12.464300848 -66.875060588</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.72"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.72.0"><gml:exterior><gml:LinearRing><gml:posList>11.8499976907 -71.3275065336 11.661924948 -71.971080282 10.452463888 -72.907560588 9.295376892 -73.009724895 8.338613587 -72.386765503 7.440218811 -72.451309367 7.066598206 -72.080996053 6.94443512 -70.096621054 6.122237244 -69.443637655 6.266233622 -67.573984335 4.512077128 -67.865077677 3.425709331 -67.304646769 2.886129863 -67.838619345 2.402025045 -67.197624878 1.222510478 -66.875060588 1.9598459941 -67.5883731345 1.749971823 -67.998247843 1.707674663 -69.856170614 1.059419657 -69.852191529 0.8297056584 -69.1880284905 0.5928460528 -69.2621331282 0.588130595 -70.054246379 -0.124900818 -70.073805908 -1.182717387 -69.399454305 -4.236484477 -69.964949504 -3.782041931 -70.73412736 -2.71513031 -70.050629028 -2.197435812 -70.992716228 -2.446516215 -72.396584026 -2.213558858 -73.197775432 -1.255167745 -73.636560018 -0.858602803 -74.344423788 -0.170479431 -74.824652873 -0.107020772 -75.283487915 0.458526103 -76.311590536 0.40829661 -77.424390829 0.819641012 -77.673316203 1.434312242 -78.828684049 2.647045418 -78.333972984 2.577704169 -77.777943489 3.92206452 -77.031564908 4.155218817 -77.435170051 6.543768622 -77.343006965 7.2350980564 -77.8958391508 7.935072326 -77.1690059 8.664740302 -77.373524543 8.545477606 -76.947173632 9.452853128 -75.620632773 10.58584219 -75.509836392 11.109669657 -74.844372315 10.775702216 -74.362172004 11.316961981 -74.187489387 11.294012762 -73.292388476 12.1180267224 -72.1680730902 12.3518191415 -72.0649861058 12.464300848 -71.697255012 12.246213077 -71.3146369268 11.8499976907 -71.3275065336</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CO</ogr:ISO2>
      <ogr:ISO3>COL</ogr:ISO3>
      <ogr:NAMEen>Colombia</ogr:NAMEen>
      <ogr:GBD_ID>125</ogr:GBD_ID>
      <ogr:GBD_NAME>Colombia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>South America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.73">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>8.0347480201 -85.874989387</gml:lowerCorner><gml:upperCorner>11.0808801812 -82.573597786</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.73"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.73.0"><gml:exterior><gml:LinearRing><gml:posList>10.92593008 -83.644357877 9.576198635 -82.573597786 9.602722473 -82.829022176 8.0347480201 -82.8976292589 8.623806057 -83.739898241 9.03534577 -83.6298722 9.651027736 -84.678374804 9.555446682 -85.117827929 10.355047919 -85.874989387 11.0808801812 -85.7017355659 10.92593008 -83.644357877</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CR</ogr:ISO2>
      <ogr:ISO3>CRI</ogr:ISO3>
      <ogr:NAMEen>Costa Rica</ogr:NAMEen>
      <ogr:GBD_ID>126</ogr:GBD_ID>
      <ogr:GBD_NAME>Costa Rica</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.74">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>19.85569896 -84.521351692</gml:lowerCorner><gml:upperCorner>23.194240627 -74.267404752</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.74"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.74.0"><gml:exterior><gml:LinearRing><gml:posList>19.8972259627 -75.0950058107 19.900051174 -75.232858853 19.85569896 -77.737212694 20.465277411 -77.082427538 20.711655992 -78.065541145 21.639715887 -78.766509569 21.698553778 -79.895375129 22.068508205 -80.450917121 22.188910223 -81.824452278 22.679730389 -81.877500935 22.696112372 -82.780669726 22.186093371 -83.415472437 21.786566473 -84.521351692 22.666205145 -84.078277148 23.194240627 -82.219349739 23.089544989 -80.62222246 22.357001044 -78.676136848 21.813299872 -77.760894335 21.07807038 -76.002186653 20.690578518 -75.708851692 20.625799872 -74.771839973 20.068304755 -74.267404752 19.8972259627 -75.0950058107</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CU</ogr:ISO2>
      <ogr:ISO3>CUB</ogr:ISO3>
      <ogr:NAMEen>Cuba</ogr:NAMEen>
      <ogr:GBD_ID>109</ogr:GBD_ID>
      <ogr:GBD_NAME>Cuba</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.75">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>34.5714646106 32.3805991804</gml:lowerCorner><gml:upperCorner>35.6620791231 34.4881741881</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.75"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.75.0"><gml:exterior><gml:LinearRing><gml:posList>34.5714646106 33.0151933697 34.6800107274 32.5833285011 34.9248308397 32.3805991804 35.2086491387 32.5839461182 35.2310920057 33.0100160094 35.4335532331 33.0834078914 35.3809441976 33.6492417758 35.6620791231 34.4881741881 35.1557834091 33.9067213262 35.1551223885 33.9059634349 34.9345975373 34.0329940884 34.8004617348 33.6583107365 34.6204133646 33.5245574525 34.5714646106 33.0151933697</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CY</ogr:ISO2>
      <ogr:ISO3>CYP</ogr:ISO3>
      <ogr:NAMEen>Cyprus</ogr:NAMEen>
      <ogr:GBD_ID>77</ogr:GBD_ID>
      <ogr:GBD_NAME>Cyprus</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.76">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>48.589541728 12.510223022</gml:lowerCorner><gml:upperCorner>50.858447164 18.833196249</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.76"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.76.0"><gml:exterior><gml:LinearRing><gml:posList>50.858447164 14.8103927 50.2845831544 16.5785977055 50.1823334108 18.3248863178 49.510260722 18.833196249 48.604166158 16.945042766 49.007914124 14.982061808 48.589541728 14.69567102 48.766430156 13.815724731 49.327842917 12.797750692 50.388760071 12.510223022 50.858447164 14.8103927</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CZ</ogr:ISO2>
      <ogr:ISO3>CZE</ogr:ISO3>
      <ogr:NAMEen>Czechia</ogr:NAMEen>
      <ogr:GBD_ID>47</ogr:GBD_ID>
      <ogr:GBD_NAME>Czechia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.77">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>10.99878713 41.7852981941</gml:lowerCorner><gml:upperCorner>12.707912502 43.377126498</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.77"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.77.0"><gml:exterior><gml:LinearRing><gml:posList>11.487860419 43.240733269 10.99878713 42.923715454 11.0902686194 41.7858764944 11.2833486151 41.7852981941 12.46590688 42.379459269 12.707912502 43.117686394 12.004828192 43.377126498 11.707953192 42.754079623 11.487860419 43.240733269</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>DJ</ogr:ISO2>
      <ogr:ISO3>DJI</ogr:ISO3>
      <ogr:NAMEen>Djibouti</ogr:NAMEen>
      <ogr:GBD_ID>177</ogr:GBD_ID>
      <ogr:GBD_NAME>Djibouti</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.78">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>17.604803778 -72.006122552</gml:lowerCorner><gml:upperCorner>19.935492255 -68.356190559</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.78"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.78.0"><gml:exterior><gml:LinearRing><gml:posList>18.03925202 -71.776234504 18.490400409 -71.9078776579 18.6014749709 -72.006122552 19.710109768 -71.757435676 19.935492255 -71.018422004 19.635972398 -69.897938606 19.093451239 -69.608469205 18.656561591 -68.356190559 18.374823309 -68.849598762 18.430121161 -70.703846809 17.604803778 -71.417225715 18.03925202 -71.776234504</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>DO</ogr:ISO2>
      <ogr:ISO3>DOM</ogr:ISO3>
      <ogr:NAMEen>Dominican Republic</ogr:NAMEen>
      <ogr:GBD_ID>111</ogr:GBD_ID>
      <ogr:GBD_NAME>Dominican Republic</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.79">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>18.981684876 -8.6829954779</gml:lowerCorner><gml:upperCorner>37.082993882 11.968860717</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.79"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.79.0"><gml:exterior><gml:LinearRing><gml:posList>36.9395107635 8.6025104286 36.448784078 8.349236695 34.647653708 8.236478719 33.893901265 7.479832398 33.207664083 7.750720255 32.07184194 9.045008179 30.228905335 9.519707885 29.128533224 9.826149129 26.504223125 9.835760945 26.113394267 9.40116215 24.856339213 10.032028035 24.266840312 11.567128133 23.517351176 11.968860717 22.193426819 9.723313029 20.87257721 7.482726278 19.478631287 5.837607055 19.142243551 4.22860966 18.981684876 3.308355754 19.79406423 3.216785116 20.231789449 1.89138798 21.101710511 1.146523885 23.500995585 -2.523226278 24.995064596 -4.821613118 27.284959249 -8.6829954779 27.6761318694 -8.6828343985 28.665899964 -8.682385213 29.389421692 -7.619452678 29.6140708361 -5.7561563712 30.5086413535 -4.3723676021 30.7113174864 -3.6455290567 31.6478209968 -3.659506721 32.132200013 -2.516146606 32.081660462 -1.249557251 32.517008566 -1.047502401 33.237972311 -1.674234172 34.741343079 -1.769525513 35.089300848 -2.222564257 36.486883856 1.044769727 36.80857982 2.933116082 36.895412502 4.787119988 36.650946356 5.235850457 37.082993882 7.383474155 36.9395107635 8.6025104286</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>DZ</ogr:ISO2>
      <ogr:ISO3>DZA</ogr:ISO3>
      <ogr:NAMEen>Algeria</ogr:NAMEen>
      <ogr:GBD_ID>139</ogr:GBD_ID>
      <ogr:GBD_NAME>Algeria</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.80">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-5.011372579 -80.916180218</gml:lowerCorner><gml:upperCorner>1.434312242 -75.283487915</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.80"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.80.0"><gml:exterior><gml:LinearRing><gml:posList>-0.107020772 -75.283487915 -1.502594502 -75.560034343 -2.573640239 -76.684591025 -2.980643819 -77.849016073 -3.408524678 -78.361723796 -4.550987243 -78.672660889 -5.011372579 -79.063928996 -3.998772888 -80.481077637 -3.3934976535 -80.3407286664 -3.197442316 -79.953195767 -2.7334027788 -79.9815280299 -2.710625909 -80.320546028 -2.102914608 -80.74497919 -1.065524768 -80.916180218 0.052209779 -80.066458957 0.839667059 -80.046213345 1.119696356 -78.991688606 1.434312242 -78.828684049 0.819641012 -77.673316203 0.40829661 -77.424390829 0.458526103 -76.311590536 -0.107020772 -75.283487915</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>EC</ogr:ISO2>
      <ogr:ISO3>ECU</ogr:ISO3>
      <ogr:NAMEen>Ecuador</ogr:NAMEen>
      <ogr:GBD_ID>122</ogr:GBD_ID>
      <ogr:GBD_NAME>Ecuador</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>South America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.81">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>21.994782613 24.688342732</gml:lowerCorner><gml:upperCorner>31.65648021 36.883636915</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.81"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.81.0"><gml:exterior><gml:LinearRing><gml:posList>31.3142666891 34.2002694411 31.211448958 34.248350857 29.490057684 34.8867293625 27.80068594 34.078461134 28.556586005 33.239431186 29.955755927 32.570485873 29.599920966 32.337657097 28.578924872 32.868907097 27.959051825 33.512543165 26.611639716 34.014008009 23.977118231 35.513926629 22.932126497 35.695305231 21.995713609 36.883636915 21.9954074694 33.1811382663 21.994782613 28.290345093 21.995351054 24.981244751 25.205439352 24.981244751 29.181372376 24.981244751 30.144155986 24.688342732 31.65648021 25.150889519 31.368231512 27.342295769 30.827053127 29.028086785 31.508734442 30.36215254 31.523993231 31.917979363 31.1999018197 32.4809028022 31.045558986 33.133311394 31.3142666891 34.2002694411</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>EG</ogr:ISO2>
      <ogr:ISO3>EGY</ogr:ISO3>
      <ogr:NAMEen>Egypt</ogr:NAMEen>
      <ogr:GBD_ID>141</ogr:GBD_ID>
      <ogr:GBD_NAME>Egypt</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.82">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>12.46590688 36.423647095</gml:lowerCorner><gml:upperCorner>18.004828192 43.117686394</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.82"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.82.0"><gml:exterior><gml:LinearRing><gml:posList>12.707912502 43.117686394 12.46590688 42.379459269 14.105961812 40.833197062 14.637402242 39.075474894 14.417183126 38.426832316 14.879532166 37.891464478 14.116684672 37.564972778 14.457439067 37.293774862 14.263523254 36.526379842 15.111507671 36.423647095 16.252859192 36.944028768 17.072602437 37.000459432 17.522290751 38.209687948 18.004828192 38.601573113 16.108710028 39.235606316 15.263902085 39.718272332 14.641587632 41.156097852 13.940252997 41.676524285 12.707912502 43.117686394</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>ER</ogr:ISO2>
      <ogr:ISO3>ERI</ogr:ISO3>
      <ogr:NAMEen>Eritrea</ogr:NAMEen>
      <ogr:GBD_ID>178</ogr:GBD_ID>
      <ogr:GBD_NAME>Eritrea</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.83">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>57.52760081 23.470876498</gml:lowerCorner><gml:upperCorner>59.665838934 28.1800455088</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.83"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.83.0"><gml:exterior><gml:LinearRing><gml:posList>59.481756903 28.019053582 59.3566509454 28.1800455088 59.1616595139 27.8426052139 58.9957844089 27.7152305574 58.7958158031 27.2703649905 58.1430650292 27.3380936388 58.0020489524 27.6414384538 57.52760081 27.352934611 58.075138449 25.263501424 57.8681862559 24.3061587342 58.3858951168 23.5526197001 59.212144273 23.470876498 59.665838934 25.72535241 59.481756903 28.019053582</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>EE</ogr:ISO2>
      <ogr:ISO3>EST</ogr:ISO3>
      <ogr:NAMEen>Estonia</ogr:NAMEen>
      <ogr:GBD_ID>58</ogr:GBD_ID>
      <ogr:GBD_NAME>Estonia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.84">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>3.40501292 33.0512948</gml:lowerCorner><gml:upperCorner>14.879532166 47.979169149</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.84"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.84.0"><gml:exterior><gml:LinearRing><gml:posList>12.46590688 42.379459269 11.2833486151 41.7852981941 11.0902686194 41.7858764944 10.99878713 42.923715454 10.632220358 42.647246541 9.41301829 43.419034057 8.985525004 44.023855021 7.996567281 46.979230184 7.996567281 47.979169149 6.496736348 46.423915242 4.911484273 44.941525106 4.953962301 43.96897465 3.977226054 41.885019165 4.28493337 40.763743937 3.867284444 39.848450969 3.40501292 39.536325317 3.612648824 38.10189091 4.432237041 36.844086548 4.619331563 35.920835409 5.309108581 35.809110962 5.622474467 35.098663371 6.637606303 34.7335177 7.657156474 33.716060425 7.801127014 33.0512948 8.4044752 33.174078003 8.445377096 33.970773559 9.454592111 34.070697863 10.87928538 34.587066691 10.869182638 34.950662069 11.77137563 35.048227173 12.575150859 35.616874633 12.695660299 36.099429159 14.263523254 36.526379842 14.457439067 37.293774862 14.116684672 37.564972778 14.879532166 37.891464478 14.417183126 38.426832316 14.637402242 39.075474894 14.105961812 40.833197062 12.46590688 42.379459269</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>ET</ogr:ISO2>
      <ogr:ISO3>ETH</ogr:ISO3>
      <ogr:NAMEen>Ethiopia</ogr:NAMEen>
      <ogr:GBD_ID>179</ogr:GBD_ID>
      <ogr:GBD_NAME>Ethiopia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.85">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>59.922430731 20.62316451</gml:lowerCorner><gml:upperCorner>70.070685324 31.56952478</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.85"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.85.0"><gml:exterior><gml:LinearRing><gml:posList>69.027260641 28.954077189 68.204003398 28.663758993 67.685843812 30.00941329 66.837549337 29.089159383 66.108058981 29.900169311 64.991434632 29.588147013 64.219698792 30.558475382 63.741536764 29.980784546 62.905928854 31.56952478 61.245900981 29.203157593 60.553045966 27.807871941 59.922430731 23.247080925 60.647040106 21.379649285 62.858465887 21.226735873 63.900620835 23.372813347 65.484116929 25.351817254 65.822699286 24.16309655 67.882162578 23.498854614 69.036355693 20.62316451 69.1261383161 21.5785150411 68.563412984 24.860942016 69.696677145 26.031981649 70.070685324 27.897294149 69.464391582 29.344544311 69.027260641 28.954077189</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>FI</ogr:ISO2>
      <ogr:ISO3>FIN</ogr:ISO3>
      <ogr:NAMEen>Finland</ogr:NAMEen>
      <ogr:GBD_ID>79</ogr:GBD_ID>
      <ogr:GBD_NAME>Finland</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.86">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-18.4084317479 177.2276707772</gml:lowerCorner><gml:upperCorner>-16.1737936533 179.9958605798</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.86"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.86.0"><gml:exterior><gml:LinearRing><gml:posList>-16.1737936533 179.9888979356 -16.7109442262 179.9958605798 -17.0587167201 178.8636084213 -16.66250139 178.6021970337 -16.1737936533 179.9888979356</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.86.1"><gml:exterior><gml:LinearRing><gml:posList>-17.351739191 178.272715691 -17.7043503911 178.6939175738 -18.1578145149 178.7654375502 -18.4084317479 178.3861250541 -18.3718810641 177.7593361195 -18.0911977729 177.2763311399 -17.620102069 177.2276707772 -17.3029781591 177.5761763198 -17.351739191 178.272715691</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>FJ</ogr:ISO2>
      <ogr:ISO3>FJI</ogr:ISO3>
      <ogr:NAMEen>Fiji</ogr:NAMEen>
      <ogr:GBD_ID>22</ogr:GBD_ID>
      <ogr:GBD_NAME>Fiji</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.87">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-52.2600741698 -60.6898708844</gml:lowerCorner><gml:upperCorner>-51.0285129026 -57.4890258905</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.87"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.87.0"><gml:exterior><gml:LinearRing><gml:posList>-51.3165537325 -59.2002944476 -52.2600741698 -60.6898708844 -51.0285129026 -60.2482799182 -51.3165537325 -59.2002944476</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.87.1"><gml:exterior><gml:LinearRing><gml:posList>-51.320631076 -58.7825450682 -51.39430623 -57.4890258905 -51.8373545719 -57.8315574331 -52.2127828999 -59.1395692115 -52.2197044851 -60.0192251877 -51.320631076 -58.7825450682</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>FK</ogr:ISO2>
      <ogr:ISO3>FLK</ogr:ISO3>
      <ogr:NAMEen>Falkland Islands</ogr:NAMEen>
      <ogr:GBD_ID>FLK</ogr:GBD_ID>
      <ogr:GBD_NAME>Falkland Islands</ogr:GBD_NAME>
      <ogr:WB_ECONOMY xsi:nil="true"/>
      <ogr:WB_GROUP xsi:nil="true"/>
      <ogr:CONTINENT xsi:nil="true"/>
      <ogr:WB_REGION xsi:nil="true"/>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.88">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-3.9368561899 9.03003991</gml:lowerCorner><gml:upperCorner>2.165760396 14.498990519</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.88"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.88.0"><gml:exterior><gml:LinearRing><gml:posList>2.161057841 13.294567912 1.221787008 13.250229533 1.380847066 14.183867635 0.913227031 14.468088013 -0.2054127 13.831227661 -0.630916442 14.498990519 -1.548689881 14.463127075 -2.2887277819 14.0603219595 -2.1680043779 13.5385407334 -2.366417745 12.966112508 -2.4526288205 11.9007915951 -3.30320811 11.944366089 -3.9368561899 11.1140163041 -2.700431046 9.902437516 -1.299086196 9.03003991 0.626166083 9.315928582 0.9983539399 9.8043711812 0.999164937 11.336341187 2.165760396 11.322078491 2.161057841 13.294567912</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GA</ogr:ISO2>
      <ogr:ISO3>GAB</ogr:ISO3>
      <ogr:NAMEen>Gabon</ogr:NAMEen>
      <ogr:GBD_ID>173</ogr:GBD_ID>
      <ogr:GBD_NAME>Gabon</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.89">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>50.194322007 -7.8771410548</gml:lowerCorner><gml:upperCorner>58.652044989 1.771169467</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.89"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.89.0"><gml:exterior><gml:LinearRing><gml:posList>54.097927151 -6.269886848 54.5176808423 -7.8771410548 55.069322007 -7.24710039 55.212388414 -6.100982226 54.657456773 -5.536610481 54.097927151 -6.269886848</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.89.1"><gml:exterior><gml:LinearRing><gml:posList>58.652044989 -3.347889778 57.943060614 -4.077626106 57.473578192 -1.759348111 56.058091539 -3.178374804 55.585109768 -1.630848762 53.086493231 0.329844597 52.485907294 1.771169467 51.824611721 0.977061394 51.131008205 1.357188347 50.747381903 0.271006707 50.84365469 -1.02074134 50.548163153 -3.494862434 50.226019598 -3.64561927 50.194322007 -5.437977668 51.39850495 -3.539173957 51.916937567 -5.288929817 52.279282945 -4.197132942 53.228094794 -4.151519335 53.377264716 -3.185210741 54.12518952 -2.795969205 55.008693752 -5.170806444 55.495591539 -4.617787239 56.0701775276 -5.5328783462 56.9117629111 -6.183059188 57.84393952 -5.807362434 58.628322658 -4.988189257 58.652044989 -3.347889778</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GB</ogr:ISO2>
      <ogr:ISO3>GBR</ogr:ISO3>
      <ogr:NAMEen>United Kingdom</ogr:NAMEen>
      <ogr:GBD_ID>95</ogr:GBD_ID>
      <ogr:GBD_NAME>United Kingdom</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>1. Developed region: G7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.90">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>41.04483429 39.9859763554</gml:lowerCorner><gml:upperCorner>43.538971456 46.496779012</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.90"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.90.0"><gml:exterior><gml:LinearRing><gml:posList>41.89044159 46.430891561 41.04483429 46.496779012 41.290452373 45.002399944 41.106587626 43.4404281 41.572347311 42.819949178 41.5142276065 41.520762566 42.64057038 41.500173373 43.3889896097 39.9859763554 43.538971456 40.651916138 43.228990377 42.422867473 42.552209982 43.939725789 42.75950999 44.859359579 41.89044159 46.430891561</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GE</ogr:ISO2>
      <ogr:ISO3>GEO</ogr:ISO3>
      <ogr:NAMEen>Georgia</ogr:NAMEen>
      <ogr:GBD_ID>35</ogr:GBD_ID>
      <ogr:GBD_NAME>Georgia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.91">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>4.737127997 -3.262509318</gml:lowerCorner><gml:upperCorner>11.134980367 1.185394727</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.91"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.91.0"><gml:exterior><gml:LinearRing><gml:posList>11.134980367 -0.166109171 10.283223572 0.3965955 9.575747376 0.230197388 7.323585104 0.645675903 6.832245585 0.51927535 6.100490627 1.185394727 4.737127997 -2.090565559 5.091335354 -3.119618293 5.107814846 -3.115305142 5.149115302 -2.843698697 6.617142436 -3.262509318 8.209267477 -2.506328084 9.488724263 -2.689210978 10.985842184 -2.750705933 10.994032898 -0.695999309 11.134980367 -0.166109171</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GH</ogr:ISO2>
      <ogr:ISO3>GHA</ogr:ISO3>
      <ogr:NAMEen>Ghana</ogr:NAMEen>
      <ogr:GBD_ID>207</ogr:GBD_ID>
      <ogr:GBD_NAME>Ghana</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.92">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>7.190208232 -15.020985481</gml:lowerCorner><gml:upperCorner>12.673387757 -7.865380818</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.92"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.92.0"><gml:exterior><gml:LinearRing><gml:posList>12.403895162 -11.388421591 12.0438540216 -10.4995113795 12.3313548474 -9.2862803666 11.657945862 -8.847233033 11.367653504 -8.375323852 10.161990662 -7.989662639 9.409917705 -7.865380818 7.544295146 -8.228976196 7.5579894 -8.48544633 7.190208232 -9.125407268 8.343471172 -9.499131226 8.484625346 -10.28223588 9.996005961 -11.272666382 9.860381165 -12.508327393 9.041489976 -13.301096158 9.774237372 -13.594309049 10.9674746765 -15.020985481 11.510719706 -14.686773235 11.709829 -13.729932414 12.0269454498 -13.7860969802 12.2165313603 -14.004229347 12.3880417559 -13.7581500333 12.673387757 -13.728278768 12.305606588 -12.36092037 12.403895162 -11.388421591</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GN</ogr:ISO2>
      <ogr:ISO3>GIN</ogr:ISO3>
      <ogr:NAMEen>Guinea</ogr:NAMEen>
      <ogr:GBD_ID>208</ogr:GBD_ID>
      <ogr:GBD_NAME>Guinea</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.93">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>13.065008856 -16.753651496</gml:lowerCorner><gml:upperCorner>13.819984436 -13.9662047098</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.93"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.93.0"><gml:exterior><gml:LinearRing><gml:posList>13.065008856 -16.753651496 13.5869141589 -16.5613991701 13.819984436 -15.097704224 13.5829768153 -14.2394964658 13.4090589101 -13.9662047098 13.3053416455 -14.30595433 13.589972636 -15.137650106 13.470056408 -15.6552237486 13.1305108817 -16.0972412049 13.065008856 -16.753651496</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GM</ogr:ISO2>
      <ogr:ISO3>GMB</ogr:ISO3>
      <ogr:NAMEen>The Gambia</ogr:NAMEen>
      <ogr:GBD_ID>206</ogr:GBD_ID>
      <ogr:GBD_NAME>Gambia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.94">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>10.9674746765 -16.7284367771</gml:lowerCorner><gml:upperCorner>12.6794339 -13.728278768</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.94"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.94.0"><gml:exterior><gml:LinearRing><gml:posList>10.9674746765 -15.020985481 11.733140367 -15.551380989 12.332530837 -16.7284367771 12.6794339 -15.195114299 12.673387757 -13.728278768 12.3880417559 -13.7581500333 12.2165313603 -14.004229347 12.0269454498 -13.7860969802 11.709829 -13.729932414 11.510719706 -14.686773235 10.9674746765 -15.020985481</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GW</ogr:ISO2>
      <ogr:ISO3>GNB</ogr:ISO3>
      <ogr:NAMEen>Guinea-Bissau</ogr:NAMEen>
      <ogr:GBD_ID>209</ogr:GBD_ID>
      <ogr:GBD_NAME>Guinea-Bissau</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.95">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>34.931626695 19.999847852</gml:lowerCorner><gml:upperCorner>41.713036397 26.636595907</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.95"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.95.0"><gml:exterior><gml:LinearRing><gml:posList>35.527167059 23.9013778 35.335679429 25.766368035 35.03904857 26.24097741 34.931626695 24.738536004 35.294134833 23.51823978 35.527167059 23.9013778</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.95.1"><gml:exterior><gml:LinearRing><gml:posList>39.0202150419 23.3945924328 38.5876067674 24.3059331441 38.2876940665 24.2567517164 38.8061357936 23.1246102897 39.0202150419 23.3945924328</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.95.2"><gml:exterior><gml:LinearRing><gml:posList>40.841981383 20.9645643201 41.335772604 22.916977986 41.561650289 24.510162395 41.239396057 25.28572229 41.713036397 26.333358602 41.378457337 26.636595907 40.73847077 26.0439559255 41.006048895 25.039073113 40.6933797366 23.6628395977 40.3571650196 23.7052774311 40.2249816872 23.328753982 40.502142645 22.821625196 40.054266669 22.571299675 39.17963288 23.343760613 38.784816799 22.769541863 38.210878804 23.9943179478 37.3485975868 23.1240659958 36.6135093274 23.3695043854 36.432521877 23.196055535 37.021185614 22.153168165 36.72671133 21.87403405 37.837388414 21.122080925 38.410589911 21.987640821 38.303900458 21.155609571 39.693508205 19.999847852 40.6872807501 21.0212289216 40.841981383 20.9645643201</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GR</ogr:ISO2>
      <ogr:ISO3>GRC</ogr:ISO3>
      <ogr:NAMEen>Greece</ogr:NAMEen>
      <ogr:GBD_ID>82</ogr:GBD_ID>
      <ogr:GBD_NAME>Greece</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.96">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>60.038804429 -72.895171679</gml:lowerCorner><gml:upperCorner>83.61347077 -13.5405579234</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.96"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.96.0"><gml:exterior><gml:LinearRing><gml:posList>69.928656317 -52.728098111 69.477280992 -52.14159095 69.2945716946 -53.2227918485 69.6443144053 -54.7423723185 70.313299872 -54.453033007 69.928656317 -52.728098111</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.96.1"><gml:exterior><gml:LinearRing><gml:posList>70.6176588016 -25.4453183621 70.2035582498 -27.3220428713 70.406161237 -27.7720348468 70.6610005766 -27.6379925077 71.0080309779 -25.4074525974 70.9120118763 -24.5850077886 70.5151144212 -24.3579793257 70.6176588016 -25.4453183621</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.96.2"><gml:exterior><gml:LinearRing><gml:posList>83.61347077 -32.207875129 82.626288153 -21.339507616 82.083441473 -21.388417121 81.517482815 -21.256825325 81.388169664 -17.505930142 81.83201732 -15.792713996 81.2086793106 -13.5405579234 80.404689846 -15.789173957 80.275736117 -19.076726099 79.148993231 -18.955189582 78.1131913784 -19.0567168931 77.208482164 -18.324452278 76.255601304 -21.704497851 76.25796133 -19.937733528 75.225531317 -19.427235481 74.438706773 -21.771636523 73.449042059 -20.543283658 73.250433661 -22.314076301 73.597398179 -23.987782356 73.240871486 -26.150624153 72.423407294 -24.546376106 71.490179755 -21.749867317 70.544867255 -21.476796028 70.440578518 -23.345448371 71.081366278 -24.264881965 71.188544012 -25.580799934 70.881293036 -27.920887825 70.372015692 -28.232899543 69.992010809 -27.283680793 70.413234768 -25.305287239 70.115057684 -22.371652799 68.69554271 -26.285389778 68.126044012 -30.008168098 68.050360419 -31.995594856 66.752142645 -33.948068814 65.935492255 -36.073801236 65.815863348 -38.121449348 65.099758205 -41.057118293 63.726019598 -40.513295051 62.838039455 -42.426014778 60.080755927 -43.138172981 60.038804429 -44.515492317 60.6519825778 -45.6330221165 60.9001607363 -47.0464899092 61.238959052 -48.640614387 63.184759833 -51.056304491 64.256348502 -52.161476679 65.686183986 -53.2392472 67.077704169 -53.964914517 68.525946356 -52.616444465 68.563910223 -51.148833788 69.7853513301 -50.9622966118 70.714178778 -54.577504036 70.9849733462 -53.8776959431 70.7718358939 -52.4222895608 71.4267579154 -52.6886996941 71.717352606 -54.081613736 71.682033596 -55.904530403 72.7039545354 -56.2745739393 73.257144468 -56.0305312812 74.0794451977 -56.7923383305 74.8701301868 -57.7690046067 75.9064786153 -59.8178492352 76.376898505 -63.476918098 76.079413153 -68.432525194 76.797796942 -70.40746009 77.695896942 -71.2968307162 78.153957424 -72.895171679 78.521226304 -72.590199348 78.9890203036 -67.2635027158 79.3554882367 -66.0285393696 79.8029113481 -68.6447398652 80.1370100677 -66.8240503703 81.4897374294 -61.0006710586 82.372707424 -54.440744595 81.789455471 -50.510243293 82.509914455 -51.107248502 81.789862372 -45.156076627 82.406887111 -43.727202929 83.61347077 -32.207875129</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GL</ogr:ISO2>
      <ogr:ISO3>GRL</ogr:ISO3>
      <ogr:NAMEen>Greenland</ogr:NAMEen>
      <ogr:GBD_ID>349</ogr:GBD_ID>
      <ogr:GBD_NAME>Greenland</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.97">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>13.7289030479 -92.246245898</gml:lowerCorner><gml:upperCorner>17.81431427 -88.220936653</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.97"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.97.0"><gml:exterior><gml:LinearRing><gml:posList>17.81431427 -89.160496175 15.95494456 -89.234135092 15.8429139169 -88.6941161154 15.725653387 -88.220936653 14.86731069 -89.231447917 14.415477804 -89.36162085 13.7289030479 -90.092981728 13.955679359 -91.316461252 14.546291408 -92.246245898 15.289507141 -92.204651449 16.06878774 -91.723776409 16.070699768 -90.485738282 16.367270813 -90.379853272 17.254968567 -91.430771037 17.801963603 -90.990901246 17.81431427 -89.160496175</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GT</ogr:ISO2>
      <ogr:ISO3>GTM</ogr:ISO3>
      <ogr:NAMEen>Guatemala</ogr:NAMEen>
      <ogr:GBD_ID>128</ogr:GBD_ID>
      <ogr:GBD_NAME>Guatemala</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.98">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>1.377798157 -61.379607911</gml:lowerCorner><gml:upperCorner>8.558010158 -56.4944124319</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.98"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.98.0"><gml:exterior><gml:LinearRing><gml:posList>5.484930731 -57.247670051 4.151143087 -58.067691203 2.0374662082 -56.4944124319 2.021453959 -57.104494182 1.377798157 -59.24214148 1.851309306 -59.74968156 2.694048564 -60.000079712 3.931905823 -59.5292299 4.513007304 -60.148778646 5.085943909 -59.983104005 5.202138367 -60.739853679 5.905299581 -61.379607911 6.715844421 -61.129829875 7.105252177 -60.292335775 7.820867412 -60.503278972 8.287763977 -59.815594849 8.558010158 -60.020985481 8.05874258 -59.16515052 6.821702861 -58.067863405 6.085109768 -57.167307095 5.484930731 -57.247670051</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>GY</ogr:ISO2>
      <ogr:ISO3>GUY</ogr:ISO3>
      <ogr:NAMEen>Guyana</ogr:NAMEen>
      <ogr:GBD_ID>113</ogr:GBD_ID>
      <ogr:GBD_NAME>Guyana</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>South America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.99">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>12.981553453 -89.36162085</gml:lowerCorner><gml:upperCorner>16.003729559 -83.1304444725</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.99"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.99.0"><gml:exterior><gml:LinearRing><gml:posList>14.9970120213 -83.1304444725 14.61947052 -84.482693848 14.5113094451 -85.008517904 13.847683004 -85.824162354 13.768256327 -86.727233847 13.314201355 -86.701860718 12.981553453 -87.314035611 13.406561591 -87.817168749 13.814997661 -87.703608358 13.851197002 -88.496815755 14.415477804 -89.36162085 14.86731069 -89.231447917 15.725653387 -88.220936653 15.922919012 -87.722157356 15.78139883 -86.338286913 16.003729559 -85.91917884 15.811010146 -84.294638383 15.360093492 -84.082508918 14.9970120213 -83.1304444725</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>HN</ogr:ISO2>
      <ogr:ISO3>HND</ogr:ISO3>
      <ogr:NAMEen>Honduras</ogr:NAMEen>
      <ogr:GBD_ID>129</ogr:GBD_ID>
      <ogr:GBD_NAME>Honduras</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.100">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>42.416327216 13.589528842</gml:lowerCorner><gml:upperCorner>46.50171051 19.01582076</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.100"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.100.0"><gml:exterior><gml:LinearRing><gml:posList>42.890936591 17.653330925 42.559212138 18.437354777 42.416327216 18.496429884 42.890936591 17.653330925</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.100.1"><gml:exterior><gml:LinearRing><gml:posList>46.50171051 16.515301554 45.955697327 17.34532841 45.931202698 18.901305786 44.865634664 19.01582076 45.284523825 16.924372193 45.0689690795 16.0143286918 44.5932791617 15.9833981709 43.5347114139 17.0680060084 42.942084052 17.580658399 43.676499742 15.931162957 44.264349677 15.110362175 45.1941602204 14.5430529039 45.2771217887 14.2437200969 44.8210378706 14.0808634866 44.8628054691 13.8959604187 45.488836981 13.589528842 45.598722106 14.4520942552 45.4768970263 15.3608683482 45.8048974331 15.409000313 45.9452217611 15.6345859905 46.2256765525 15.7021412095 46.50171051 16.515301554</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>HR</ogr:ISO2>
      <ogr:ISO3>HRV</ogr:ISO3>
      <ogr:NAMEen>Croatia</ogr:NAMEen>
      <ogr:GBD_ID>46</ogr:GBD_ID>
      <ogr:GBD_NAME>Croatia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.101">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>18.03925202 -74.159982877</gml:lowerCorner><gml:upperCorner>19.93768952 -71.757435676</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.101"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.101.0"><gml:exterior><gml:LinearRing><gml:posList>19.710109768 -71.757435676 18.6014749709 -72.006122552 18.490400409 -71.9078776579 18.03925202 -71.776234504 18.237534898 -72.071766731 18.258734442 -73.646311002 18.659165757 -74.159982877 18.4910551162 -73.1319747531 18.8797286218 -72.761092642 19.93768952 -73.167103645 19.710109768 -71.757435676</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>HT</ogr:ISO2>
      <ogr:ISO3>HTI</ogr:ISO3>
      <ogr:NAMEen>Haiti</ogr:NAMEen>
      <ogr:GBD_ID>114</ogr:GBD_ID>
      <ogr:GBD_NAME>Haiti</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.102">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>45.931202698 16.094035278</gml:lowerCorner><gml:upperCorner>48.526083069 22.877600546</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.102"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.102.0"><gml:exterior><gml:LinearRing><gml:posList>48.404798483 22.132839803 47.946738587 22.877600546 47.492941997 21.988869263 46.27852 21.134864542 46.108091126 20.242825969 45.931202698 18.901305786 45.955697327 17.34532841 46.50171051 16.515301554 46.862773743 16.094035278 48.005443014 17.14833785 47.750006409 17.825712524 48.12962148 19.884294881 48.526083069 20.481674439 48.404798483 22.132839803</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>HU</ogr:ISO2>
      <ogr:ISO3>HUN</ogr:ISO3>
      <ogr:NAMEen>Hungary</ogr:NAMEen>
      <ogr:GBD_ID>48</ogr:GBD_ID>
      <ogr:GBD_NAME>Hungary</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.103">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-10.5342761419 95.0874460499</gml:lowerCorner><gml:upperCorner>5.605129299 140.977161906</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.103"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.0"><gml:exterior><gml:LinearRing><gml:posList>-9.357598566 120.011729363 -9.5659590358 120.9045030197 -9.9768628434 121.3781656926 -10.1538118716 120.6592638547 -9.8724788681 119.93157255 -9.570489191 118.941579623 -9.357598566 120.011729363</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.1"><gml:exterior><gml:LinearRing><gml:posList>-9.485772394 125.061615431 -10.16383229 124.418467644 -10.5342761419 123.1589178782 -9.9395229967 122.9888335432 -9.443525747 123.6361778499 -9.4799588898 124.2663267197 -8.9708519321 124.4749225128 -8.8668875148 124.8711070826 -9.485772394 125.061615431</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.2"><gml:exterior><gml:LinearRing><gml:posList>-8.3094082173 116.8220393006 -8.8437389181 116.5878729866 -8.7470376448 116.0597964755 -8.2715223954 116.2425350082 -8.3094082173 116.8220393006</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.3"><gml:exterior><gml:LinearRing><gml:posList>-8.100355727 117.971202019 -8.309014581 118.988291863 -8.629815363 119.041514519 -8.6652097526 118.3240530087 -8.8282260237 117.4530050853 -8.7361759482 116.9768120581 -8.2968667333 117.1429527274 -8.100355727 117.971202019</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.4"><gml:exterior><gml:LinearRing><gml:posList>-7.9576655248 124.0396213573 -8.1698311081 124.0382250872 -8.454941538 122.9923471708 -8.610935154 122.80738366 -8.9854390002 121.4523199097 -8.870782159 119.919932488 -8.4218961518 119.7413577617 -8.29273854 120.212901238 -8.3556194866 121.5454394141 -7.9576655248 124.0396213573</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.5"><gml:exterior><gml:LinearRing><gml:posList>-8.1509154603 115.4945819273 -8.4326059926 115.904171452 -8.6462177799 115.5349949521 -8.5339159711 115.0756822696 -8.3238220928 114.7455164489 -8.1663034842 114.761840929 -8.1509154603 115.4945819273</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.6"><gml:exterior><gml:LinearRing><gml:posList>-8.1540021443 138.524683893 -8.4592834492 137.4247996908 -7.7657635599 137.2308023723 -7.2838068747 137.7258830141 -7.1296386173 138.5411519578 -7.5932902226 138.8036047339 -8.1540021443 138.524683893</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.7"><gml:exterior><gml:LinearRing><gml:posList>-6.8905084416 115.0004305672 -7.1967395664 114.6602269022 -7.233778154 113.548984206 -6.907166788 113.0419858155 -6.8905084416 115.0004305672</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.8"><gml:exterior><gml:LinearRing><gml:posList>-6.009535415 106.1591903 -6.082081685 107.3527192309 -6.3697732532 108.4475821574 -6.7998858712 108.9999301014 -6.946384373 110.472422722 -6.5680611691 111.045415981 -6.904554946 112.603770379 -7.612214994 113.2223920978 -7.830987238 114.467295769 -8.409112238 114.358653191 -8.775567316 114.597178582 -8.2772763 113.19792728 -8.447035415 112.672699415 -8.111911717 110.476735873 -7.729518572 109.2532341519 -7.817471295 108.326829456 -7.410957331 106.522662137 -6.982961613 106.537149257 -6.808770441 105.453623894 -6.4860698599 105.3405580905 -6.009535415 106.1591903</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.9"><gml:exterior><gml:LinearRing><gml:posList>-5.424248956 134.581228061 -5.289621535 135.1326797359 -6.1406059336 135.1968436142 -6.6571383156 135.1053809672 -6.8193528947 134.4078538016 -6.1396887671 134.4969495579 -5.424248956 134.581228061</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.10"><gml:exterior><gml:LinearRing><gml:posList>-3.1849267554 127.0728399293 -3.606052342 127.243418816 -3.859378356 126.742185315 -3.7562365395 126.1148511258 -3.24819134 125.991742403 -3.011460149 126.5009165999 -3.1849267554 127.0728399293</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.11"><gml:exterior><gml:LinearRing><gml:posList>-2.8556457014 129.1027576391 -3.3835850213 131.1018798655 -3.865004165 130.841563347 -3.5322755978 129.9073022644 -3.403574328 129.0251339031 -3.43728767 128.1294097718 -2.9452356455 127.7908118326 -2.8556457014 129.1027576391</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.12"><gml:exterior><gml:LinearRing><gml:posList>-2.549899998 107.848155144 -2.7201736024 108.2352418629 -3.0398468874 108.2964344173 -3.2105936749 107.9073488446 -3.05084923 107.4428729183 -2.6685683112 107.4312590983 -2.549899998 107.848155144</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.13"><gml:exterior><gml:LinearRing><gml:posList>-1.635674738 124.529551629 -1.606493649 127.2127486716 -2.0461953028 126.9386061434 -2.004082941 124.406016472 -1.635674738 124.529551629</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.14"><gml:exterior><gml:LinearRing><gml:posList>-1.58993906 135.477305535 -1.1260187943 135.3121883572 -1.3019478166 136.0998124881 -1.2391152269 136.9971347938 -1.6740080772 136.9849510559 -1.7051194852 136.1701774255 -1.58993906 135.477305535</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.15"><gml:exterior><gml:LinearRing><gml:posList>-1.489190363 105.875743035 -2.3156317222 106.4462602505 -2.6152677606 106.9252860531 -2.8958004683 106.8520812985 -3.0630666178 106.3234355671 -2.7316453828 106.1537256181 -2.196508879 105.8074532962 -2.0965900716 105.1421768827 -1.646238679 105.0320983398 -1.648695571 105.349864129 -1.489190363 105.875743035</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.16"><gml:exterior><gml:LinearRing><gml:posList>-0.7890127964 98.8634406359 -1.3143477613 99.2678556631 -1.7277054524 99.4530374178 -1.8096315617 99.1354670139 -1.514800823 98.7718058602 -1.0347139752 98.4150010684 -0.7272988787 98.5057997463 -0.7890127964 98.8634406359</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.17"><gml:exterior><gml:LinearRing><gml:posList>-2.600518488 140.974457227 -6.896632182 140.977161906 -9.106133722 140.976980014 -8.108575128 139.93238366 -8.138360284 138.83855228 -7.559991144 139.093028191 -5.419610284 138.074880405 -4.51962656 135.970469597 -4.461195571 135.227793816 -3.8056091987 133.8914561048 -4.111993097 132.969411655 -3.554131769 132.931813998 -2.676446222 132.20191491 -2.3585273769 132.9115392319 -2.105157159 132.073415561 -1.520440363 131.811045769 -1.446221613 130.960297071 -0.819431248 131.243011915 -0.360039972 132.709157748 -0.723239842 133.977224155 -2.862074477 134.4638778 -3.36305104 135.470469597 -2.219659113 136.403819207 -2.074395441 137.226328972 -1.48414479 137.794444207 -2.372002863 139.864756707 -2.600518488 140.974457227</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.18"><gml:exterior><gml:LinearRing><gml:posList>1.7200491332 97.4136980152 0.986070054 97.939219597 0.572739976 97.704112175 1.5159783697 96.9947557273 1.7200491332 97.4136980152</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.19"><gml:exterior><gml:LinearRing><gml:posList>1.674546617 125.061696811 1.4091246306 125.3887999672 0.398871161 124.321787957 0.516302802 123.074229363 0.2892154189 120.7618138113 -0.3474546955 120.4542678694 -1.394707941 120.675954623 -0.85930755 121.500661655 -0.5054612683 123.2593806208 -0.5894456828 124.0681040548 -1.0248249187 123.6607047191 -1.3112195353 122.3695523702 -1.908135675 121.692149285 -2.926853123 122.312510613 -3.556573175 122.200531446 -4.195977472 122.907074415 -4.656426691 121.484629754 -4.087090753 121.62769616 -3.53915781 120.893809441 -2.759047133 121.080414259 -3.21461354 120.387868686 -5.509535415 120.3232528 -5.355645441 119.352793816 -4.314548435 119.626800977 -3.427911066 119.297618035 -3.090915623 118.764170769 -1.970147394 119.336436394 -1.214450779 119.328379754 -0.633884373 119.733653191 0.742865302 120.062836134 1.0843454301 120.3427084667 1.353664455 120.9091903 1.120917059 121.480723504 0.9581424688 123.7805209633 1.674546617 125.061696811</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.20"><gml:exterior><gml:LinearRing><gml:posList>1.9159206868 128.1711790565 1.1588421193 127.8601626426 1.6418114367 128.7671442768 1.3898983414 129.1040057498 0.6879450699 128.2599383365 0.5665038449 129.4004083518 0.0887002723 129.2594276753 0.0300477703 128.2364263815 -0.6959794658 128.6362681974 -1.0724876392 128.4181497387 -0.3434792186 127.4610034348 0.4221962287 127.2098815755 1.1795134837 127.2107636389 2.2322579293 127.6571492473 1.9159206868 128.1711790565</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.21"><gml:exterior><gml:LinearRing><gml:posList>4.159654039 117.567149285 3.434759833 117.446543816 2.262030341 118.091481967 1.885931708 117.862559441 0.962469794 119.0029403 0.811753648 117.81128991 -0.525160415 117.441416863 -1.765313409 116.379567905 -2.224216404 116.603282097 -3.608819269 115.967946811 -4.170505467 114.624766472 -3.7257150697 114.5350377453 -3.45680104 113.632497592 -3.2380021703 113.1191597762 -3.423760675 112.618011915 -3.545098566 111.822032097 -3.0434641002 111.3476283274 -3.002862238 110.263194207 -1.103610935 109.922699415 -0.853610935 109.273203972 0.010484117 109.300466342 0.83222077 108.855479363 1.560614325 109.095957879 2.083238023 109.645274285 1.403274638 109.84283492 0.851370341 110.553902629 1.008466695 111.823075806 1.449034119 112.1800566 1.537013244 113.023415975 1.233259176 113.649527629 1.436037496 114.500638469 2.247202454 114.778761027 3.030823873 115.464093873 4.38921641 115.844122355 4.159654039 117.567149285</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.103.22"><gml:exterior><gml:LinearRing><gml:posList>5.605129299 95.50269616 5.281805731 96.126231316 5.251898505 97.496836785 4.080308335 98.305674675 3.180487372 99.750743035 2.156805731 100.561045769 2.291489976 101.050303582 1.386542059 102.118174675 0.774115302 102.4638778 0.239488023 103.760020379 -0.695896092 103.345550977 -1.031670831 104.377207879 -2.28492604 104.883311394 -2.401788019 105.62378991 -3.255547784 106.07300866 -5.828545831 105.782562696 -5.8892379744 105.5211916841 -5.5606868029 105.1168532427 -5.841892185 104.54175866 -4.951918227 103.692067905 -3.991469008 102.312510613 -2.587090753 101.104828321 -0.8163388 100.301442905 0.098578192 99.610850457 0.273749091 99.13933353 1.936468817 98.535166863 2.411566473 97.658213738 2.866197007 97.602712436 4.827337958 95.426280144 5.3899706436 95.0874460499 5.605129299 95.50269616</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>ID</ogr:ISO2>
      <ogr:ISO3>IDN</ogr:ISO3>
      <ogr:NAMEen>Indonesia</ogr:NAMEen>
      <ogr:GBD_ID>11</ogr:GBD_ID>
      <ogr:GBD_NAME>Indonesia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>4. Emerging region: MIKT</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.104">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>8.075995184 68.183034701</gml:lowerCorner><gml:upperCorner>35.495405579 97.323495728</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.104"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.104.0"><gml:exterior><gml:LinearRing><gml:posList>13.3138525463 92.7883808127 13.9513424516 93.0652817296 13.5463446117 93.3370677793 12.519232489 92.990570509 11.3007962104 92.7172711563 11.741359768 92.560313347 12.1822966338 92.5414591913 13.3138525463 92.7883808127</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.104.1"><gml:exterior><gml:LinearRing><gml:posList>30.1969693 80.996016887 29.806140442 80.476151978 28.837026469 80.036385539 27.494963684 82.752137085 27.32903066 84.577038615 27.028377177 84.640238892 26.571712545 85.82161381 26.353327942 87.326224813 26.453941955 88.074189494 27.860884501 88.118217814 28.10583079 88.610487508 27.315543111 88.89233077 26.821413066 89.096504354 26.69620107 89.817287232 26.854847718 92.035859822 27.759443665 91.632887004 27.915764873 92.628485148 28.67189443 93.446213013 29.319451803 94.630430135 29.052672221 95.281553182 29.368466899 96.14196578 28.217477723 97.323495728 27.341743063 96.758879028 27.257510478 96.142585897 26.475594381 95.040637655 26.049935608 95.15122522 23.842628479 94.099713176 24.041143494 93.302965942 23.129726257 93.374899536 21.995351054 92.978954712 21.977574362 92.57587854 23.731653341 92.150788208 22.944466248 91.586068156 23.612099915 91.140824015 24.405979106 92.107586711 25.182960104 92.001753377 25.14999054 90.364644003 25.374162903 89.79501469 25.931751608 89.83439213 26.45389028 88.461142619 25.908135478 88.0743962 25.167870585 88.924163452 24.645602722 88.021789592 24.223251242 88.74980717 23.649952901 88.540104208 23.183805644 88.954704224 21.5961373196 89.1214720516 21.558294989 88.249278191 21.8664491806 88.045146682 21.174709377 86.837250196 20.670721747 86.99488366 19.91034577 86.2685653 19.575181382 85.336680535 18.271551825 84.077403191 17.030340887 82.299327019 16.579779364 82.30738366 16.320135809 81.248545769 15.846747137 80.99887129 15.674546617 80.263194207 15.014146226 80.054535352 13.349066473 80.347829623 12.022040106 79.861827019 10.282212632 79.851328972 10.0043319472 79.262285305 9.274847723 78.956797722 8.965562242 78.239756707 8.39981611 78.0463219136 8.075995184 77.510915561 8.939113674 76.53809655 11.693670966 75.530772332 12.861761786 74.817556186 14.701605536 74.263194207 16.068915106 73.447032097 19.841986395 72.650075717 20.512437242 72.879161004 22.156724351 72.542165561 21.983547268 72.128103061 21.205064195 72.104746941 20.710150458 70.982432488 21.149237372 70.0576278 22.295070705 68.944590691 22.561957098 70.17847741 23.19155508 68.622569207 23.842108466 68.183034701 24.289216207 68.725913127 24.165218608 69.972038615 24.682577209 71.063858277 25.980327454 70.064642782 26.530113017 70.158073771 26.807770487 69.465092814 28.011469625 70.341938517 27.701462707 70.831572713 27.950207215 71.860863892 29.022622376 72.901523885 29.927321676 73.370332479 31.083788554 74.658832641 31.711192118 74.489437297 32.261675314 75.359048299 32.809910584 74.306502726 34.37095225 73.774855591 34.768886617 74.285832153 34.503812358 75.777110637 35.424740888 77.2516900425 35.495405579 77.800346314 34.658867493 78.273185669 34.372295837 78.922138306 33.552785136 78.781371704 32.6454245 79.476419719 32.2487005277 78.7887122705 30.938371074 79.577808879 30.1969693 80.996016887</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>IN</ogr:ISO2>
      <ogr:ISO3>IND</ogr:ISO3>
      <ogr:NAMEen>India</ogr:NAMEen>
      <ogr:GBD_ID>163</ogr:GBD_ID>
      <ogr:GBD_NAME>India</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>3. Emerging region: BRIC</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>South Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.105">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>51.576646226 -10.381947395</gml:lowerCorner><gml:upperCorner>55.160549221 -5.996571418</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.105"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.105.0"><gml:exterior><gml:LinearRing><gml:posList>55.069322007 -7.24710039 54.5176808423 -7.8771410548 54.097927151 -6.269886848 52.9649112 -5.996571418 51.9640433145 -6.689683212 51.576646226 -8.696156379 51.877671617 -10.381947395 52.5541041108 -10.091436754 53.146673895 -9.5600548398 53.412665106 -10.176503059 54.328762111 -9.843983528 54.276597398 -8.470285611 55.160549221 -8.27847246 55.069322007 -7.24710039</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>IE</ogr:ISO2>
      <ogr:ISO3>IRL</ogr:ISO3>
      <ogr:NAMEen>Ireland</ogr:NAMEen>
      <ogr:GBD_ID>84</ogr:GBD_ID>
      <ogr:GBD_NAME>Ireland</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.106">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>25.202093817 44.061372111</gml:lowerCorner><gml:upperCorner>39.685279032 63.141344441</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.106"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.106.0"><gml:exterior><gml:LinearRing><gml:posList>38.863701274 46.135870809 38.882175599 46.51403894 39.685279032 47.846877482 39.378941142 48.338940471 38.821559143 48.012965535 38.434068101 48.874278191 37.623358466 49.132497592 37.403876044 50.148773634 36.737005927 51.08863366 36.582709052 51.90902754 36.821112372 54.01433353 37.342759507 53.913747592 37.517452698 54.783663371 38.075894063 55.423107544 38.265081482 57.218346802 37.88504314 57.7902916814 37.694651184 58.80109257 36.637144674 60.342290487 36.647790019 61.075475708 35.61849884 61.269675741 33.638387146 60.511789185 31.494667868 60.821744425 31.381909892 61.661176391 30.831400859 61.785199829 29.85817861 60.844378703 28.542599997 61.892790161 28.258327942 62.739301799 27.225161031 62.800641723 26.625404359 63.141344441 26.242378642 61.857133423 25.202093817 61.588226759 25.329863349 59.903805956 25.777004299 57.326182488 26.850978908 57.024912957 27.201605536 56.353037957 26.498236395 54.801524285 26.708563544 53.716075066 27.646633205 52.423106316 27.991418381 51.380271138 30.195746161 50.073415561 30.401068427 48.948578321 29.961330471 48.531016472 30.994698385 47.672934611 31.761835022 47.831322876 32.458691712 47.343342326 32.959488017 46.273433472 33.973560486 45.380361369 34.444358623 45.417051635 35.828305156 46.281391642 35.976874899 45.359587443 37.141920065 44.766135295 37.968820496 44.221465698 39.400283508 44.061372111 39.561142994 44.8346988445 38.9452682836 45.2491653187 38.863701274 46.135870809</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>IR</ogr:ISO2>
      <ogr:ISO3>IRN</ogr:ISO3>
      <ogr:NAMEen>Iran</ogr:NAMEen>
      <ogr:GBD_ID>142</ogr:GBD_ID>
      <ogr:GBD_NAME>Iran (Islamic Republic of)</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.107">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>29.095744527 38.774511352</gml:lowerCorner><gml:upperCorner>37.358909404 48.531016472</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.107"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.107.0"><gml:exterior><gml:LinearRing><gml:posList>37.141920065 44.766135295 35.976874899 45.359587443 35.828305156 46.281391642 34.444358623 45.417051635 33.973560486 45.380361369 32.959488017 46.273433472 32.458691712 47.343342326 31.761835022 47.831322876 30.994698385 47.672934611 29.961330471 48.531016472 29.994045315 47.94800866 29.960911357 47.110488322 29.095744527 46.53243575 29.201836243 44.691824585 31.079861145 42.075395142 31.920533345 40.424126424 32.118144023 39.14637496 33.371685079 38.774511352 34.331497294 40.690466756 34.768473206 41.195655558 36.527383932 41.414866984 37.109984029 42.357238403 37.358909404 42.722177368 37.141920065 44.766135295</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>IQ</ogr:ISO2>
      <ogr:ISO3>IRQ</ogr:ISO3>
      <ogr:NAMEen>Iraq</ogr:NAMEen>
      <ogr:GBD_ID>143</ogr:GBD_ID>
      <ogr:GBD_NAME>Iraq</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.108">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>63.396714585 -24.539906379</gml:lowerCorner><gml:upperCorner>66.6859947673 -13.3505551021</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.108"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.108.0"><gml:exterior><gml:LinearRing><gml:posList>66.536607164 -16.0385105418 66.2231532665 -14.055118716 65.2726073668 -13.3505551021 64.405096747 -14.544911262 63.7863471306 -16.197778422 63.396714585 -18.732045051 63.875555731 -21.168324348 64.7255270037 -22.053374729 64.7491346385 -23.9014390224 65.2480485837 -22.8594740911 65.50800202 -24.539906379 66.2790028114 -24.5077115773 66.6859947673 -22.9514264805 66.0441178154 -21.714235818 65.6866923221 -20.5221696773 66.2490592523 -19.9009905122 66.0474954429 -17.9635446548 66.536607164 -16.0385105418</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>IS</ogr:ISO2>
      <ogr:ISO3>ISL</ogr:ISO3>
      <ogr:NAMEen>Iceland</ogr:NAMEen>
      <ogr:GBD_ID>83</ogr:GBD_ID>
      <ogr:GBD_NAME>Iceland</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.109">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>29.490057684 34.248350857</gml:lowerCorner><gml:upperCorner>33.4067217 35.821099894</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.109"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.109.0"><gml:exterior><gml:LinearRing><gml:posList>32.744346858 35.757590349 32.3757741523 35.6320930748 32.2566098282 35.1057683822 31.7510099591 35.0794930423 31.4818723424 35.0857968532 31.4113397213 35.4497660796 31.2308702979 35.4010613942 31.0915048678 35.4312948719 29.558986721 34.955577019 29.490057684 34.8867293625 31.211448958 34.248350857 31.5182216314 34.5445930809 31.583141323 34.481204125 33.089016018 35.105235222 33.4067217 35.821099894 32.744346858 35.757590349</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>IL</ogr:ISO2>
      <ogr:ISO3>ISR</ogr:ISO3>
      <ogr:NAMEen>Israel</ogr:NAMEen>
      <ogr:GBD_ID>85</ogr:GBD_ID>
      <ogr:GBD_NAME>Israel</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.110">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>17.7218074697 -78.4783277625</gml:lowerCorner><gml:upperCorner>18.454272251 -75.1831962621</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.110"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.110.0"><gml:exterior><gml:LinearRing><gml:posList>18.454272251 -76.7034708945 18.2967442564 -75.8865239825 17.9904066292 -75.1831962621 17.7999943094 -75.4966896776 17.8498546616 -76.1735955858 17.7218074697 -76.8975798464 17.8723482329 -77.5608941908 18.2597648926 -78.4783277625 18.4158567828 -77.790729114 18.454272251 -76.7034708945</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>JM</ogr:ISO2>
      <ogr:ISO3>JAM</ogr:ISO3>
      <ogr:NAMEen>Jamaica</ogr:NAMEen>
      <ogr:GBD_ID>115</ogr:GBD_ID>
      <ogr:GBD_NAME>Jamaica</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.111">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>29.189950664 34.9493851167</gml:lowerCorner><gml:upperCorner>33.371685079 39.14637496</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.111"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.111.0"><gml:exterior><gml:LinearRing><gml:posList>32.118144023 39.14637496 31.49099884 36.959531698 30.499483134 37.981071411 29.865516663 36.756236613 29.189950664 36.016436808 29.3516858265 34.9493851167 29.558986721 34.955577019 31.0915048678 35.4312948719 31.2308702979 35.4010613942 31.4113397213 35.4497660796 31.7653694425 35.5594024594 32.3757741523 35.6320930748 32.744346858 35.757590349 32.316788229 36.819385213 33.371685079 38.774511352 32.118144023 39.14637496</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>JO</ogr:ISO2>
      <ogr:ISO3>JOR</ogr:ISO3>
      <ogr:NAMEen>Jordan</ogr:NAMEen>
      <ogr:GBD_ID>144</ogr:GBD_ID>
      <ogr:GBD_NAME>Jordan</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.112">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>31.0031912 129.564138217</gml:lowerCorner><gml:upperCorner>45.7033298223 145.4350127251</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.112"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.112.0"><gml:exterior><gml:LinearRing><gml:posList>33.7852630008 130.9813035177 33.580236163 131.729537981 32.92359591 132.025474986 31.372626044 131.338226759 31.0031912 130.657722026 31.246299862 130.222204714 32.093451239 130.179860873 32.3791388814 130.4303056674 32.833312771 130.2203415316 32.694121637 129.834378031 33.21112702 129.564138217 33.7852630008 130.9813035177</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.112.1"><gml:exterior><gml:LinearRing><gml:posList>34.362494208 134.161631707 34.1393762803 134.8799162548 33.248329294 134.190255131 33.4391359169 133.535916772 32.8858625169 132.8307053199 33.466050523 132.381114129 33.9874007042 132.8313671297 33.965236721 133.510996941 34.362494208 134.161631707</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.112.2"><gml:exterior><gml:LinearRing><gml:posList>41.353195528 141.276095682 39.540472723 142.06934655 38.371527411 141.097829623 37.377142645 141.04184004 36.283095802 140.564107371 35.74241771 140.841644727 34.907356652 139.926395846 35.3141771 139.782153008 34.712282661 138.215989191 34.739813544 136.855967644 33.7903614861 136.1118296563 33.8460215217 135.1863123858 34.55812547 135.414379151 34.739813544 134.381358269 34.3733408758 133.3731560864 34.0512764756 131.0470503731 34.4036562391 131.0199537041 35.601484911 133.091261013 35.7350818704 134.611686648 35.526862684 135.810499536 37.2793486342 136.7291878658 37.531490165 137.295709888 36.9872317258 137.454498748 37.183789445 138.244894979 38.169134833 139.43238366 39.504669646 140.04857715 40.590155341 139.856781446 41.0692014442 140.545975004 41.353195528 141.276095682</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.112.3"><gml:exterior><gml:LinearRing><gml:posList>45.455715236 142.017751498 45.7033298223 142.9521029019 44.11347077 143.728282097 44.0575205995 145.4350127251 43.301459052 145.361664259 42.661078192 143.638845248 41.932277736 143.226084832 42.4033290249 141.6804755914 42.2027249836 140.8211602301 41.6829255132 141.0216983414 41.401109117 140.208994988 42.663397528 139.876963738 43.14085521 141.169118686 45.455715236 142.017751498</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>JP</ogr:ISO2>
      <ogr:ISO3>JPN</ogr:ISO3>
      <ogr:NAMEen>Japan</ogr:NAMEen>
      <ogr:GBD_ID>67</ogr:GBD_ID>
      <ogr:GBD_NAME>Japan</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>1. Developed region: G7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.113">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>40.599486796 46.479312378</gml:lowerCorner><gml:upperCorner>55.434550273 87.323796021</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.113"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.113.0"><gml:exterior><gml:LinearRing><gml:posList>49.085273743 87.323796021 48.40758901 85.783683309 47.051831971 85.498636109 46.818073629 84.767104533 47.211537985 83.150355672 45.533190817 82.291493368 45.018958639 80.061706991 43.128040467 80.787864217 42.189518941 80.21032841 42.790980937 79.148274374 42.849659526 75.178594198 43.261701558 74.178758585 42.420926005 73.505518026 42.834053243 71.847738078 42.248145854 70.947793009 41.366985168 69.031579224 40.599486796 68.593001343 41.200380351 67.937434123 41.199191793 66.688208049 41.993484395 66.504136597 41.99761851 66.017241251 42.877203065 65.795239298 43.697359721 64.956530803 43.7543168338 63.8668029342 43.480628765 62.026115357 44.382821758 61.036304972 44.8477769646 60.0574978679 45.3664368028 59.9384183174 45.8377495941 60.4400627206 46.0920620042 60.4323324049 46.1677709675 60.1802851415 45.7152264177 59.7411264504 45.6857981258 59.1591200621 45.5445630245 58.5908979437 44.996221008 55.978422486 41.321716614 55.978422486 41.290814107 55.429515421 42.318064067 54.195275513 42.126628723 52.978708944 41.7488757414 52.4376706173 42.730780341 52.712901238 42.834947007 51.932790561 44.655340887 50.299082879 45.332586981 51.39722741 45.400702216 52.566661004 46.718003648 53.188161655 47.120917059 51.212575717 46.327879184 49.2271305193 47.769695129 48.044488159 47.819769592 47.119480021 48.410224508 46.479312378 49.201494039 46.756988397 49.820319316 46.899906861 50.452580465 47.556921021 50.1074706112 48.3054787052 50.57954946 48.67339034 51.635298971 50.581697632 51.503033142 53.346335083 50.763168457 55.1045814792 51.066263937 56.508002564 51.150031434 58.308409465 50.678148092 58.92594283 50.782482808 61.37209843 51.258396912 61.66365686 51.888875224 60.138065226 52.334997864 61.039922323 52.922171326 61.058422485 53.635745342 61.007572876 53.950248108 61.60050826 54.34304067 65.212949259 54.956129863 68.166050659 55.434550273 68.943160848 55.294145406 70.820358928 54.101556702 71.182300659 53.874852193 73.485260864 53.432553609 73.424075968 54.461172995 76.85239384 53.8425845084 76.9598554934 53.272330832 77.866802206 50.78811554 79.990083456 51.30201182 80.682754354 50.730341289 81.460226277 50.992650859 83.433955933 49.581522522 85.26309493 49.58720693 86.591799357 49.085273743 87.323796021</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>KZ</ogr:ISO2>
      <ogr:ISO3>KAZ</ogr:ISO3>
      <ogr:NAMEen>Kazakhstan</ogr:NAMEen>
      <ogr:GBD_ID>36</ogr:GBD_ID>
      <ogr:GBD_NAME>Kazakhstan</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.114">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-4.677504165 33.977078085</gml:lowerCorner><gml:upperCorner>5.0303758226 41.885019165</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.114"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.114.0"><gml:exterior><gml:LinearRing><gml:posList>4.619331563 35.920835409 4.432237041 36.844086548 3.612648824 38.10189091 3.40501292 39.536325317 3.867284444 39.848450969 4.28493337 40.763743937 3.977226054 41.885019165 2.814144593 40.965385376 -0.870798441 40.979751424 -1.696302993 41.535085483 -2.762627863 40.174978061 -4.430433852 39.540049675 -4.677504165 39.190603061 -3.513427836 37.599079223 -3.045962829 37.644864542 -1.0524528975 34.0741009683 -0.2645195722 34.2532441194 0.2459167412 33.9956852793 1.675945333 34.978670695 2.477317607 34.923583619 4.219691875 33.977078085 5.0303758226 35.4115983372 4.619331563 35.920835409</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>KE</ogr:ISO2>
      <ogr:ISO3>KEN</ogr:ISO3>
      <ogr:NAMEen>Kenya</ogr:NAMEen>
      <ogr:GBD_ID>180</ogr:GBD_ID>
      <ogr:GBD_NAME>Kenya</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.115">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>39.285096741 69.300348348</gml:lowerCorner><gml:upperCorner>43.261701558 80.21032841</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.115"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.115.0"><gml:exterior><gml:LinearRing><gml:posList>42.189518941 80.21032841 41.377527161 78.359898723 41.039511617 78.074954875 40.944685364 76.766817668 40.291701966 75.681819296 40.511636861 74.835359335 39.44834259 73.632642049 39.285096741 71.732913045 39.515625305 69.300348348 39.986914368 69.313990926 40.238371888 70.958955119 40.147886455 71.673071736 40.833852782 73.143291994 40.99935903 72.165134725 41.447290344 71.753170207 41.108137919 71.180130249 41.578341777 70.169287557 42.248145854 70.947793009 42.834053243 71.847738078 42.420926005 73.505518026 43.261701558 74.178758585 42.849659526 75.178594198 42.790980937 79.148274374 42.189518941 80.21032841</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>KG</ogr:ISO2>
      <ogr:ISO3>KGZ</ogr:ISO3>
      <ogr:NAMEen>Kyrgyzstan</ogr:NAMEen>
      <ogr:GBD_ID>37</ogr:GBD_ID>
      <ogr:GBD_NAME>Kyrgyzstan</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.116">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>10.419663804 102.332233928</gml:lowerCorner><gml:upperCorner>14.704581605 107.5203927</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.116"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.116.0"><gml:exterior><gml:LinearRing><gml:posList>10.419663804 104.451345248 10.500921942 103.62387129 11.063666083 103.67457116 10.933986721 103.095550977 11.645900783 102.913584832 13.564599508 102.332233928 14.295821025 103.085004517 14.345740458 105.184307903 13.917110291 105.924572795 14.590350851 106.512495565 14.323519592 106.946112508 14.704581605 107.5203927 12.343847148 107.514294882 11.677272848 106.435032593 11.770497131 106.015419962 11.015607808 105.752903687 10.914089457 105.0290719 10.419663804 104.451345248</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>KH</ogr:ISO2>
      <ogr:ISO3>KHM</ogr:ISO3>
      <ogr:NAMEen>Cambodia</ogr:NAMEen>
      <ogr:GBD_ID>10</ogr:GBD_ID>
      <ogr:GBD_NAME>Cambodia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.117">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>34.300441799 126.268646681</gml:lowerCorner><gml:upperCorner>38.624335028 129.554794958</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.117"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.117.0"><gml:exterior><gml:LinearRing><gml:posList>37.8278101612 126.6674589269 38.307223613 127.157488648 38.624335028 128.364919467 37.282171942 129.3310653 36.082477915 129.554794958 35.045950018 129.00040153 34.636177542 127.8206686059 34.300441799 126.603282097 35.137152411 126.268646681 35.9526458904 126.7635135809 36.788763739 126.356700066 37.1276836335 126.8571127528 37.8278101612 126.6674589269</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>KR</ogr:ISO2>
      <ogr:ISO3>KOR</ogr:ISO3>
      <ogr:NAMEen>South Korea</ogr:NAMEen>
      <ogr:GBD_ID>68</ogr:GBD_ID>
      <ogr:GBD_NAME>Republic of Korea</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>4. Emerging region: MIKT</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.118">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>41.873181662 20.064955688</gml:lowerCorner><gml:upperCorner>43.257412415 21.76438684</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.118"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.118.0"><gml:exterior><gml:LinearRing><gml:posList>42.246289302 21.56406571 41.873181662 20.567147258 42.546758118 20.064955688 42.82743866 20.34535201 43.257412415 20.8194316 42.669618836 21.76438684 42.246289302 21.56406571</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>XK</ogr:ISO2>
      <ogr:ISO3>KOS</ogr:ISO3>
      <ogr:NAMEen>Kosovo</ogr:NAMEen>
      <ogr:GBD_ID>KOS</ogr:GBD_ID>
      <ogr:GBD_NAME>Kosovo</ogr:GBD_NAME>
      <ogr:WB_ECONOMY xsi:nil="true"/>
      <ogr:WB_GROUP xsi:nil="true"/>
      <ogr:CONTINENT xsi:nil="true"/>
      <ogr:WB_REGION xsi:nil="true"/>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.119">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>28.533504944 46.53243575</gml:lowerCorner><gml:upperCorner>29.994045315 48.4327812271</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.119"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.119.0"><gml:exterior><gml:LinearRing><gml:posList>28.5404797232 48.4327812271 28.533504944 47.668128703 28.994587911 47.434086141 29.095744527 46.53243575 29.960911357 47.110488322 29.994045315 47.94800866 28.5404797232 48.4327812271</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>KW</ogr:ISO2>
      <ogr:ISO3>KWT</ogr:ISO3>
      <ogr:NAMEen>Kuwait</ogr:NAMEen>
      <ogr:GBD_ID>145</ogr:GBD_ID>
      <ogr:GBD_NAME>Kuwait</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.120">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>13.917110291 100.099295288</gml:lowerCorner><gml:upperCorner>22.446124573 107.657593628</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.120"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.120.0"><gml:exterior><gml:LinearRing><gml:posList>22.397548726 102.118655233 21.575325012 102.974106893 20.868391419 103.115286906 20.958825175 104.056676473 20.091797994 104.956879924 19.299908346 103.848420451 18.192714946 105.451423381 17.664039205 105.735282024 16.064188538 107.298959188 15.28242747 107.657593628 14.704581605 107.5203927 14.323519592 106.946112508 14.590350851 106.512495565 13.917110291 105.924572795 14.345740458 105.184307903 15.634602356 105.650997762 16.528914693 104.754515015 17.372790833 104.816423381 18.318443705 104.000039103 18.441459452 103.386639852 17.850074362 102.667613973 18.21379893 102.078502645 17.461674296 101.13230717 18.427791036 101.030297892 19.570279439 101.246202026 19.53710317 100.46196049 20.158021342 100.548673544 20.31780487 100.099295288 21.552690735 101.159023885 21.134473369 101.7179045 22.446124573 101.654549195 22.397548726 102.118655233</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>LA</ogr:ISO2>
      <ogr:ISO3>LAO</ogr:ISO3>
      <ogr:NAMEen>Laos</ogr:NAMEen>
      <ogr:GBD_ID>12</ogr:GBD_ID>
      <ogr:GBD_NAME>Lao People's Democratic Republic</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.121">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>33.089016018 35.105235222</gml:lowerCorner><gml:upperCorner>34.6498489555 36.7411761638</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.121"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.121.0"><gml:exterior><gml:LinearRing><gml:posList>33.4067217 35.821099894 33.089016018 35.105235222 34.6498489555 35.9698996554 34.6251981176 36.7411761638 34.0779836581 36.5485591885 33.4067217 35.821099894</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>LB</ogr:ISO2>
      <ogr:ISO3>LBN</ogr:ISO3>
      <ogr:NAMEen>Lebanon</ogr:NAMEen>
      <ogr:GBD_ID>146</ogr:GBD_ID>
      <ogr:GBD_NAME>Lebanon</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.122">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>4.3528445816 -11.476185676</gml:lowerCorner><gml:upperCorner>8.484625346 -7.446543336</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.122"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.122.0"><gml:exterior><gml:LinearRing><gml:posList>7.5579894 -8.48544633 6.492834778 -8.618719849 5.845949198 -7.446543336 4.3528445816 -7.5406661241 5.145738023 -9.278716601 6.176988023 -10.39561927 6.919419664 -11.476185676 7.76903595 -10.615187134 8.484625346 -10.28223588 8.343471172 -9.499131226 7.190208232 -9.125407268 7.5579894 -8.48544633</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>LR</ogr:ISO2>
      <ogr:ISO3>LBR</ogr:ISO3>
      <ogr:NAMEen>Liberia</ogr:NAMEen>
      <ogr:GBD_ID>210</ogr:GBD_ID>
      <ogr:GBD_NAME>Liberia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.123">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>19.496123759 9.40116215</gml:lowerCorner><gml:upperCorner>33.1812253147 25.150889519</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.123"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.123.0"><gml:exterior><gml:LinearRing><gml:posList>31.65648021 25.150889519 30.144155986 24.688342732 29.181372376 24.981244751 25.205439352 24.981244751 21.995351054 24.981244751 20.00206187 24.980521281 19.995421448 23.981305786 19.496123759 23.981305786 20.654967754 21.63447229 21.87489329 19.164132121 23.444719951 15.985101359 22.99566376 14.979909027 22.617948711 14.231720011 23.17967153 13.482256713 23.517351176 11.968860717 24.266840312 11.567128133 24.856339213 10.032028035 26.113394267 9.40116215 26.504223125 9.835760945 29.128533224 9.826149129 30.228905335 9.519707885 30.915633443 10.270153036 31.411830547 10.108095744 32.465487163 11.5641309 33.1812253147 11.505111803 32.832668361 12.348317905 32.904242255 13.351735873 32.389797268 15.195974155 31.275539455 16.044606967 31.086615302 17.36589603 30.266180731 19.056813998 30.510931708 19.74887129 31.216782945 20.146657748 31.717718817 19.92066491 32.557847398 20.562673373 32.930731512 21.608653191 32.646470445 23.086436394 32.323553778 23.095550977 31.967922268 24.981700066 31.65648021 25.150889519</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>LY</ogr:ISO2>
      <ogr:ISO3>LBY</ogr:ISO3>
      <ogr:NAMEen>Libya</ogr:NAMEen>
      <ogr:GBD_ID>147</ogr:GBD_ID>
      <ogr:GBD_NAME>Libya</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.124">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>5.923732815 79.6312190726</gml:lowerCorner><gml:upperCorner>9.7080948376 81.880707227</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.124"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.124.0"><gml:exterior><gml:LinearRing><gml:posList>9.7080948376 80.4093974923 8.482163804 81.365733269 7.327093817 81.880707227 6.462144273 81.681488477 5.923732815 80.590830925 6.807196356 79.863047722 9.1284358774 79.6312190726 9.7080948376 80.4093974923</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>LK</ogr:ISO2>
      <ogr:ISO3>LKA</ogr:ISO3>
      <ogr:NAMEen>Sri Lanka</ogr:NAMEen>
      <ogr:GBD_ID>17</ogr:GBD_ID>
      <ogr:GBD_NAME>Sri Lanka</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>South Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.125">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-30.600301615 27.014867391</gml:lowerCorner><gml:upperCorner>-28.700469258 29.435908244</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.125"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.125.0"><gml:exterior><gml:LinearRing><gml:posList>-28.700469258 28.758636922 -29.342393901 29.435908244 -29.911144714 29.150654337 -30.600301615 27.743608439 -29.62558075 27.014867391 -28.908621928 27.747432495 -28.700469258 28.758636922</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>LS</ogr:ISO2>
      <ogr:ISO3>LSO</ogr:ISO3>
      <ogr:NAMEen>Lesotho</ogr:NAMEen>
      <ogr:GBD_ID>194</ogr:GBD_ID>
      <ogr:GBD_NAME>Lesotho</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.126">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>53.939292704 21.0364811833</gml:lowerCorner><gml:upperCorner>56.438726706 26.594531291</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.126"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.126.0"><gml:exterior><gml:LinearRing><gml:posList>55.666990866 26.594531291 55.2483949451 26.585069889 54.869675192 25.782694539 53.969678447 24.788698365 53.939292704 23.485625448 54.356269837 22.767219686 54.893756409 22.808870891 55.3006228883 21.0364811833 56.072617906 21.05339603 56.417410177 22.094082479 56.260520528 24.447220499 56.438726706 24.892257934 55.666990866 26.594531291</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>LT</ogr:ISO2>
      <ogr:ISO3>LTU</ogr:ISO3>
      <ogr:NAMEen>Lithuania</ogr:NAMEen>
      <ogr:GBD_ID>60</ogr:GBD_ID>
      <ogr:GBD_NAME>Lithuania</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.127">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>55.666990866 21.05339603</gml:lowerCorner><gml:upperCorner>58.075138449 28.148906697</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.127"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.127.0"><gml:exterior><gml:LinearRing><gml:posList>57.52760081 27.352934611 56.142414043 28.148906697 55.666990866 26.594531291 56.438726706 24.892257934 56.260520528 24.447220499 56.417410177 22.094082479 56.072617906 21.05339603 57.555324611 21.699229363 57.759711005 22.585785352 57.3156049681 23.189601341 57.2795303695 23.89180841 57.8681862559 24.3061587342 58.075138449 25.263501424 57.52760081 27.352934611</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>LV</ogr:ISO2>
      <ogr:ISO3>LVA</ogr:ISO3>
      <ogr:NAMEen>Latvia</ogr:NAMEen>
      <ogr:GBD_ID>59</ogr:GBD_ID>
      <ogr:GBD_NAME>Latvia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.128">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>27.6761318694 -12.9749154211</gml:lowerCorner><gml:upperCorner>35.901092623 -1.047502401</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.128"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.128.0"><gml:exterior><gml:LinearRing><gml:posList>35.089300848 -2.222564257 34.741343079 -1.769525513 33.237972311 -1.674234172 32.517008566 -1.047502401 32.081660462 -1.249557251 32.132200013 -2.516146606 31.6478209968 -3.659506721 30.7113174864 -3.6455290567 30.5086413535 -4.3723676021 29.6140708361 -5.7561563712 29.389421692 -7.619452678 28.665899964 -8.682385213 27.6761318694 -8.6828343985 27.9272019046 -12.9749154211 28.340492658 -11.4855734559 29.0053458059 -10.5737561215 30.1419244431 -9.6550693221 31.4175609109 -9.8474923441 32.592160932 -9.2598762553 33.2840484963 -8.5290897307 34.0550490556 -6.8219168959 35.8129879986 -5.9078166961 35.901092623 -5.3691729611 35.1674018576 -4.3757149505 35.3453610371 -2.9473907671 35.2924982301 -2.9124758378 35.089300848 -2.222564257</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MA</ogr:ISO2>
      <ogr:ISO3>MAR</ogr:ISO3>
      <ogr:NAMEen>Morocco</ogr:NAMEen>
      <ogr:GBD_ID>148</ogr:GBD_ID>
      <ogr:GBD_NAME>Morocco</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.129">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>45.461773987 26.617889038</gml:lowerCorner><gml:upperCorner>48.451979066 29.928177938</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.129"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.129.0"><gml:exterior><gml:LinearRing><gml:posList>45.461773987 28.199497925 46.662424215 28.234121134 48.258967591 26.617889038 48.451979066 27.751773315 47.870722555 29.236023803 46.809753723 29.928177938 46.341461894 29.84776941 46.45478831 28.94580896 45.7285548955 28.636325653 45.461773987 28.199497925</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MD</ogr:ISO2>
      <ogr:ISO3>MDA</ogr:ISO3>
      <ogr:NAMEen>Moldova</ogr:NAMEen>
      <ogr:GBD_ID>61</ogr:GBD_ID>
      <ogr:GBD_NAME>Republic of Moldova</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.130">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-25.584161066 43.22543379</gml:lowerCorner><gml:upperCorner>-12.090508722 50.48340905</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.130"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.130.0"><gml:exterior><gml:LinearRing><gml:posList>-12.090508722 49.351735873 -13.033868097 49.942149285 -15.443454685 50.48340905 -15.970961196 50.230153842 -15.544610284 49.638438347 -16.552422784 49.849457227 -17.79680755 49.497731967 -22.481133722 47.900645379 -24.934340102 47.126149936 -25.584161066 45.136078321 -24.998955988 44.028819207 -24.40618255 43.706716342 -23.454034113 43.757985873 -22.233819269 43.22543379 -21.309502863 43.507497592 -19.912774347 44.47934004 -17.455743097 43.943125847 -16.225355727 44.85425866 -15.706719659 46.219004754 -15.198418878 46.97144616 -13.411553644 48.747406446 -12.333428644 48.984629754 -12.090508722 49.351735873</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MG</ogr:ISO2>
      <ogr:ISO3>MDG</ogr:ISO3>
      <ogr:NAMEen>Madagascar</ogr:NAMEen>
      <ogr:GBD_ID>181</ogr:GBD_ID>
      <ogr:GBD_NAME>Madagascar</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.131">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>14.546291408 -117.125121489</gml:lowerCorner><gml:upperCorner>32.712836405 -86.827951627</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.131"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.131.0"><gml:exterior><gml:LinearRing><gml:posList>25.965806382 -97.139271614 24.51585521 -97.651437955 22.596096096 -97.890980598 20.647040106 -97.150542773 18.76581452 -95.752756314 18.144232489 -94.466949023 18.880117173 -91.431187992 19.375921942 -90.730091926 21.000392971 -90.360096809 21.284572658 -89.785959439 21.600083726 -87.96206621 21.433294989 -86.827951627 20.855047919 -86.875314908 19.614935614 -87.742095507 19.492010809 -87.433013476 18.2453102094 -87.8296610249 18.371730861 -88.091542121 18.481350002 -88.303822395 17.81431427 -89.160496175 17.801963603 -90.990901246 17.254968567 -91.430771037 16.367270813 -90.379853272 16.070699768 -90.485738282 16.06878774 -91.723776409 15.289507141 -92.204651449 14.546291408 -92.246245898 16.088934637 -93.898101366 16.428900458 -94.852080858 15.97537991 -95.434478252 15.65902517 -96.556038426 15.979454108 -97.772718996 16.528179282 -98.856550207 17.264237103 -101.059028659 17.89025992 -101.811061035 18.33348265 -103.498399656 18.874937644 -103.979977027 19.368475653 -105.017119921 20.406910593 -105.698681425 21.492865302 -105.200062629 22.638454494 -105.800160286 25.106838283 -108.283802864 25.793402411 -109.440541145 26.334458726 -109.225412564 27.812201239 -110.615589973 28.052012803 -111.250662024 28.987435522 -112.152178958 30.812439603 -113.128840604 31.156691667 -113.048722937 31.908880927 -114.933827278 30.183252241 -114.660302305 28.441077049 -112.846304024 27.863902157 -112.773217563 26.714577386 -111.814584775 24.114455978 -110.350380957 23.560204887 -109.476458899 22.874202959 -109.895378363 23.54103237 -110.307362344 24.77338288 -112.088042773 26.178127346 -112.343495246 26.78912995 -113.163685676 26.725530915 -113.637137676 27.860256252 -115.05589759 27.950832424 -114.168324348 28.458441473 -114.042591926 29.755825783 -115.69756433 30.813177802 -116.068023241 32.53166949 -117.125121489 32.712836405 -114.724284628 31.333644104 -111.067117676 31.327442932 -108.214811158 31.777751364 -108.215121217 31.773823954 -106.51718868 30.676991679 -105.00849524 29.667905986 -104.530824138 28.985105286 -103.147988648 29.8939387 -102.321010702 29.6314105479 -101.303668774 29.4604523152 -101.0390261447 28.296516825 -100.284339153 27.573770244 -99.507203125 27.0568388944 -99.4616351591 26.5658300599 -99.1715476129 26.075412089 -98.22265621 25.965806382 -97.139271614</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MX</ogr:ISO2>
      <ogr:ISO3>MEX</ogr:ISO3>
      <ogr:NAMEen>Mexico</ogr:NAMEen>
      <ogr:GBD_ID>130</ogr:GBD_ID>
      <ogr:GBD_NAME>Mexico</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>4. Emerging region: MIKT</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.132">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>40.841981383 20.567147258</gml:lowerCorner><gml:upperCorner>42.313439026 22.916977986</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.132"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.132.0"><gml:exterior><gml:LinearRing><gml:posList>42.313439026 22.345023234 41.9077564398 22.8565652991 41.335772604 22.916977986 40.841981383 20.9645643201 40.906252751 20.9410466833 40.9084800643 20.7245659003 41.0826440979 20.633525624 41.873181662 20.567147258 42.246289302 21.56406571 42.313439026 22.345023234</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MK</ogr:ISO2>
      <ogr:ISO3>MKD</ogr:ISO3>
      <ogr:NAMEen>Macedonia</ogr:NAMEen>
      <ogr:GBD_ID>49</ogr:GBD_ID>
      <ogr:GBD_NAME>North Macedonia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.133">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>10.161990662 -12.264130412</gml:lowerCorner><gml:upperCorner>24.995064596 4.22860966</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.133"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.133.0"><gml:exterior><gml:LinearRing><gml:posList>19.142243551 4.22860966 16.416053365 4.183961222 15.353973491 3.507103312 15.283616028 1.331525512 14.910977275 0.218466838 15.069727274 -0.75289506 14.198643494 -2.023411825 14.04299408 -2.840855469 13.5175966081 -3.3208293581 13.469954122 -3.971613322 12.149492086 -4.560466268 11.836436259 -5.28248938 10.42548879 -5.522578085 10.189870097 -6.015932983 10.720742086 -6.245841431 10.185503439 -6.959389608 10.161990662 -7.989662639 11.367653504 -8.375323852 11.657945862 -8.847233033 12.3313548474 -9.2862803666 12.0438540216 -10.4995113795 12.403895162 -11.388421591 13.360813497 -11.613110718 14.774939067 -12.264130412 15.541171367 -11.727057251 15.102903544 -10.915168823 15.42262563 -10.736368368 15.49564443 -9.349217896 15.494197489 -5.510950887 19.620612285 -5.963533081 21.822855123 -6.219383097 24.99413442 -6.593107056 24.995064596 -4.821613118 23.500995585 -2.523226278 21.101710511 1.146523885 20.231789449 1.89138798 19.79406423 3.216785116 18.981684876 3.308355754 19.142243551 4.22860966</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>ML</ogr:ISO2>
      <ogr:ISO3>MLI</ogr:ISO3>
      <ogr:NAMEen>Mali</ogr:NAMEen>
      <ogr:GBD_ID>211</ogr:GBD_ID>
      <ogr:GBD_NAME>Mali</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.134">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>10.350531317 92.265147332</gml:lowerCorner><gml:upperCorner>28.538465881 101.159023885</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.134"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.134.0"><gml:exterior><gml:LinearRing><gml:posList>21.552690735 101.159023885 20.31780487 100.099295288 20.444024557 99.942043905 19.67585439 98.53262089 19.802978414 98.024538208 18.520782776 97.379099569 17.011572571 98.436192668 16.273478089 98.682379192 15.325964865 98.533551066 15.125796204 98.164994751 13.714874573 99.152531372 13.171341858 99.102095175 11.815765686 99.630021606 10.350531317 98.747406446 11.76194896 98.768077019 13.186753648 98.578461134 13.690252997 98.160411004 14.880316473 97.794769727 16.539862372 97.652842644 17.375311591 96.904551629 16.737372137 96.801036004 15.733628648 95.431976759 16.014797268 94.214610222 17.545558986 94.615000847 18.821030992 94.153086785 19.313910223 93.497813347 19.916571356 93.745371941 20.259182033 92.731211785 21.061102606 92.265147332 21.977574362 92.57587854 21.995351054 92.978954712 23.129726257 93.374899536 24.041143494 93.302965942 23.842628479 94.099713176 26.049935608 95.15122522 26.475594381 95.040637655 27.257510478 96.142585897 27.341743063 96.758879028 28.217477723 97.323495728 28.538465881 97.546221151 27.577335918 98.679278605 25.878989971 98.692404419 24.745028178 97.53609257 23.863557435 97.637068319 24.075715027 98.585227499 23.179387309 98.859009237 23.057379252 99.493079061 22.153532613 99.144469849 22.045528869 99.94240564 21.427892151 100.187145223 21.552690735 101.159023885</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MM</ogr:ISO2>
      <ogr:ISO3>MMR</ogr:ISO3>
      <ogr:NAMEen>Myanmar</ogr:NAMEen>
      <ogr:GBD_ID>15</ogr:GBD_ID>
      <ogr:GBD_NAME>Myanmar</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.135">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>41.852362372 18.437354777</gml:lowerCorner><gml:upperCorner>43.532796122 20.34535201</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.135"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.135.0"><gml:exterior><gml:LinearRing><gml:posList>43.532796122 19.195344686 42.82743866 20.34535201 42.546758118 20.064955688 42.3263247431 19.4004744743 42.0884163441 19.3710404475 41.852362372 19.365082227 42.416327216 18.496429884 42.559212138 18.437354777 43.532796122 19.195344686</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>ME</ogr:ISO2>
      <ogr:ISO3>MNE</ogr:ISO3>
      <ogr:NAMEen>Montenegro</ogr:NAMEen>
      <ogr:GBD_ID>50</ogr:GBD_ID>
      <ogr:GBD_NAME>Montenegro</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.136">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>41.596144308 87.816324097</gml:lowerCorner><gml:upperCorner>52.128550517 119.699959351</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.136"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.136.0"><gml:exterior><gml:LinearRing><gml:posList>49.823264873 116.684277792 48.12210256 115.514530071 47.705564678 115.852700643 47.776900575 117.5094003564 47.9775440208 117.7416300051 47.96624644 118.542252239 47.159525655 119.699959351 46.591627503 119.680115601 46.715392558 118.238291464 46.309319153 116.603559205 45.44435903 115.637830038 45.385499573 114.533711385 44.746262105 113.635058228 45.087481588 112.011488079 44.346596578 111.396435181 43.696636251 111.933353313 42.768605041 110.40672815 42.449296367 109.485130656 42.286618958 106.767828818 41.596144308 105.014809204 42.184609681 102.034164266 42.5154422 101.637599325 42.564198914 99.474475545 42.740906474 96.350635214 44.287117005 95.379428345 44.951262513 93.525277955 45.186183574 90.873243449 46.566409404 91.047496379 47.658642477 90.344800252 47.992988994 89.045551392 48.599489441 87.942828004 49.165837301 87.816324097 50.843357646 92.301683391 50.565235087 94.237585897 50.015191142 94.624539022 49.725544739 97.301688274 50.518622946 98.293462362 51.001125794 97.806360311 52.128550517 98.886397746 51.731727193 100.005967652 51.32389679 102.18387089 50.545546366 102.327634726 50.131204733 103.668069703 50.476454977 105.328640178 49.933490703 107.946516561 49.327429505 108.538056681 49.137673646 110.731359497 49.588602194 113.043673137 50.276880595 114.286284628 49.895405172 115.368699179 49.823264873 116.684277792</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MN</ogr:ISO2>
      <ogr:ISO3>MNG</ogr:ISO3>
      <ogr:NAMEen>Mongolia</ogr:NAMEen>
      <ogr:GBD_ID>38</ogr:GBD_ID>
      <ogr:GBD_NAME>Mongolia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.137">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-26.846123956 30.214465373</gml:lowerCorner><gml:upperCorner>-10.474786066 40.84253991</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.137"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.137.0"><gml:exterior><gml:LinearRing><gml:posList>-26.846123956 32.893077019 -26.840014343 32.113884318 -25.95810435 31.949243204 -24.423107605 31.986553589 -22.397339783 31.288921753 -21.290327251 32.408543335 -20.564583435 32.513136434 -20.032006124 33.00726648 -19.46397878 32.763353719 -18.33288503 33.034965047 -16.681616312 32.968509156 -16.023465271 31.259982951 -16.001244405 30.402567587 -15.635995381 30.396263061 -14.981461691 30.214465373 -14.013872172 33.202706747 -14.601743266 33.644850302 -14.387389425 34.344084106 -15.27115977 34.569083293 -15.889830017 34.233186483 -17.129340922 35.291933228 -16.6215708371 35.3354014174 -16.004965108 35.79567509 -14.667475687 35.853035929 -13.5137256274 34.8572364092 -12.1880329058 34.6947903101 -11.57355601 34.964614706 -11.413462423 35.82637089 -11.701506856 36.199681437 -11.72259084 37.427823527 -11.319101257 37.875238077 -11.413462423 38.492254679 -10.474786066 40.436859571 -13.636407159 40.539398634 -14.464450779 40.84253991 -15.477634373 40.588389519 -16.42978281 39.855967644 -17.869073175 37.086436394 -18.576429946 36.4795028 -19.890069269 34.743337436 -22.40992604 35.542246941 -24.597914321 35.106455925 -25.543877863 32.872731967 -25.949965102 32.516612175 -26.846123956 32.893077019</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MZ</ogr:ISO2>
      <ogr:ISO3>MOZ</ogr:ISO3>
      <ogr:NAMEen>Mozambique</ogr:NAMEen>
      <ogr:GBD_ID>184</ogr:GBD_ID>
      <ogr:GBD_NAME>Mozambique</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.138">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>14.774939067 -17.0071292065</gml:lowerCorner><gml:upperCorner>27.284959249 -4.821613118</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.138"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.138.0"><gml:exterior><gml:LinearRing><gml:posList>14.774939067 -12.264130412 16.636660055 -14.34325415 16.4882628454 -16.1595803585 15.80882396 -16.542347786 17.670681649 -16.0442444101 18.9792173663 -16.2093181427 19.396429755 -16.521839973 20.215155341 -16.194808647 21.1954084035 -17.0071292065 21.332859192 -16.958830933 21.333427633 -13.015247355 23.018001811 -13.015247355 23.444978333 -12.032723348 25.994900208 -12.01530839 26.013141989 -8.680809082 27.284959249 -8.6829954779 24.995064596 -4.821613118 24.99413442 -6.593107056 21.822855123 -6.219383097 19.620612285 -5.963533081 15.494197489 -5.510950887 15.49564443 -9.349217896 15.42262563 -10.736368368 15.102903544 -10.915168823 15.541171367 -11.727057251 14.774939067 -12.264130412</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MR</ogr:ISO2>
      <ogr:ISO3>MRT</ogr:ISO3>
      <ogr:NAMEen>Mauritania</ogr:NAMEen>
      <ogr:GBD_ID>212</ogr:GBD_ID>
      <ogr:GBD_NAME>Mauritania</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.139">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-17.129340922 32.80779545</gml:lowerCorner><gml:upperCorner>-9.407900086 35.853035929</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.139"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.139.0"><gml:exterior><gml:LinearRing><gml:posList>-9.717958679 33.911862426 -10.4406874472 34.270905967 -11.6923237157 34.3156493143 -12.1880329058 34.030116111 -13.3958089194 34.3250689664 -13.5739957383 34.5898375036 -13.5137256274 34.8572364092 -14.667475687 35.853035929 -16.004965108 35.79567509 -16.6215708371 35.3354014174 -17.129340922 35.291933228 -15.889830017 34.233186483 -15.27115977 34.569083293 -14.387389425 34.344084106 -14.601743266 33.644850302 -14.013872172 33.202706747 -13.699162699 32.80779545 -12.139516296 33.252729533 -10.818149922 33.319082072 -10.577027689 33.674202515 -9.407900086 32.920863485 -9.717958679 33.911862426</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MW</ogr:ISO2>
      <ogr:ISO3>MWI</ogr:ISO3>
      <ogr:NAMEen>Malawi</ogr:NAMEen>
      <ogr:GBD_ID>182</ogr:GBD_ID>
      <ogr:GBD_NAME>Malawi</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.140">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>0.851370341 100.127289259</gml:lowerCorner><gml:upperCorner>7.026760158 119.267100457</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.140"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.140.0"><gml:exterior><gml:LinearRing><gml:posList>6.257513739 102.07309004 5.381333726 103.120290561 4.795070705 103.448578321 2.961818752 103.434580925 1.446519273 104.294688347 1.538397528 103.367360873 2.849676825 101.286387566 3.868068752 100.720713738 6.012681382 100.349131707 6.442287502 100.127289259 6.246467387 101.081664266 5.6376415 101.105435426 5.739909159 101.800483439 6.257513739 102.07309004</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.140.1"><gml:exterior><gml:LinearRing><gml:posList>4.159654039 117.567149285 4.38921641 115.844122355 3.030823873 115.464093873 2.247202454 114.778761027 1.436037496 114.500638469 1.233259176 113.649527629 1.537013244 113.023415975 1.449034119 112.1800566 1.008466695 111.823075806 0.851370341 110.553902629 1.403274638 109.84283492 2.083238023 109.645274285 1.6558130593 110.214584819 1.9251429141 111.1475221577 2.680121161 111.44027754 3.161688544 113.007823113 4.601141669 113.99878991 4.021435242 114.586628052 4.6450282549 115.6069422041 4.9900154729 115.1117252181 5.560003973 115.848643425 7.026760158 116.777354363 6.389878648 117.743825717 5.908026434 117.774261915 5.796128648 118.410655144 5.201076565 119.267100457 4.8971412175 118.5739341706 4.444037177 118.645274285 4.159654039 117.567149285</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MY</ogr:ISO2>
      <ogr:ISO3>MYS</ogr:ISO3>
      <ogr:NAMEen>Malaysia</ogr:NAMEen>
      <ogr:GBD_ID>13</ogr:GBD_ID>
      <ogr:GBD_NAME>Malaysia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.141">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-28.959368184 11.766123894</gml:lowerCorner><gml:upperCorner>-16.951057231 25.259780721</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.141"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.141.0"><gml:exterior><gml:LinearRing><gml:posList>-17.641144307 23.381652466 -17.479500427 24.220464314 -17.794106547 25.259780721 -18.029441019 24.183050578 -18.478199157 23.592182251 -18.009803975 23.31147587 -18.319345805 20.975081014 -22.000671489 20.971980428 -22.000671489 19.978345988 -24.752493184 19.981446574 -28.4223467 19.98165328 -28.959368184 19.081656535 -28.704293315 17.403619425 -28.082625834 16.892952921 -28.5729305965 16.48707116 -27.322442316 15.295258009 -24.159112238 14.473643425 -22.276462498 14.386485222 -20.816094659 13.387217644 -20.154229425 13.154633009 -18.143183242 11.849081614 -17.252699477 11.766123894 -16.951057231 13.166307006 -17.408186951 13.942745402 -17.389893494 18.453581177 -17.747701111 18.761986124 -18.031404724 20.806202433 -17.641144307 23.381652466</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>NA</ogr:ISO2>
      <ogr:ISO3>NAM</ogr:ISO3>
      <ogr:NAMEen>Namibia</ogr:NAMEen>
      <ogr:GBD_ID>195</ogr:GBD_ID>
      <ogr:GBD_NAME>Namibia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.142">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>11.69577301 0.152941121</gml:lowerCorner><gml:upperCorner>23.517351176 15.953992147</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.142"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.142.0"><gml:exterior><gml:LinearRing><gml:posList>22.99566376 14.979909027 21.507267151 15.180395955 20.374571432 15.953992147 19.903540751 15.736020955 16.876231994 15.452420695 15.749634095 14.368972615 14.380131124 13.449183797 13.7057695706 13.6032181205 13.064164937 12.472705932 13.37510203 10.674986206 12.82252594 9.64073409 12.923579204 8.679345743 13.327146302 7.75557784 13.006855774 6.90663741 13.62627533 6.368944133 13.873417867 5.554316854 13.472977194 4.125773559 12.528538716 3.645699503 11.69577301 3.596400187 12.1491879924 3.1116627147 12.2310288425 2.6025122308 11.89653595 2.390168904 12.705633851 2.109049113 13.0673172 0.971754191 14.546710103 0.152941121 14.910977275 0.218466838 15.283616028 1.331525512 15.353973491 3.507103312 16.416053365 4.183961222 19.142243551 4.22860966 19.478631287 5.837607055 20.87257721 7.482726278 22.193426819 9.723313029 23.517351176 11.968860717 23.17967153 13.482256713 22.617948711 14.231720011 22.99566376 14.979909027</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>NE</ogr:ISO2>
      <ogr:ISO3>NER</ogr:ISO3>
      <ogr:NAMEen>Niger</ogr:NAMEen>
      <ogr:GBD_ID>213</ogr:GBD_ID>
      <ogr:GBD_NAME>Niger</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.143">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>4.280991929 2.7038412893</gml:lowerCorner><gml:upperCorner>13.873417867 14.669936157</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.143"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.143.0"><gml:exterior><gml:LinearRing><gml:posList>13.0750351902 14.0665556526 12.385601705 14.179836873 12.167423808 14.669936157 11.496431173 14.593765096 11.276263733 13.982329549 9.382890931 12.862036173 8.831503397 12.805708862 6.437282613 11.31949467 7.058071595 10.602535848 6.99440623 10.119464559 5.847499492 8.855872437 4.815293687 8.594167514 4.54682038 8.279063347 4.607896226 7.312022332 4.280991929 6.188731316 4.636460679 5.594899936 6.006537177 4.885915561 6.358872789 4.405528191 6.3683516726 2.7038412893 8.772308045 2.722861776 10.599896749 3.837419067 11.69577301 3.596400187 12.528538716 3.645699503 13.472977194 4.125773559 13.873417867 5.554316854 13.62627533 6.368944133 13.006855774 6.90663741 13.327146302 7.75557784 12.923579204 8.679345743 12.82252594 9.64073409 13.37510203 10.674986206 13.064164937 12.472705932 13.7057695706 13.6032181205 13.0750351902 14.0665556526</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>NG</ogr:ISO2>
      <ogr:ISO3>NGA</ogr:ISO3>
      <ogr:NAMEen>Nigeria</ogr:NAMEen>
      <ogr:GBD_ID>214</ogr:GBD_ID>
      <ogr:GBD_NAME>Nigeria</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.144">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>10.92593008 -87.646514452</gml:lowerCorner><gml:upperCorner>14.9970120213 -83.1304444725</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.144"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.144.0"><gml:exterior><gml:LinearRing><gml:posList>14.9970120213 -83.1304444725 13.287095445 -83.570139127 12.400336005 -83.492298957 11.37490469 -83.868031379 10.92593008 -83.644357877 11.0808801812 -85.7017355659 12.872381903 -87.646514452 12.981553453 -87.314035611 13.314201355 -86.701860718 13.768256327 -86.727233847 13.847683004 -85.824162354 14.5113094451 -85.008517904 14.61947052 -84.482693848 14.9970120213 -83.1304444725</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>NI</ogr:ISO2>
      <ogr:ISO3>NIC</ogr:ISO3>
      <ogr:NAMEen>Nicaragua</ogr:NAMEen>
      <ogr:GBD_ID>131</ogr:GBD_ID>
      <ogr:GBD_NAME>Nicaragua</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.145">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>50.749926656 3.457530144</gml:lowerCorner><gml:upperCorner>53.460638739 7.194590691</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.145"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.145.0"><gml:exterior><gml:LinearRing><gml:posList>53.245021877 7.194590691 52.230637309 7.026320027 51.806735535 5.928040812 50.749926656 5.994910116 51.474326884 5.012127727 51.4498969326 3.9028219865 51.555080471 3.457530144 52.336371161 4.508148634 52.967596747 4.745453321 53.460638739 6.730235222 53.245021877 7.194590691</gml:posList></gml:LinearRing></gml:exterior><gml:interior><gml:LinearRing><gml:posList>52.9229571027 5.0652747295 52.7036115474 5.1430106197 52.7942623757 5.3861957515 53.0249188203 5.4059467683 52.9229571027 5.0652747295</gml:posList></gml:LinearRing></gml:interior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>NL</ogr:ISO2>
      <ogr:ISO3>NLD</ogr:ISO3>
      <ogr:NAMEen>Netherlands</ogr:NAMEen>
      <ogr:GBD_ID>89</ogr:GBD_ID>
      <ogr:GBD_NAME>Netherlands</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.146">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>58.120550848 4.926768425</gml:lowerCorner><gml:upperCorner>80.381170966 31.07699629</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.146"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.146.0"><gml:exterior><gml:LinearRing><gml:posList>69.8058419025 30.8409538907 69.027260641 28.954077189 69.464391582 29.344544311 70.070685324 27.897294149 69.696677145 26.031981649 68.563412984 24.860942016 69.1261383161 21.5785150411 69.036355693 20.62316451 68.350196025 19.931010376 68.535921123 18.171841268 67.422759095 16.127108195 67.052704163 16.41556604 66.1254481 14.540538371 65.31808136 14.514286743 64.584017639 13.642763713 64.009530742 13.93943811 63.956355693 12.681892131 63.288902893 11.992425171 60.117985332 12.510946492 58.991725979 11.437510613 59.247788804 10.469574415 58.257228908 8.501231316 58.120550848 6.530528191 58.754747227 5.47795116 59.27724844 6.252126498 59.511216539 5.18230228 60.16030508 5.705577019 60.800116278 4.926768425 62.186183986 5.431162957 62.597967841 7.422699415 62.910834052 7.501149936 63.9020251176 9.5967858469 65.1233519413 11.1735475585 65.4670264944 12.3661622556 66.8213547582 13.1436239451 67.444727192 14.2981110218 68.4351589636 15.4342010145 69.1096551719 17.4413072991 70.1025672293 18.6044920621 70.4929983479 22.9657239204 71.0714317048 24.8183233748 71.1854682047 27.7669932951 70.284247137 31.07699629 69.8058419025 30.8409538907</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.146.1"><gml:exterior><gml:LinearRing><gml:posList>78.363951949 22.8505039189 78.0715128562 23.821847843 77.1124882485 22.4308910793 77.442368882 20.921722852 78.2583075281 20.3508674817 78.6111302836 21.8781829892 78.363951949 22.8505039189</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.146.2"><gml:exterior><gml:LinearRing><gml:posList>79.875799872 16.820078972 78.7753978786 21.0128177785 78.466986395 18.993011915 76.566839911 16.809580925 77.767889716 13.718760613 79.561428127 10.708018425 79.811468817 14.548594597 78.916245835 16.347911004 79.875799872 16.820078972</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.146.3"><gml:exterior><gml:LinearRing><gml:posList>80.381170966 23.142425977 79.854966539 27.149668816 79.188869533 24.053070509 79.1267540877 21.9692794211 80.1012594417 18.5986216075 80.381170966 23.142425977</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>NO</ogr:ISO2>
      <ogr:ISO3>NOR</ogr:ISO3>
      <ogr:NAMEen>Norway</ogr:NAMEen>
      <ogr:GBD_ID>90</ogr:GBD_ID>
      <ogr:GBD_NAME>Norway</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.147">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>26.353327942 80.036385539</gml:lowerCorner><gml:upperCorner>30.330087789 88.118217814</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.147"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.147.0"><gml:exterior><gml:LinearRing><gml:posList>27.860884501 88.118217814 26.453941955 88.074189494 26.353327942 87.326224813 26.571712545 85.82161381 27.028377177 84.640238892 27.32903066 84.577038615 27.494963684 82.752137085 28.837026469 80.036385539 29.806140442 80.476151978 30.1969693 80.996016887 30.330087789 82.088766724 29.191707662 83.517051636 29.247001445 84.099134969 28.318789368 85.080212036 27.885172424 85.980260458 27.860884501 88.118217814</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>NP</ogr:ISO2>
      <ogr:ISO3>NPL</ogr:ISO3>
      <ogr:NAMEen>Nepal</ogr:NAMEen>
      <ogr:GBD_ID>164</ogr:GBD_ID>
      <ogr:GBD_NAME>Nepal</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>South Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.148">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-46.601983331 166.489756707</gml:lowerCorner><gml:upperCorner>-34.411716404 178.564300977</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.148"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.148.0"><gml:exterior><gml:LinearRing><gml:posList>-40.508477472 172.904470248 -41.0790455495 173.3243996278 -40.9494579123 174.0818245663 -41.730157159 174.292165561 -43.737481378 172.417328321 -44.2786309125 171.3460021856 -45.907810154 170.700450066 -46.578383071 169.635101759 -46.601983331 168.419200066 -45.807549738 166.489756707 -44.3041323609 167.1678405538 -43.565687758 169.733653191 -42.592380467 171.122813347 -40.886895441 172.106781446 -40.508477472 172.904470248</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.148.1"><gml:exterior><gml:LinearRing><gml:posList>-34.411716404 173.079274936 -35.594333592 174.569509311 -36.823663019 174.81910241 -37.631768488 175.997243686 -37.995538019 177.197032097 -37.54306406 177.992035352 -37.717705988 178.564300977 -39.097344659 177.86622155 -39.447523696 176.906260613 -40.177911066 176.837250196 -41.609795831 175.330332879 -41.3744981778 174.7963093887 -40.5367488832 175.1321695213 -39.9025219824 174.8265772525 -39.4818300564 173.6903489852 -38.839450779 174.578868035 -37.764906508 174.85035241 -36.268161717 174.024099155 -34.479913019 172.674082879 -34.411716404 173.079274936</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>NZ</ogr:ISO2>
      <ogr:ISO3>NZL</ogr:ISO3>
      <ogr:NAMEen>New Zealand</ogr:NAMEen>
      <ogr:GBD_ID>72</ogr:GBD_ID>
      <ogr:GBD_NAME>New Zealand</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.149">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>23.803452867 60.844378703</gml:lowerCorner><gml:upperCorner>37.021669006 77.2516900425</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.149"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.149.0"><gml:exterior><gml:LinearRing><gml:posList>35.424740888 77.2516900425 34.503812358 75.777110637 34.768886617 74.285832153 34.37095225 73.774855591 32.809910584 74.306502726 32.261675314 75.359048299 31.711192118 74.489437297 31.083788554 74.658832641 29.927321676 73.370332479 29.022622376 72.901523885 27.950207215 71.860863892 27.701462707 70.831572713 28.011469625 70.341938517 26.807770487 69.465092814 26.530113017 70.158073771 25.980327454 70.064642782 24.682577209 71.063858277 24.165218608 69.972038615 24.289216207 68.725913127 23.842108466 68.183034701 23.803452867 67.63160241 24.662909247 67.156993035 25.47288646 66.251800977 25.193670966 63.500498894 25.202093817 61.588226759 26.242378642 61.857133423 26.625404359 63.141344441 27.225161031 62.800641723 28.258327942 62.739301799 28.542599997 61.892790161 29.85817861 60.844378703 29.407818502 62.477508993 29.386605326 64.086092977 29.835337627 66.195628296 30.922868144 66.366263876 31.825913798 68.159126018 31.937689922 69.298849731 33.075010682 69.547516724 33.31894928 70.294447876 34.043788758 70.002837769 34.054253235 71.062618042 35.203123678 71.633694296 36.045707906 71.16591923 36.820596008 72.565213664 37.021669006 74.542353963 36.462633362 75.976581665 35.806239319 76.166027466 35.646111714 76.777350741 35.424740888 77.2516900425</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>PK</ogr:ISO2>
      <ogr:ISO3>PAK</ogr:ISO3>
      <ogr:NAMEen>Pakistan</ogr:NAMEen>
      <ogr:GBD_ID>165</ogr:GBD_ID>
      <ogr:GBD_NAME>Pakistan</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>South Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.150">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>7.2350980564 -82.8976292589</gml:lowerCorner><gml:upperCorner>9.602722473 -77.1690059</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.150"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.150.0"><gml:exterior><gml:LinearRing><gml:posList>8.664740302 -77.373524543 7.935072326 -77.1690059 7.2350980564 -77.8958391508 7.9018121419 -78.475176067 8.4240114191 -78.190940111 8.7954105573 -78.9233108395 8.651556319 -79.7478058187 8.220648505 -80.470244921 7.503444729 -79.994658983 7.3504854641 -80.6736723029 7.674777347 -80.9869352746 7.9145856864 -81.4365443076 8.1505497186 -82.5307180284 8.0347480201 -82.8976292589 9.602722473 -82.829022176 9.576198635 -82.573597786 9.014146226 -82.245961067 8.781882361 -81.30446316 9.5551847667 -80.0340165738 9.5393414078 -79.0090882292 9.22646719 -78.02456621 8.664740302 -77.373524543</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>PA</ogr:ISO2>
      <ogr:ISO3>PAN</ogr:ISO3>
      <ogr:NAMEen>Panama</ogr:NAMEen>
      <ogr:GBD_ID>132</ogr:GBD_ID>
      <ogr:GBD_NAME>Panama</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.151">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-18.3377462069 -81.252797004</gml:lowerCorner><gml:upperCorner>-0.107020772 -68.684252483</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.151"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.151.0"><gml:exterior><gml:LinearRing><gml:posList>-4.236484477 -69.964949504 -4.179433695 -70.832235067 -5.132088725 -72.917947551 -6.435264995 -73.131784628 -6.90417694 -73.765492717 -7.543517761 -74.018474691 -9.085645854 -72.95939205 -9.409036967 -73.214983684 -9.540915222 -72.306770386 -10.005589701 -72.195666057 -10.006829935 -71.391425741 -9.565823263 -70.624418295 -11.010799662 -70.641342326 -10.952301941 -69.577634644 -12.502491557 -68.684252483 -12.867947286 -68.980952718 -14.211482849 -68.883723511 -14.964408467 -69.390669312 -15.5197582472 -69.3689075755 -15.8555155657 -70.0147979905 -16.231204529 -69.111242796 -16.2216749995 -68.9623371317 -16.551258157 -69.0377217651 -17.506588198 -69.510088752 -18.3377462069 -70.3947027461 -17.68726979 -71.385161913 -17.284600519 -71.514515754 -16.220696358 -73.70027773 -15.358371153 -75.189460982 -14.080987238 -76.291737434 -13.357555919 -76.221101527 -11.292316287 -77.647256706 -8.217420554 -78.985740012 -6.787355106 -79.964679566 -6.072068061 -81.101457036 -4.238702081 -81.252797004 -3.3934976535 -80.3407286664 -3.998772888 -80.481077637 -5.011372579 -79.063928996 -4.550987243 -78.672660889 -3.408524678 -78.361723796 -2.980643819 -77.849016073 -2.573640239 -76.684591025 -1.502594502 -75.560034343 -0.107020772 -75.283487915 -0.170479431 -74.824652873 -0.858602803 -74.344423788 -1.255167745 -73.636560018 -2.213558858 -73.197775432 -2.446516215 -72.396584026 -2.197435812 -70.992716228 -2.71513031 -70.050629028 -3.782041931 -70.73412736 -4.236484477 -69.964949504</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>PE</ogr:ISO2>
      <ogr:ISO3>PER</ogr:ISO3>
      <ogr:NAMEen>Peru</ogr:NAMEen>
      <ogr:GBD_ID>123</ogr:GBD_ID>
      <ogr:GBD_NAME>Peru</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>South America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.152">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>5.573797919 117.19800866</gml:lowerCorner><gml:upperCorner>18.567206122 126.610362175</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.152"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.152.0"><gml:exterior><gml:LinearRing><gml:posList>9.611273505 125.6435653 9.4028124364 126.2966297174 8.229925848 126.463063998 7.277044989 126.610362175 6.299302476 126.2060653 7.0783445697 125.9087938387 6.768296617 125.388926629 6.149725653 125.706553582 5.573797919 125.387543165 5.5801884273 124.7574960305 6.3795879505 123.8480691126 7.5502448675 123.7878046433 7.3224240353 123.2147088293 7.4428031319 122.5453882017 6.978949286 121.937022332 8.009914455 122.292246941 8.607489325 123.758799675 8.3930089687 124.1456058451 8.94912344 124.780772332 9.026190497 125.526540561 9.611273505 125.6435653</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.152.1"><gml:exterior><gml:LinearRing><gml:posList>10.977362372 123.269786004 10.825588283 123.575531446 9.03530508 123.033213738 9.836615302 122.395192905 10.118068752 122.878916863 10.977362372 123.269786004</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.152.2"><gml:exterior><gml:LinearRing><gml:posList>11.077337958 123.999522332 10.266830021 124.1515525062 9.7090304167 123.5355034086 11.077337958 123.999522332</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.152.3"><gml:exterior><gml:LinearRing><gml:posList>11.474708091 119.8173816398 10.6382385291 119.973706306 9.8128616059 118.990314348 9.209255234 118.6090660029 8.334784247 117.19800866 10.194525458 118.822276238 10.5680089282 119.224594286 11.474708091 119.8173816398</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.152.4"><gml:exterior><gml:LinearRing><gml:posList>11.446926174 124.443125847 11.381333726 124.984141472 10.254339911 125.22925866 10.151922919 124.780446811 11.446926174 124.443125847</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.152.5"><gml:exterior><gml:LinearRing><gml:posList>11.76194896 122.300140821 11.6593393459 123.2467481824 10.5790858774 122.6961464585 10.433172919 121.949717644 11.76194896 122.300140821</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.152.6"><gml:exterior><gml:LinearRing><gml:posList>12.562241929 124.918955925 12.248602606 125.500498894 11.268133856 125.566172722 11.092189846 125.246836785 12.533270575 124.27621504 12.562241929 124.918955925</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.152.7"><gml:exterior><gml:LinearRing><gml:posList>13.3770924556 121.1739491587 12.7825364025 121.7517639888 12.214422919 121.24496504 13.518377997 120.356618686 13.3770924556 121.1739491587</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.152.8"><gml:exterior><gml:LinearRing><gml:posList>18.567206122 120.9091903 18.5457329302 122.046919633 17.090521552 122.529063347 16.319037177 122.231944207 15.932074286 121.577810092 15.308172919 121.379161004 14.195054429 121.731618686 14.0530061458 122.3113161303 14.345892645 122.470957879 14.1726195195 123.3210262735 13.729315497 123.972666863 13.5096169576 123.6330267233 12.9042950985 124.4381476369 12.8169812509 123.5653689618 13.4163325985 123.0018236052 13.228664455 122.690684441 13.6829419232 121.8336528618 13.656927802 121.430023634 13.810858466 120.628103061 14.3962812198 120.6324679739 14.803941148 120.079925977 16.367499091 119.812510613 16.017401434 120.299082879 17.556382554 120.333832227 18.567206122 120.9091903</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>PH</ogr:ISO2>
      <ogr:ISO3>PHL</ogr:ISO3>
      <ogr:NAMEen>Philippines</ogr:NAMEen>
      <ogr:GBD_ID>16</ogr:GBD_ID>
      <ogr:GBD_NAME>Philippines</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.153">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-10.556735935 140.974457227</gml:lowerCorner><gml:upperCorner>-2.600518488 155.91675866</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.153"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.153.0"><gml:exterior><gml:LinearRing><gml:posList>-5.483575128 154.810801629 -6.517510675 155.91675866 -6.872816665 155.71778405 -5.947849217 154.755869988 -5.483575128 154.810801629</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.153.1"><gml:exterior><gml:LinearRing><gml:posList>-4.190362238 152.23340905 -5.455743097 152.098887566 -6.1407966735 151.1962840725 -6.299248956 149.629405144 -5.775079034 148.386241082 -5.444756769 148.429942254 -5.5308122515 150.1236942065 -4.818780206 151.695648634 -4.176690363 151.540293816 -4.190362238 152.23340905</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.153.2"><gml:exterior><gml:LinearRing><gml:posList>-9.106133722 140.976980014 -6.896632182 140.977161906 -2.600518488 140.974457227 -3.463118745 144.025066527 -4.861016534 145.811859571 -5.422458592 145.727386915 -5.961602472 147.449066602 -6.691013279 147.854014519 -6.740411066 146.977305535 -7.462579034 147.188487175 -8.055352472 148.114024285 -9.032484633 148.559255405 -8.974541925 149.141856316 -9.486748956 149.226573113 -9.715020441 150.055918816 -10.06243255 149.939463738 -10.556735935 150.692881707 -10.166273696 147.977386915 -9.460219008 147.052744988 -8.087497654 146.079356316 -7.972263279 143.956797722 -8.335625909 142.980479363 -8.962172133 143.405528191 -9.330254816 142.652598504 -9.106133722 140.976980014</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.153.3"><gml:exterior><gml:LinearRing><gml:posList>-2.710625909 151.043630405 -3.0473657915 152.1340476963 -4.0367768299 153.0415969584 -4.848321222 152.897715691 -4.0239327893 152.6465802059 -3.4112695729 152.011949685 -3.1172158548 151.2259973399 -2.710625909 151.043630405</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>PG</ogr:ISO2>
      <ogr:ISO3>PNG</ogr:ISO3>
      <ogr:NAMEen>Papua New Guinea</ogr:NAMEen>
      <ogr:GBD_ID>26</ogr:GBD_ID>
      <ogr:GBD_NAME>Papua New Guinea</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.154">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>49.072199606 14.123922973</gml:lowerCorner><gml:upperCorner>54.838324286 24.10770634</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.154"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.154.0"><gml:exterior><gml:LinearRing><gml:posList>54.588041797 19.3094839524 54.4342658307 19.7579288966 54.356269837 22.767219686 53.939292704 23.485625448 52.670042013 23.868961222 52.289393413 23.165644979 51.517399191 23.606238241 50.540843811 24.10770634 49.528760885 22.640922485 49.072199606 22.539636678 49.377245586 21.81957727 49.205886536 19.747765747 49.510260722 18.833196249 50.1823334108 18.3248863178 50.2845831544 16.5785977055 50.858447164 14.8103927 52.576921082 14.644821411 52.850651144 14.123922973 54.1549113173 14.2620399341 54.6251439864 16.339610995 54.838324286 18.316905144 54.588041797 19.3094839524</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>PL</ogr:ISO2>
      <ogr:ISO3>POL</ogr:ISO3>
      <ogr:NAMEen>Poland</ogr:NAMEen>
      <ogr:GBD_ID>51</ogr:GBD_ID>
      <ogr:GBD_NAME>Poland</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.155">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>17.8596035031 -67.286528571</gml:lowerCorner><gml:upperCorner>18.5531984128 -65.2806438915</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.155"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.155.0"><gml:exterior><gml:LinearRing><gml:posList>18.5531984128 -67.1123529402 18.4018341195 -65.4755635124 18.1550054859 -65.2806438915 17.8596035031 -65.5702772068 17.9588114535 -67.1362014505 18.3549275785 -67.286528571 18.5531984128 -67.1123529402</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>PR</ogr:ISO2>
      <ogr:ISO3>PRI</ogr:ISO3>
      <ogr:NAMEen>Puerto Rico</ogr:NAMEen>
      <ogr:GBD_ID>385</ogr:GBD_ID>
      <ogr:GBD_NAME>Puerto Rico</ogr:GBD_NAME>
      <ogr:WB_ECONOMY xsi:nil="true"/>
      <ogr:WB_GROUP xsi:nil="true"/>
      <ogr:CONTINENT xsi:nil="true"/>
      <ogr:WB_REGION xsi:nil="true"/>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.156">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>37.8278101612 124.36996504</gml:lowerCorner><gml:upperCorner>42.961900736 130.699961785</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.156"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.156.0"><gml:exterior><gml:LinearRing><gml:posList>42.530480042 130.530771119 42.295111395 130.699961785 41.549402728 129.648797907 40.834585581 129.716557947 39.746975002 127.520843946 39.165961005 127.452240431 38.624335028 128.364919467 38.307223613 127.157488648 37.8278101612 126.6674589269 38.115383205 124.677093946 39.310614325 125.444916212 40.098293361 124.36996504 40.909131979 126.015336142 41.66593333 126.617883342 41.4335661479 127.8440018872 41.993742778 128.034592733 42.442371724 129.703328085 42.961900736 129.851742798 42.530480042 130.530771119</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>KP</ogr:ISO2>
      <ogr:ISO3>PRK</ogr:ISO3>
      <ogr:NAMEen>North Korea</ogr:NAMEen>
      <ogr:GBD_ID>7</ogr:GBD_ID>
      <ogr:GBD_NAME>Democratic People's Republic of Korea</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.157">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>37.021470445 -9.491688606</gml:lowerCorner><gml:upperCorner>41.96898021 -6.205947225</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.157"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.157.0"><gml:exterior><gml:LinearRing><gml:posList>37.192816473 -7.414418098 37.021470445 -8.997873502 38.359227269 -8.795070659 38.707586981 -9.491688606 40.993801174 -8.655100064 41.96898021 -8.750803189 41.92607249 -6.567630575 41.570280253 -6.205947225 41.01599884 -6.942491415 38.446362407 -7.359210164 38.022563985 -7.023985149 37.192816473 -7.414418098</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>PT</ogr:ISO2>
      <ogr:ISO3>PRT</ogr:ISO3>
      <ogr:NAMEen>Portugal</ogr:NAMEen>
      <ogr:GBD_ID>91</ogr:GBD_ID>
      <ogr:GBD_NAME>Portugal</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.158">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-27.487313334 -62.650357219</gml:lowerCorner><gml:upperCorner>-19.286728617 -54.2666745135</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.158"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.158.0"><gml:exterior><gml:LinearRing><gml:posList>-20.165124613 -58.158796753 -20.953603617 -57.81757727 -22.035294698 -57.986817586 -22.317344665 -55.874388387 -23.976829936 -55.398035034 -24.0662345087 -54.2666745135 -25.4316939044 -54.6094558586 -25.574944884 -54.600202613 -26.644987488 -54.793059042 -27.443698425 -55.754731608 -27.487313334 -57.180096802 -27.156274109 -58.653288534 -25.45984019 -57.556921346 -24.007008972 -60.033669393 -23.805470886 -61.00634904 -22.234455668 -62.650357219 -20.579776306 -62.277305054 -19.657765401 -61.761212525 -19.298097432 -60.006384237 -19.286728617 -59.089540975 -19.821372985 -58.175281535 -20.165124613 -58.158796753</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>PY</ogr:ISO2>
      <ogr:ISO3>PRY</ogr:ISO3>
      <ogr:NAMEen>Paraguay</ogr:NAMEen>
      <ogr:GBD_ID>136</ogr:GBD_ID>
      <ogr:GBD_NAME>Paraguay</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>South America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.159">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>31.211448958 34.2002694411</gml:lowerCorner><gml:upperCorner>32.3757741523 35.6320930748</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.159"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.159.0"><gml:exterior><gml:LinearRing><gml:posList>31.211448958 34.248350857 31.3142666891 34.2002694411 31.583141323 34.481204125 31.5182216314 34.5445930809 31.211448958 34.248350857</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.159.1"><gml:exterior><gml:LinearRing><gml:posList>32.3757741523 35.6320930748 31.7653694425 35.5594024594 31.4113397213 35.4497660796 31.4818723424 35.0857968532 31.7510099591 35.0794930423 32.2566098282 35.1057683822 32.3757741523 35.6320930748</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>PS</ogr:ISO2>
      <ogr:ISO3>PSE</ogr:ISO3>
      <ogr:NAMEen>Palestine</ogr:NAMEen>
      <ogr:GBD_ID>149</ogr:GBD_ID>
      <ogr:GBD_NAME>Palestine</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.160">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>24.6258503609 50.7747813164</gml:lowerCorner><gml:upperCorner>26.3645657251 52.1444789786</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.160"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.160.0"><gml:exterior><gml:LinearRing><gml:posList>24.6258503609 51.2152582947 24.746649481 50.807871941 25.5712169028 50.7747813164 26.3645657251 51.4139191573 26.1298318498 52.1444789786 25.0645346991 51.968261838 24.6258503609 51.2152582947</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>QA</ogr:ISO2>
      <ogr:ISO3>QAT</ogr:ISO3>
      <ogr:NAMEen>Qatar</ogr:NAMEen>
      <ogr:GBD_ID>151</ogr:GBD_ID>
      <ogr:GBD_NAME>Qatar</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.161">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>43.654287415 20.242825969</gml:lowerCorner><gml:upperCorner>48.258967591 29.65902754</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.161"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.161.0"><gml:exterior><gml:LinearRing><gml:posList>45.461773987 28.199497925 45.215887762 29.65902754 44.328558661 28.640879754 43.741278387 28.578379754 44.177046204 27.027476441 43.654287415 25.359619588 43.886592281 23.325150732 44.228434539 22.691640373 44.864859517 21.368545369 46.108091126 20.242825969 46.27852 21.134864542 47.492941997 21.988869263 47.946738587 22.877600546 47.710060527 24.896598755 48.258967591 26.617889038 46.662424215 28.234121134 45.461773987 28.199497925</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>RO</ogr:ISO2>
      <ogr:ISO3>ROU</ogr:ISO3>
      <ogr:NAMEen>Romania</ogr:NAMEen>
      <ogr:GBD_ID>52</ogr:GBD_ID>
      <ogr:GBD_NAME>Romania</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.162">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-2.808251241 28.8744457681</gml:lowerCorner><gml:upperCorner>-1.066836592 30.831016887</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.162"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.162.0"><gml:exterior><gml:LinearRing><gml:posList>-1.066836592 30.471785843 -1.59416514 30.831016887 -2.400627543 30.55459965 -2.316911723 29.931691935 -2.808251241 29.697546021 -2.720711365 29.015365438 -2.4770641795 28.8744457681 -1.6863271024 29.2212526473 -1.3930817714 29.57793851 -1.066836592 30.471785843</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>RW</ogr:ISO2>
      <ogr:ISO3>RWA</ogr:ISO3>
      <ogr:NAMEen>Rwanda</ogr:NAMEen>
      <ogr:GBD_ID>185</ogr:GBD_ID>
      <ogr:GBD_NAME>Rwanda</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.163">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>21.332859192 -16.958830933</gml:lowerCorner><gml:upperCorner>27.9272019046 -8.680809082</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.163"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.163.0"><gml:exterior><gml:LinearRing><gml:posList>27.6761318694 -8.6828343985 27.284959249 -8.6829954779 26.013141989 -8.680809082 25.994900208 -12.01530839 23.444978333 -12.032723348 23.018001811 -13.015247355 21.333427633 -13.015247355 21.332859192 -16.958830933 21.7602629087 -16.7817356159 23.7436017102 -15.9410683848 26.1042730358 -14.504787129 26.6916078648 -13.5957450375 27.9272019046 -12.9749154211 27.6761318694 -8.6828343985</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>EH</ogr:ISO2>
      <ogr:ISO3>SAH</ogr:ISO3>
      <ogr:NAMEen>Western Sahara</ogr:NAMEen>
      <ogr:GBD_ID>SAH</ogr:GBD_ID>
      <ogr:GBD_NAME>Western Sahara</ogr:GBD_NAME>
      <ogr:WB_ECONOMY xsi:nil="true"/>
      <ogr:WB_GROUP xsi:nil="true"/>
      <ogr:CONTINENT xsi:nil="true"/>
      <ogr:WB_REGION xsi:nil="true"/>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.164">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>16.370957749 34.64714603</gml:lowerCorner><gml:upperCorner>32.118144023 55.637564738</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.164"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.164.0"><gml:exterior><gml:LinearRing><gml:posList>29.095744527 46.53243575 28.994587911 47.434086141 28.533504944 47.668128703 28.5404797232 48.4327812271 27.612494208 48.873301629 26.816961981 50.001963738 25.733343817 50.144704623 24.746649481 50.807871941 24.6258503609 51.2152582947 24.256170966 51.56934655 22.93110789 52.583074178 22.703576559 55.186842895 21.978969625 55.637564738 19.995421448 54.978380168 18.995637513 51.97861495 18.612095032 49.128814738 18.148919169 48.16194869 17.09182607 47.427574911 17.428394674 45.165387411 17.325920308 43.165044393 16.370957749 42.789480014 17.453599351 42.301524285 17.909247137 41.719248894 19.972479559 40.522634311 20.461574611 39.646494988 21.315008856 39.08073978 22.546792489 39.081517394 23.784002997 38.452647332 24.267731705 37.522720274 25.193670966 37.230479363 25.85565827 36.663422071 28.054388739 35.216319207 28.099107164 34.64714603 29.3516858265 34.9493851167 29.189950664 36.016436808 29.865516663 36.756236613 30.499483134 37.981071411 31.49099884 36.959531698 32.118144023 39.14637496 31.920533345 40.424126424 31.079861145 42.075395142 29.201836243 44.691824585 29.095744527 46.53243575</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SA</ogr:ISO2>
      <ogr:ISO3>SAU</ogr:ISO3>
      <ogr:NAMEen>Saudi Arabia</ogr:NAMEen>
      <ogr:GBD_ID>152</ogr:GBD_ID>
      <ogr:GBD_NAME>Saudi Arabia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.165">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>8.68932733 21.935125773</gml:lowerCorner><gml:upperCorner>21.995713609 38.601573113</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.165"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.165.0"><gml:exterior><gml:LinearRing><gml:posList>21.995713609 36.883636915 21.278062242 37.113617384 19.159654039 37.333506707 18.709947007 37.551036004 18.004828192 38.601573113 17.522290751 38.209687948 17.072602437 37.000459432 16.252859192 36.944028768 15.111507671 36.423647095 14.263523254 36.526379842 12.695660299 36.099429159 12.575150859 35.616874633 11.77137563 35.048227173 10.869182638 34.950662069 10.87928538 34.587066691 9.454592111 34.070697863 10.192040507 33.902095581 10.843241069 33.18208785 12.210366923 33.209217977 11.9998113 32.082206665 11.050851135 32.414072713 9.792323304 31.234816528 9.735763448 30.749264771 10.270485332 30.012978963 9.324544836 28.843840924 9.595410258 27.895072062 9.520453593 26.556859172 10.417814839 25.843052612 10.293222962 25.084080851 8.886745504 24.558376505 8.68932733 24.170328031 8.783392639 23.482318156 9.907768453 23.624014934 10.919153747 22.861064087 11.396101379 22.914807576 13.059152323 21.935125773 14.122627462 22.531471802 15.704262187 23.094641561 15.721160381 23.984406372 19.496123759 23.981305786 19.995421448 23.981305786 20.00206187 24.980521281 21.995351054 24.981244751 21.994782613 28.290345093 21.9954074694 33.1811382663 21.995713609 36.883636915</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SD</ogr:ISO2>
      <ogr:ISO3>SDN</ogr:ISO3>
      <ogr:NAMEen>Sudan</ogr:NAMEen>
      <ogr:GBD_ID>522</ogr:GBD_ID>
      <ogr:GBD_NAME>Sudan</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.166">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>3.490201518 24.170328031</gml:lowerCorner><gml:upperCorner>12.210366923 35.920835409</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.166"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.166.0"><gml:exterior><gml:LinearRing><gml:posList>9.454592111 34.070697863 8.445377096 33.970773559 8.4044752 33.174078003 7.801127014 33.0512948 7.657156474 33.716060425 6.637606303 34.7335177 5.622474467 35.098663371 5.309108581 35.809110962 4.619331563 35.920835409 5.0303758226 35.4115983372 4.219691875 33.977078085 3.774292705 33.53260909 3.520897318 32.174552449 3.815814718 31.780984741 3.490201518 30.839543498 4.669535217 29.457612345 4.27782786 28.404136596 5.070725199 27.44130131 5.768744608 27.123801311 6.104951477 26.481049846 6.6532901 26.378007039 7.335574036 25.360032999 8.68932733 24.170328031 8.886745504 24.558376505 10.293222962 25.084080851 10.417814839 25.843052612 9.520453593 26.556859172 9.595410258 27.895072062 9.324544836 28.843840924 10.270485332 30.012978963 9.735763448 30.749264771 9.792323304 31.234816528 11.050851135 32.414072713 11.9998113 32.082206665 12.210366923 33.209217977 10.843241069 33.18208785 10.192040507 33.902095581 9.454592111 34.070697863</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SS</ogr:ISO2>
      <ogr:ISO3>SSD</ogr:ISO3>
      <ogr:NAMEen>South Sudan</ogr:NAMEen>
      <ogr:GBD_ID>435</ogr:GBD_ID>
      <ogr:GBD_NAME>South Sudan</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.167">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>12.305606588 -17.178212043</gml:lowerCorner><gml:upperCorner>16.636660055 -11.388421591</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.167"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.167.0"><gml:exterior><gml:LinearRing><gml:posList>14.774939067 -12.264130412 13.360813497 -11.613110718 12.403895162 -11.388421591 12.305606588 -12.36092037 12.673387757 -13.728278768 12.6794339 -15.195114299 12.332530837 -16.7284367771 13.065008856 -16.753651496 13.1305108817 -16.0972412049 13.470056408 -15.6552237486 13.589972636 -15.137650106 13.3053416455 -14.30595433 13.4090589101 -13.9662047098 13.5829768153 -14.2394964658 13.819984436 -15.097704224 13.5869141589 -16.5613991701 14.653143622 -17.178212043 15.80882396 -16.542347786 16.4882628454 -16.1595803585 16.636660055 -14.34325415 14.774939067 -12.264130412</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SN</ogr:ISO2>
      <ogr:ISO3>SEN</ogr:ISO3>
      <ogr:NAMEen>Senegal</ogr:NAMEen>
      <ogr:GBD_ID>216</ogr:GBD_ID>
      <ogr:GBD_NAME>Senegal</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.168">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-10.7237775948 158.595957879</gml:lowerCorner><gml:upperCorner>-7.58587005 162.113617384</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.168"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.168.0"><gml:exterior><gml:LinearRing><gml:posList>-10.0392086359 161.6911158236 -10.456475519 162.113617384 -10.7237775948 161.9147680078 -10.5388576024 161.4621071738 -10.1782775918 161.3053229011 -10.0392086359 161.6911158236</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.168.1"><gml:exterior><gml:LinearRing><gml:posList>-9.3771085016 159.6175514515 -9.2281311882 160.0909048409 -9.6050073251 160.7865962363 -10.00409519 160.9232172042 -10.0783048147 160.402137312 -9.793877863 159.835622592 -9.3771085016 159.6175514515</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.168.2"><gml:exterior><gml:LinearRing><gml:posList>-8.358819269 160.800466342 -8.5658739397 161.2101170662 -9.509047133 161.381114129 -9.15211354 160.867930535 -8.358819269 160.800466342</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.168.3"><gml:exterior><gml:LinearRing><gml:posList>-7.58587005 158.595957879 -8.2784932419 160.2429488908 -8.7042709402 160.2477488718 -8.0624301052 158.8595300429 -7.58587005 158.595957879</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SB</ogr:ISO2>
      <ogr:ISO3>SLB</ogr:ISO3>
      <ogr:NAMEen>Solomon Islands</ogr:NAMEen>
      <ogr:GBD_ID>28</ogr:GBD_ID>
      <ogr:GBD_NAME>Solomon Islands</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.169">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>6.919419664 -13.301096158</gml:lowerCorner><gml:upperCorner>9.996005961 -10.28223588</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.169"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.169.0"><gml:exterior><gml:LinearRing><gml:posList>8.484625346 -10.28223588 7.76903595 -10.615187134 6.919419664 -11.476185676 7.389837958 -12.506581184 8.171291408 -13.157704231 9.041489976 -13.301096158 9.860381165 -12.508327393 9.996005961 -11.272666382 8.484625346 -10.28223588</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SL</ogr:ISO2>
      <ogr:ISO3>SLE</ogr:ISO3>
      <ogr:NAMEen>Sierra Leone</ogr:NAMEen>
      <ogr:GBD_ID>217</ogr:GBD_ID>
      <ogr:GBD_NAME>Sierra Leone</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.170">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>13.245266018 -90.092981728</gml:lowerCorner><gml:upperCorner>14.415477804 -87.703608358</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.170"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.170.0"><gml:exterior><gml:LinearRing><gml:posList>14.415477804 -89.36162085 13.851197002 -88.496815755 13.814997661 -87.703608358 13.406561591 -87.817168749 13.245266018 -88.785674608 13.7289030479 -90.092981728 14.415477804 -89.36162085</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SV</ogr:ISO2>
      <ogr:ISO3>SLV</ogr:ISO3>
      <ogr:NAMEen>El Salvador</ogr:NAMEen>
      <ogr:GBD_ID>127</ogr:GBD_ID>
      <ogr:GBD_NAME>El Salvador</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.171">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>42.246289302 18.901305786</gml:lowerCorner><gml:upperCorner>46.108091126 22.981366822</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.171"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.171.0"><gml:exterior><gml:LinearRing><gml:posList>46.108091126 20.242825969 44.864859517 21.368545369 44.228434539 22.691640373 43.807921448 22.349467407 43.198992208 22.981366822 42.313439026 22.345023234 42.246289302 21.56406571 42.669618836 21.76438684 43.257412415 20.8194316 42.82743866 20.34535201 43.532796122 19.195344686 44.0055586499 19.3904035405 44.4457522097 19.240471937 44.88713206 19.36866744 44.865634664 19.01582076 45.931202698 18.901305786 46.108091126 20.242825969</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>RS</ogr:ISO2>
      <ogr:ISO3>SRB</ogr:ISO3>
      <ogr:NAMEen>Serbia</ogr:NAMEen>
      <ogr:GBD_ID>53</ogr:GBD_ID>
      <ogr:GBD_NAME>Serbia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.172">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>2.0374662082 -58.067691203</gml:lowerCorner><gml:upperCorner>6.040526734 -54.003029745</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.172"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.172.0"><gml:exterior><gml:LinearRing><gml:posList>5.348374742 -54.170969205 4.912802022 -54.482121949 3.455397441 -54.003029745 2.326267396 -54.615292115 2.543050029 -54.978577434 2.333088684 -56.116802531 2.0374662082 -56.4944124319 4.151143087 -58.067691203 5.484930731 -57.247670051 6.040526734 -57.0246599988 5.7888870089 -54.0511833664 5.348374742 -54.170969205</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SR</ogr:ISO2>
      <ogr:ISO3>SUR</ogr:ISO3>
      <ogr:NAMEen>Suriname</ogr:NAMEen>
      <ogr:GBD_ID>118</ogr:GBD_ID>
      <ogr:GBD_NAME>Suriname</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>South America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.173">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>47.750006409 16.945042766</gml:lowerCorner><gml:upperCorner>49.510260722 22.539636678</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.173"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.173.0"><gml:exterior><gml:LinearRing><gml:posList>49.072199606 22.539636678 48.404798483 22.132839803 48.526083069 20.481674439 48.12962148 19.884294881 47.750006409 17.825712524 48.005443014 17.14833785 48.604166158 16.945042766 49.510260722 18.833196249 49.205886536 19.747765747 49.377245586 21.81957727 49.072199606 22.539636678</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SK</ogr:ISO2>
      <ogr:ISO3>SVK</ogr:ISO3>
      <ogr:NAMEen>Slovakia</ogr:NAMEen>
      <ogr:GBD_ID>54</ogr:GBD_ID>
      <ogr:GBD_NAME>Slovakia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.174">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>45.4768970263 13.5331741824</gml:lowerCorner><gml:upperCorner>46.862773743 16.515301554</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.174"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.174.0"><gml:exterior><gml:LinearRing><gml:posList>46.50171051 16.515301554 46.2256765525 15.7021412095 45.9452217611 15.6345859905 45.8048974331 15.409000313 45.4768970263 15.3608683482 45.598722106 14.4520942552 45.488836981 13.589528842 45.593207098 13.7051558128 45.9008391125 13.7097930591 46.2217184825 13.5331741824 46.519745586 13.6943058283 46.378643087 14.540331665 46.862773743 16.094035278 46.50171051 16.515301554</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SI</ogr:ISO2>
      <ogr:ISO3>SVN</ogr:ISO3>
      <ogr:NAMEen>Slovenia</ogr:NAMEen>
      <ogr:GBD_ID>55</ogr:GBD_ID>
      <ogr:GBD_NAME>Slovenia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.175">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>55.345445054 11.231700066</gml:lowerCorner><gml:upperCorner>69.036355693 24.16309655</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.175"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.175.0"><gml:exterior><gml:LinearRing><gml:posList>65.822699286 24.16309655 65.53534577 21.853526238 63.878607489 20.786468946 63.459418036 19.280772332 62.218410549 17.508636915 60.99217357 17.167735222 59.784979559 19.0420028 59.140041408 18.414317254 58.607163804 16.789317254 57.471747137 16.688975457 56.086371161 15.851735873 56.147406317 14.685801629 55.52362702 14.363047722 55.345445054 13.280528191 58.339667059 11.231700066 58.991725979 11.437510613 60.117985332 12.510946492 63.288902893 11.992425171 63.956355693 12.681892131 64.009530742 13.93943811 64.584017639 13.642763713 65.31808136 14.514286743 66.1254481 14.540538371 67.052704163 16.41556604 67.422759095 16.127108195 68.535921123 18.171841268 68.350196025 19.931010376 69.036355693 20.62316451 67.882162578 23.498854614 65.822699286 24.16309655</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SE</ogr:ISO2>
      <ogr:ISO3>SWE</ogr:ISO3>
      <ogr:NAMEen>Sweden</ogr:NAMEen>
      <ogr:GBD_ID>93</ogr:GBD_ID>
      <ogr:GBD_NAME>Sweden</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>2. Developed region: nonG7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.176">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-27.316264343 30.7343610625</gml:lowerCorner><gml:upperCorner>-25.743647155 32.113884318</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.176"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.176.0"><gml:exterior><gml:LinearRing><gml:posList>-25.95810435 31.949243204 -26.840014343 32.113884318 -27.316264343 31.968260132 -27.205573426 31.157043497 -26.7092737533 30.7343610625 -26.1715482936 30.8000898689 -25.743647155 31.426897827 -25.95810435 31.949243204</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SZ</ogr:ISO2>
      <ogr:ISO3>SWZ</ogr:ISO3>
      <ogr:NAMEen>Swaziland</ogr:NAMEen>
      <ogr:GBD_ID>197</ogr:GBD_ID>
      <ogr:GBD_NAME>Eswatini</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.177">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>32.316788229 35.757590349</gml:lowerCorner><gml:upperCorner>37.109984029 42.357238403</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.177"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.177.0"><gml:exterior><gml:LinearRing><gml:posList>37.109984029 42.357238403 36.527383932 41.414866984 34.768473206 41.195655558 34.331497294 40.690466756 33.371685079 38.774511352 32.316788229 36.819385213 32.744346858 35.757590349 33.4067217 35.821099894 34.0779836581 36.5485591885 34.6251981176 36.7411761638 34.6498489555 35.9698996554 35.9177504328 35.9113054189 36.229004211 36.664252563 36.827520651 36.658568156 36.634199117 37.446116984 36.905526225 38.190050903 36.742151184 39.765251913 37.100475566 40.708966919 37.109984029 42.357238403</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SY</ogr:ISO2>
      <ogr:ISO3>SYR</ogr:ISO3>
      <ogr:NAMEen>Syria</ogr:NAMEen>
      <ogr:GBD_ID>153</ogr:GBD_ID>
      <ogr:GBD_NAME>Syrian Arab Republic</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.178">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>7.523262838 13.449183797</gml:lowerCorner><gml:upperCorner>23.444719951 23.984406372</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.178"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.178.0"><gml:exterior><gml:LinearRing><gml:posList>19.496123759 23.981305786 15.721160381 23.984406372 15.704262187 23.094641561 14.122627462 22.531471802 13.059152323 21.935125773 11.396101379 22.914807576 10.919153747 22.861064087 11.000828349 22.460468384 9.138125509 20.435165649 9.015264791 19.100570109 8.047881979 18.589283488 7.985198466 17.67977828 7.550237936 16.768619425 7.870011699 16.54868453 7.523262838 15.481049439 8.479147644 15.183703247 9.637759094 13.947602987 9.978177593 14.181697225 9.991277568 15.681243937 10.793114929 15.065880981 11.530821838 15.135644165 12.5368490974 14.6935040769 13.0826216418 14.4578984438 13.0750351902 14.0665556526 13.7057695706 13.6032181205 14.380131124 13.449183797 15.749634095 14.368972615 16.876231994 15.452420695 19.903540751 15.736020955 20.374571432 15.953992147 21.507267151 15.180395955 22.99566376 14.979909027 23.444719951 15.985101359 21.87489329 19.164132121 20.654967754 21.63447229 19.496123759 23.981305786</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>TD</ogr:ISO2>
      <ogr:ISO3>TCD</ogr:ISO3>
      <ogr:NAMEen>Chad</ogr:NAMEen>
      <ogr:GBD_ID>204</ogr:GBD_ID>
      <ogr:GBD_NAME>Chad</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.179">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>6.100490627 -0.166109171</gml:lowerCorner><gml:upperCorner>11.134980367 1.619639519</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.179"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.179.0"><gml:exterior><gml:LinearRing><gml:posList>10.992740987 0.901474243 10.367094421 0.768769164 9.996471049 1.331008748 9.049526266 1.601173136 6.213893947 1.619639519 6.100490627 1.185394727 6.832245585 0.51927535 7.323585104 0.645675903 9.575747376 0.230197388 10.283223572 0.3965955 11.134980367 -0.166109171 10.992740987 0.901474243</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>TG</ogr:ISO2>
      <ogr:ISO3>TGO</ogr:ISO3>
      <ogr:NAMEen>Togo</ogr:NAMEen>
      <ogr:GBD_ID>218</ogr:GBD_ID>
      <ogr:GBD_NAME>Togo</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.180">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>5.6376415 97.379099569</gml:lowerCorner><gml:upperCorner>20.444024557 105.650997762</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.180"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.180.0"><gml:exterior><gml:LinearRing><gml:posList>20.31780487 100.099295288 20.158021342 100.548673544 19.53710317 100.46196049 19.570279439 101.246202026 18.427791036 101.030297892 17.461674296 101.13230717 18.21379893 102.078502645 17.850074362 102.667613973 18.441459452 103.386639852 18.318443705 104.000039103 17.372790833 104.816423381 16.528914693 104.754515015 15.634602356 105.650997762 14.345740458 105.184307903 14.295821025 103.085004517 13.564599508 102.332233928 11.645900783 102.913584832 12.672796942 101.830251498 12.707912502 100.837412957 13.471380927 100.963389519 13.402736721 100.038828972 12.192694403 100.020030144 10.128322658 99.159922722 9.232489325 99.255056186 9.294663804 99.853526238 8.317084052 100.271494988 7.5379581379 100.4034894539 6.856675229 100.997573081 6.83222077 101.566254102 6.257513739 102.07309004 5.739909159 101.800483439 5.6376415 101.105435426 6.246467387 101.081664266 6.442287502 100.127289259 8.379624742 98.655446811 8.534369208 98.199473504 10.350531317 98.747406446 11.815765686 99.630021606 13.171341858 99.102095175 13.714874573 99.152531372 15.125796204 98.164994751 15.325964865 98.533551066 16.273478089 98.682379192 17.011572571 98.436192668 18.520782776 97.379099569 19.802978414 98.024538208 19.67585439 98.53262089 20.444024557 99.942043905 20.31780487 100.099295288</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>TH</ogr:ISO2>
      <ogr:ISO3>THA</ogr:ISO3>
      <ogr:NAMEen>Thailand</ogr:NAMEen>
      <ogr:GBD_ID>18</ogr:GBD_ID>
      <ogr:GBD_NAME>Thailand</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.181">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>36.7048408 67.350079794</gml:lowerCorner><gml:upperCorner>41.025920716 74.892306763</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.181"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.181.0"><gml:exterior><gml:LinearRing><gml:posList>40.238371888 70.958955119 39.986914368 69.313990926 39.515625305 69.300348348 39.285096741 71.732913045 39.44834259 73.632642049 38.602838644 73.797386515 38.510673727 74.776344849 37.231113587 74.892306763 37.459471741 73.276074667 36.7048408 71.611060018 37.898359681 71.597727499 38.443520203 70.76108606 37.541430562 70.157867065 37.586027324 69.528654826 36.930873516 68.011124715 37.188868103 67.780544475 38.174053447 68.360664103 38.993770854 68.085642131 39.219829407 67.350079794 39.54843984 68.517450399 40.802678529 69.435843953 41.025920716 70.445704793 40.456136373 70.353824097 40.238371888 70.958955119</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>TJ</ogr:ISO2>
      <ogr:ISO3>TJK</ogr:ISO3>
      <ogr:NAMEen>Tajikistan</ogr:NAMEen>
      <ogr:GBD_ID>39</ogr:GBD_ID>
      <ogr:GBD_NAME>Tajikistan</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.182">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>35.147519837 52.4376706173</gml:lowerCorner><gml:upperCorner>42.780852356 66.554159384</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.182"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.182.0"><gml:exterior><gml:LinearRing><gml:posList>37.3641804 66.51958785 37.578379212 65.761442912 37.233232321 65.063087606 36.249984843 64.444572388 35.856262106 63.342934204 35.147519837 62.302015828 35.61849884 61.269675741 36.647790019 61.075475708 36.637144674 60.342290487 37.694651184 58.80109257 37.88504314 57.7902916814 38.265081482 57.218346802 38.075894063 55.423107544 37.517452698 54.783663371 37.342759507 53.913747592 38.901760158 53.977549675 39.949530341 53.567067905 40.053615627 52.738454623 41.069344576 52.850626343 40.625677802 53.721039259 41.115179755 54.716075066 42.078843492 53.896657748 42.0518562957 53.3883454888 41.8176312082 52.8633801047 41.1371947361 52.808278209 41.7488757414 52.4376706173 42.126628723 52.978708944 42.318064067 54.195275513 41.290814107 55.429515421 41.321716614 55.978422486 41.254123841 57.010194132 41.9264799805 57.0678011871 41.7137378972 57.3585332315 41.5818655748 57.6460428773 41.7677933092 57.8881172646 42.1517623718 57.6438448365 42.780852356 58.612266887 42.199518332 60.028201131 41.389516093 60.094243612 41.124984436 61.877907349 40.009238586 62.452807658 38.961679789 64.120612834 38.237408753 65.604139852 38.026853129 66.554159384 37.3641804 66.51958785</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>TM</ogr:ISO2>
      <ogr:ISO3>TKM</ogr:ISO3>
      <ogr:NAMEen>Turkmenistan</ogr:NAMEen>
      <ogr:GBD_ID>40</ogr:GBD_ID>
      <ogr:GBD_NAME>Turkmenistan</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.183">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>9.8654905174 -61.3936382625</gml:lowerCorner><gml:upperCorner>10.9107963664 -60.2208175163</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.183"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.183.0"><gml:exterior><gml:LinearRing><gml:posList>10.800746984 -60.2208175163 9.8654905174 -60.5242114293 10.2400614612 -61.2595093492 10.9107963664 -61.3936382625 10.800746984 -60.2208175163</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>TT</ogr:ISO2>
      <ogr:ISO3>TTO</ogr:ISO3>
      <ogr:NAMEen>Trinidad and Tobago</ogr:NAMEen>
      <ogr:GBD_ID>119</ogr:GBD_ID>
      <ogr:GBD_NAME>Trinidad and Tobago</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.184">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>30.228905335 7.479832398</gml:lowerCorner><gml:upperCorner>37.3556819345 11.5641309</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.184"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.184.0"><gml:exterior><gml:LinearRing><gml:posList>33.1812253147 11.505111803 32.465487163 11.5641309 31.411830547 10.108095744 30.915633443 10.270153036 30.228905335 9.519707885 32.07184194 9.045008179 33.207664083 7.750720255 33.893901265 7.479832398 34.647653708 8.236478719 36.448784078 8.349236695 36.9395107635 8.6025104286 37.3556819345 9.9317372161 36.999521317 11.3067396837 36.0596545195 10.9848302788 35.23823214 11.160781762 34.325628973 10.126719597 33.702826239 10.330251498 33.1812253147 11.505111803</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>TN</ogr:ISO2>
      <ogr:ISO3>TUN</ogr:ISO3>
      <ogr:NAMEen>Tunisia</ogr:NAMEen>
      <ogr:GBD_ID>154</ogr:GBD_ID>
      <ogr:GBD_NAME>Tunisia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.185">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>35.9177504328 26.0439559255</gml:lowerCorner><gml:upperCorner>42.091747132 44.8346988445</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.185"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.185.0"><gml:exterior><gml:LinearRing><gml:posList>41.97256094 28.016774936 41.3893845085 28.5649604107 41.2238341077 29.1370488388 41.0075173081 29.1290810713 40.989203192 27.524180535 40.601996161 26.828949415 40.4066923807 26.2158644583 40.73847077 26.0439559255 41.378457337 26.636595907 41.713036397 26.333358602 42.091747132 27.273352906 41.97256094 28.016774936</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.185.1"><gml:exterior><gml:LinearRing><gml:posList>41.5142276065 41.520762566 41.572347311 42.819949178 41.106587626 43.4404281 40.110162659 43.665427287 39.702797343 44.774558553 39.561142994 44.8346988445 39.400283508 44.061372111 37.968820496 44.221465698 37.141920065 44.766135295 37.358909404 42.722177368 37.109984029 42.357238403 37.100475566 40.708966919 36.742151184 39.765251913 36.905526225 38.190050903 36.634199117 37.446116984 36.827520651 36.658568156 36.229004211 36.664252563 35.9177504328 35.9113054189 36.926336981 36.020274285 36.544378973 35.347992384 36.767645575 34.554698113 36.143215236 33.685313347 36.027167059 32.80372155 36.543443101 32.02605228 36.891180731 30.68653405 36.248114325 30.400401238 36.123358466 29.672373894 36.751288153 28.938975457 36.657782294 27.671153191 37.0155558825 27.8845116578 36.963364976 27.263194207 37.7777320419 27.1172740121 38.1216010052 26.4472254948 38.5717596401 26.1680597776 38.9754551408 26.7031522424 39.474514065 26.074229363 40.403550523 26.735606316 40.434068101 29.151540561 41.240708726 29.231781446 41.091986395 31.231211785 42.019029039 33.324473504 41.736273505 35.956797722 41.240423895 36.451019727 40.91787344 38.33090254 40.922796942 40.142751498 41.5142276065 41.520762566</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>TR</ogr:ISO2>
      <ogr:ISO3>TUR</ogr:ISO3>
      <ogr:NAMEen>Turkey</ogr:NAMEen>
      <ogr:GBD_ID>155</ogr:GBD_ID>
      <ogr:GBD_NAME>Turkey</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>4. Emerging region: MIKT</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.186">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>21.8922591271 120.059418165</gml:lowerCorner><gml:upperCorner>25.22280508 121.808848504</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.186"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.186.0"><gml:exterior><gml:LinearRing><gml:posList>25.22280508 121.635996941 24.805161851 121.808848504 23.1703344586 121.5628161436 21.9416664402 121.0742603766 21.8922591271 120.5955862961 23.151027736 120.059418165 23.774807033 120.189219597 25.050238348 121.059336785 25.22280508 121.635996941</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>TW</ogr:ISO2>
      <ogr:ISO3>TWN</ogr:ISO3>
      <ogr:NAMEen>Taiwan</ogr:NAMEen>
      <ogr:GBD_ID>8</ogr:GBD_ID>
      <ogr:GBD_NAME>Taiwan (Province of China)</ogr:GBD_NAME>
      <ogr:WB_ECONOMY xsi:nil="true"/>
      <ogr:WB_GROUP xsi:nil="true"/>
      <ogr:CONTINENT xsi:nil="true"/>
      <ogr:WB_REGION xsi:nil="true"/>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.187">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-11.72259084 29.6543531249</gml:lowerCorner><gml:upperCorner>-1.002573344 40.436859571</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.187"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.187.0"><gml:exterior><gml:LinearRing><gml:posList>-1.002573344 31.7827295994 -2.0389465306 31.6110317142 -2.6971447188 31.8265062553 -2.5181713295 33.2214978538 -1.0524528975 34.0741009683 -3.045962829 37.644864542 -3.513427836 37.599079223 -4.677504165 39.190603061 -6.030694269 38.778005405 -6.857679946 39.464528842 -8.309258722 39.28003991 -9.982842706 39.763194207 -10.474786066 40.436859571 -11.413462423 38.492254679 -11.319101257 37.875238077 -11.72259084 37.427823527 -11.701506856 36.199681437 -11.413462423 35.82637089 -11.57355601 34.964614706 -11.418836771 34.917072388 -11.395995789 34.894128052 -11.2686444765 34.7416643137 -11.234558614 34.719771769 -9.708450216 34.288066853 -9.606524565 34.1903125879 -9.522208354 34.089319296 -9.510185831 34.078490688 -9.717958679 33.911862426 -9.407900086 32.920863485 -8.5890077003 31.1666602765 -7.5079376485 30.5834065464 -6.9562992743 30.5645672423 -6.2713139505 29.7503560666 -5.9177826338 29.9461082112 -4.4505910788 29.6543531249 -4.271934509 30.003005411 -2.978576761 30.825487508 -2.400627543 30.55459965 -1.59416514 30.831016887 -1.066836592 30.471785843 -1.002573344 31.7827295994</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>TZ</ogr:ISO2>
      <ogr:ISO3>TZA</ogr:ISO3>
      <ogr:NAMEen>Tanzania</ogr:NAMEen>
      <ogr:GBD_ID>189</ogr:GBD_ID>
      <ogr:GBD_NAME>United Republic of Tanzania</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.188">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-1.3930817714 29.57793851</gml:lowerCorner><gml:upperCorner>4.219691875 34.978670695</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.188"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.188.0"><gml:exterior><gml:LinearRing><gml:posList>0.2459167412 33.9956852793 0.3863195129 33.270656663 -0.1341162639 31.9198196836 -1.002573344 31.7827295994 -1.066836592 30.471785843 -1.3930817714 29.57793851 -0.4850430547 29.6410151842 -0.0986732303 29.6887608019 0.785017802 29.92828129 1.2387726624 30.4783817597 1.8187516415 31.1276079405 2.170394737 31.2693446377 2.462279765 30.707665243 3.490201518 30.839543498 3.815814718 31.780984741 3.520897318 32.174552449 3.774292705 33.53260909 4.219691875 33.977078085 2.477317607 34.923583619 1.675945333 34.978670695 0.2459167412 33.9956852793</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>UG</ogr:ISO2>
      <ogr:ISO3>UGA</ogr:ISO3>
      <ogr:NAMEen>Uganda</ogr:NAMEen>
      <ogr:GBD_ID>190</ogr:GBD_ID>
      <ogr:GBD_NAME>Uganda</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.189">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>44.38300202 22.132839803</gml:lowerCorner><gml:upperCorner>52.35460907 40.141663045</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.189"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.189.0"><gml:exterior><gml:LinearRing><gml:posList>47.103257554 38.216644727 46.652736721 35.901621941 46.166245835 34.81128991 45.37124258 35.332204623 45.3797036399 36.2612047858 45.0897417379 36.0571274955 44.38300202 33.957855665 45.630072333 32.868174675 45.92963288 33.777028842 46.62913646 31.186859571 45.559068101 29.589691602 45.215887762 29.65902754 45.461773987 28.199497925 45.7285548955 28.636325653 46.45478831 28.94580896 46.341461894 29.84776941 46.809753723 29.928177938 47.870722555 29.236023803 48.451979066 27.751773315 48.258967591 26.617889038 47.710060527 24.896598755 47.946738587 22.877600546 48.404798483 22.132839803 49.072199606 22.539636678 49.528760885 22.640922485 50.540843811 24.10770634 51.517399191 23.606238241 51.880012716 24.390789836 51.928511047 25.76791508 51.595378927 27.25402592 51.484429627 30.148629598 52.074677836 30.959329467 52.100567729 31.76434493 52.35460907 33.804065389 51.248914286 34.185954224 51.183181865 35.099180135 50.345145162 35.696869751 50.424933573 37.435264933 49.858508199 39.182755168 49.245780741 40.141663045 49.020316468 39.681122681 47.832947083 39.759050741 47.848243306 38.830942017 47.103257554 38.216644727</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>UA</ogr:ISO2>
      <ogr:ISO3>UKR</ogr:ISO3>
      <ogr:NAMEen>Ukraine</ogr:NAMEen>
      <ogr:GBD_ID>63</ogr:GBD_ID>
      <ogr:GBD_NAME>Ukraine</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Europe</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.190">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-34.969903253 -58.388824023</gml:lowerCorner><gml:upperCorner>-30.102037455 -53.1724546853</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.190"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.190.0"><gml:exterior><gml:LinearRing><gml:posList>-33.7406759724 -53.3790945694 -34.39039479 -53.764881965 -34.969903253 -54.939320442 -34.90618255 -56.311268684 -34.462985935 -57.120025194 -34.470967085 -57.855354162 -33.942071222 -58.388824023 -32.4471299123 -58.2001118521 -30.182962748 -57.611698365 -30.102037455 -56.831280885 -30.798222351 -56.011356771 -31.471049499 -54.591520956 -32.6592425794 -53.1724546853 -33.5597688778 -53.5367920278 -33.7406759724 -53.3790945694</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>UY</ogr:ISO2>
      <ogr:ISO3>URY</ogr:ISO3>
      <ogr:NAMEen>Uruguay</ogr:NAMEen>
      <ogr:GBD_ID>99</ogr:GBD_ID>
      <ogr:GBD_NAME>Uruguay</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>South America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.191">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>18.9415301746 -171.6725968281</gml:lowerCorner><gml:upperCorner>71.30634182 -67.1760148371</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.191"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.191.0"><gml:exterior><gml:LinearRing><gml:posList>20.1914139212 -155.7286750347 19.8119678601 -154.9917607147 19.2182769231 -154.0011474991 18.9415301746 -154.6289157004 19.3503960947 -155.356381876 19.7188038047 -156.015147019 20.263576209 -157.0486693146 20.1914139212 -155.7286750347</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.191.1"><gml:exterior><gml:LinearRing><gml:posList>47.9963331198 -89.5817526941 47.4931163875 -91.012379962 46.8040099661 -92.0953455857 46.6004446766 -91.5844188642 46.9685595131 -90.8843315668 46.5764664959 -90.435279091 47.6571945852 -88.2879335198 47.4561896511 -88.0448985329 47.0051828717 -88.1030850409 46.5388740762 -87.1512158525 46.7863481185 -84.9527472241 46.4576578772 -84.4910523084 46.3463741605 -84.1059436185 45.8570405694 -84.734476224 45.909437384 -86.9994609998 45.0072113346 -87.636612153 44.8035113587 -87.3279713658 43.2672249796 -87.907279968 42.2793389692 -87.8225030995 41.6279111561 -87.2919117605 41.8760601152 -86.6565267922 42.9643242929 -86.2223397046 43.6275266712 -86.542902239 44.688414985 -86.2695851468 45.3709737054 -85.2487420846 45.6255190391 -84.5431119253 45.3586821023 -83.5102158195 44.3295851142 -83.3533197398 43.92130207 -82.6893814512 43.0174198005 -82.4163685665 42.5575330121 -82.6446981164 41.7215189485 -83.4591436437 41.3956578598 -82.5048151437 41.5222344344 -81.6722062102 42.4070930004 -79.4708040857 42.8456955497 -78.8675048072 42.9457793529 -78.9269663609 43.105730209 -79.0652311503 43.2782003383 -79.0586315217 43.3387554625 -76.7127593969 44.4965126518 -75.7914962913 44.9992536226 -74.7129614499 45.0150542586 -71.5148352757 45.7315249384 -70.4073514996 46.7040753931 -70.0077634866 47.4398444359 -69.2676277858 47.035631383 -67.805184752 45.7624138603 -67.8108158023 45.604585751 -67.4409387966 45.1786562916 -67.1760148371 44.5430556063 -67.9230355717 44.4845658232 -69.2592911177 43.2748883891 -70.5816544846 42.7710032507 -70.7740603337 42.1737332353 -70.6333497412 41.7537124695 -69.939443925 41.5789198853 -70.0067099135 41.4817979233 -71.4808260244 41.2449747186 -72.8924267533 40.9965680767 -73.6502580002 40.6620018633 -73.8632079403 41.029992458 -71.9180326946 40.7255563625 -71.6468170497 40.0585110468 -73.9483186434 39.6655075656 -73.7835064968 38.8345242018 -74.6471110991 39.1868168091 -75.2700127247 38.4438413926 -74.8817244381 37.6637396817 -74.8737006896 37.2496799394 -75.4141546787 37.86087675 -75.7269497271 38.6964558945 -76.3793701984 37.7152453196 -76.110045855 37.0849561864 -75.9543602316 36.7547404001 -75.7082030231 35.3841820098 -76.065093612 35.5497093538 -76.5849503829 34.7319196409 -76.522775903 34.682359094 -77.1368709559 33.8922793411 -78.0323380778 33.7785505001 -78.7633357988 33.1837832472 -79.1834611247 32.4863955523 -80.3312068927 31.5443965045 -81.3333658716 30.8544375446 -81.5223283116 28.3734805139 -80.7382706948 26.8111839641 -80.038238143 25.2005068813 -80.506174341 25.1214053193 -81.086537297 25.9081077932 -81.7184953039 26.8657900841 -82.0570369708 27.886175826 -82.8355607337 28.8729922328 -82.6310929607 30.1065941207 -84.0240372965 29.7300478967 -84.8866268144 30.3807640426 -86.3648169182 30.4081077926 -88.859364444 30.0799827927 -89.479074731 29.6398786257 -89.722238793 29.0844900428 -89.4710344592 29.0317272872 -90.2620821891 29.2948672328 -91.3279923739 29.8312441857 -91.8475236238 29.5338364148 -92.2764931898 29.7728111657 -93.1995930067 29.3759626098 -94.8888240796 28.7638613729 -95.6019181546 28.5973574669 -96.4448136626 27.0091820111 -97.5502010326 25.965806382 -97.139271614 26.075412089 -98.22265621 26.5658300599 -99.1715476129 27.0568388944 -99.4616351591 27.573770244 -99.507203125 28.296516825 -100.284339153 29.4604523152 -101.0390261447 29.6314105479 -101.303668774 29.8939387 -102.321010702 28.985105286 -103.147988648 29.667905986 -104.530824138 30.676991679 -105.00849524 31.773823954 -106.51718868 31.777751364 -108.215121217 31.327442932 -108.214811158 31.333644104 -111.067117676 32.712836405 -114.724284628 32.53166949 -117.125121489 33.7309771841 -118.0905362668 34.2760359137 -119.2963627273 34.450272949 -120.4726814864 35.1393903439 -120.6448869012 35.6648294928 -121.2883128551 37.1945259515 -122.4044570306 37.9314261099 -122.6734582432 38.9185760621 -123.726242286 39.8335142449 -123.8540188557 40.4438176408 -124.4092020225 41.4358759616 -124.0621554092 42.8454450292 -124.5477189297 43.881740602 -124.1510311045 46.7474064231 -123.8999963225 48.1694041629 -124.7531550298 47.9868024497 -122.2127986813 48.9925148664 -122.7530123738 48.9925145204 -119.3560433034 48.9925145204 -114.5202661233 48.9926178734 -110.5637117631 48.9926178734 -107.2666261907 48.9926178734 -102.4307456586 48.9926695504 -98.0346057254 48.9926695504 -95.1771057269 48.5165487406 -93.7567789853 48.5362632995 -92.6486037335 48.0364488477 -91.4279288888 47.9963331198 -89.5817526941</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.191.2"><gml:exterior><gml:LinearRing><gml:posList>55.0603701549 -163.7565405297 54.764553098 -163.3694962591 54.5851504291 -164.958241376 55.0603701549 -163.7565405297</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.191.3"><gml:exterior><gml:LinearRing><gml:posList>56.5924441619 -133.5296027453 56.1500479092 -132.402257715 55.5386335126 -131.5600134909 55.1973876063 -131.6732657991 55.8414102822 -132.9431758368 56.5924441619 -133.5296027453</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.191.4"><gml:exterior><gml:LinearRing><gml:posList>57.3591832802 -135.6667231565 57.1150368445 -134.5484523975 56.0368930717 -133.5562684247 57.3591832802 -135.6667231565</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.191.5"><gml:exterior><gml:LinearRing><gml:posList>57.9291038774 -153.0127661152 57.5178896856 -152.9733780947 56.741196977 -154.1226293973 57.4285342166 -154.7268774433 57.9291038774 -153.0127661152</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.191.6"><gml:exterior><gml:LinearRing><gml:posList>58.2297223672 -135.7149959376 57.4860700236 -134.8195695055 57.8350283564 -136.4172257548 58.2297223672 -135.7149959376</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.191.7"><gml:exterior><gml:LinearRing><gml:posList>58.3343470039 -135.4346551747 57.9925672538 -134.3629026577 56.9737261689 -133.2033914181 57.0181493805 -133.9628610533 58.3343470039 -135.4346551747</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.191.8"><gml:exterior><gml:LinearRing><gml:posList>60.4580846858 -166.0936024146 59.8583836225 -165.3115315865 59.7083690523 -166.2072540143 60.4041105373 -167.7545562515 60.4580846858 -166.0936024146</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.191.9"><gml:exterior><gml:LinearRing><gml:posList>63.7963320632 -171.6725968281 63.2884345258 -168.5320194101 62.6055436895 -168.5210561798 63.1874484376 -170.7924027833 63.7963320632 -171.6725968281</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.191.10"><gml:exterior><gml:LinearRing><gml:posList>69.6509463146 -141.0055639467 66.8811383701 -141.0042309407 62.6535928011 -141.0022930822 60.3210736778 -141.0011562046 60.073388538 -139.1821457895 58.9077232558 -137.4231058744 59.7924754522 -135.4827591939 58.4038780411 -133.392240804 56.5950478831 -131.5858911152 55.9079518201 -130.0196184876 55.2735049208 -129.9579158627 55.282731628 -130.9598045864 55.8772761664 -131.351934377 56.4495246709 -132.2664384531 58.2143008172 -134.241607266 58.6833010133 -135.7943763605 58.3675750857 -136.8652801472 59.0807558957 -138.2879126019 59.7012392933 -140.3227433296 60.112372105 -142.7984920263 59.9970156601 -143.9186092141 61.0098330424 -147.0472306308 60.7796084335 -148.3747045887 59.9592959341 -148.4353735358 59.9975039411 -149.3889054365 59.2077090206 -151.1616105151 59.7865257512 -151.8828833001 60.9945335634 -151.5667618794 59.7101504263 -153.0552872718 59.3811709345 -154.145253092 58.8441429398 -153.2891333009 58.2167829142 -154.1657202155 57.5768089556 -156.0204565438 56.9828555059 -156.5497941104 56.4380557012 -158.4310196965 55.9540469124 -158.712025556 55.8527285535 -159.8303930688 55.0398623429 -162.2020971701 55.1195335669 -163.3345434598 55.8798688525 -161.8262833673 55.9897321344 -160.5799047875 57.6376406615 -157.7045792653 58.8866640988 -157.0844620758 58.6139997109 -158.1536759439 58.8568777615 -159.940193759 58.6596473379 -161.3638876679 59.9975039411 -162.5258276326 59.8008486682 -163.6149389606 60.5530459337 -165.4312231388 60.8257510115 -164.6691381775 61.5945498389 -166.1999405847 63.0952254933 -166.3717023081 63.2621523766 -164.145375152 63.0308291348 -163.3505346578 63.7465273762 -160.7915746972 64.9395205391 -161.1825659043 64.4125430006 -163.1349991738 64.5851911125 -166.1896866723 65.5914980775 -168.0810033999 66.5563418265 -164.7180883595 66.078599314 -163.6804907053 66.075913767 -162.179432763 66.7504983474 -161.9335596418 67.1032168259 -163.6883032023 67.7895946692 -164.6994846926 68.3588320583 -166.5481664778 68.8748232687 -166.236765109 68.9293886986 -164.3464249407 70.3206240486 -161.9057104183 70.8368593988 -158.043039519 71.30634182 -156.8172094395 70.4487164964 -151.979685032 70.5230166263 -149.488514786 69.9828148041 -144.5901586707 70.1166445559 -143.3002823688 69.6509463146 -141.0055639467</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>US</ogr:ISO2>
      <ogr:ISO3>USA</ogr:ISO3>
      <ogr:NAMEen>United States of America</ogr:NAMEen>
      <ogr:GBD_ID>102</ogr:GBD_ID>
      <ogr:GBD_NAME>United States of America</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>1. Developed region: G7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>North America</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.192">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>37.188868103 55.978422486</gml:lowerCorner><gml:upperCorner>45.5445630245 73.143291994</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.192"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.192.0"><gml:exterior><gml:LinearRing><gml:posList>44.8477769646 60.0574978679 44.382821758 61.036304972 43.480628765 62.026115357 43.7543168338 63.8668029342 43.697359721 64.956530803 42.877203065 65.795239298 41.99761851 66.017241251 41.993484395 66.504136597 41.199191793 66.688208049 41.200380351 67.937434123 40.599486796 68.593001343 41.366985168 69.031579224 42.248145854 70.947793009 41.578341777 70.169287557 41.108137919 71.180130249 41.447290344 71.753170207 40.99935903 72.165134725 40.833852782 73.143291994 40.147886455 71.673071736 40.238371888 70.958955119 40.456136373 70.353824097 41.025920716 70.445704793 40.802678529 69.435843953 39.54843984 68.517450399 39.219829407 67.350079794 38.993770854 68.085642131 38.174053447 68.360664103 37.188868103 67.780544475 37.3641804 66.51958785 38.026853129 66.554159384 38.237408753 65.604139852 38.961679789 64.120612834 40.009238586 62.452807658 41.124984436 61.877907349 41.389516093 60.094243612 42.199518332 60.028201131 42.780852356 58.612266887 42.1517623718 57.6438448365 42.1809383742 57.3784177926 41.9264799805 57.0678011871 41.254123841 57.010194132 41.321716614 55.978422486 44.996221008 55.978422486 45.5445630245 58.5908979437 45.4725188329 58.7425749644 45.280348556 59.1469282788 45.1392957925 59.4439560554 44.7390456402 59.1932447906 44.2712917219 59.3646445595 44.2253802383 59.9132594456 44.8477769646 60.0574978679</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>UZ</ogr:ISO2>
      <ogr:ISO3>UZB</ogr:ISO3>
      <ogr:NAMEen>Uzbekistan</ogr:NAMEen>
      <ogr:GBD_ID>41</ogr:GBD_ID>
      <ogr:GBD_NAME>Uzbekistan</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Europe &amp; Central Asia</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.193">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>0.733031311 -73.009724895</gml:lowerCorner><gml:upperCorner>12.1529377554 -59.815594849</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.193"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.193.0"><gml:exterior><gml:LinearRing><gml:posList>8.558010158 -60.020985481 8.287763977 -59.815594849 7.820867412 -60.503278972 7.105252177 -60.292335775 6.715844421 -61.129829875 5.905299581 -61.379607911 5.202138367 -60.739853679 4.900580546 -60.612626303 4.248527324 -61.567270875 4.020711772 -62.76621578 3.563762919 -62.931011923 3.968363546 -63.42540035 4.118871155 -64.58967037 3.594717102 -64.202975627 2.47132314 -64.047972169 2.420576884 -63.384834351 1.647394104 -64.080864218 0.733031311 -66.156344767 1.222510478 -66.875060588 2.402025045 -67.197624878 2.886129863 -67.838619345 3.425709331 -67.304646769 4.512077128 -67.865077677 6.266233622 -67.573984335 6.122237244 -69.443637655 6.94443512 -70.096621054 7.066598206 -72.080996053 7.440218811 -72.451309367 8.338613587 -72.386765503 9.295376892 -73.009724895 10.452463888 -72.907560588 11.661924948 -71.971080282 11.8499976907 -71.3275065336 11.591253973 -71.960519986 10.7147395761 -71.6067718311 9.826076565 -72.124379036 9.048976955 -71.514800585 9.760891018 -71.041859504 10.809230861 -71.5165013838 11.457750066 -70.0887373652 11.6450175023 -69.9059043713 11.7837194357 -70.2881459923 12.0212045797 -70.3553734606 12.1529377554 -70.1107759561 12.0166749572 -69.7380336194 11.5051125359 -69.7087029495 11.452866929 -68.863189257 10.466457424 -67.941232877 10.64858633 -66.251779752 10.23444245 -65.827504036 10.106878973 -64.748117642 10.619696356 -64.131214973 10.759995835 -62.719634569 10.4480402473 -62.3122743483 9.743181367 -62.6153068742 9.898770728 -61.636734699 9.338609117 -60.783802864 8.5726326047 -61.0497022505 8.558010158 -60.020985481</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>VE</ogr:ISO2>
      <ogr:ISO3>VEN</ogr:ISO3>
      <ogr:NAMEen>Venezuela</ogr:NAMEen>
      <ogr:GBD_ID>133</ogr:GBD_ID>
      <ogr:GBD_NAME>Venezuela (Bolivarian Republic of)</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>South America</ogr:CONTINENT>
      <ogr:WB_REGION>Latin America &amp; Caribbean</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.194">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>8.590765692 102.118655233</gml:lowerCorner><gml:upperCorner>23.365810039 109.465017123</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.194"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.194.0"><gml:exterior><gml:LinearRing><gml:posList>21.485663153 107.99122155 21.013373114 106.785411004 20.231993895 106.569614893 19.999172268 106.025157097 18.998602606 105.614268425 18.123521226 106.434825066 17.470404364 106.634613477 15.3867862 108.772959832 14.102754545 109.4362521656 12.913316148 109.465017123 12.3091270009 109.2934091215 11.355902411 109.018077019 10.7116009877 108.2254648771 10.378485419 107.271983269 10.3504578895 106.8888270153 9.54710521 106.484873894 8.590765692 104.752207879 9.85109514 104.892789234 10.419663804 104.451345248 10.914089457 105.0290719 11.015607808 105.752903687 11.770497131 106.015419962 11.677272848 106.435032593 12.343847148 107.514294882 14.704581605 107.5203927 15.28242747 107.657593628 16.064188538 107.298959188 17.664039205 105.735282024 18.192714946 105.451423381 19.299908346 103.848420451 20.091797994 104.956879924 20.958825175 104.056676473 20.868391419 103.115286906 21.575325012 102.974106893 22.397548726 102.118655233 22.765174866 102.44271814 22.8292514013 103.9727918576 23.365810039 105.312155396 22.904649556 105.853879435 22.744349263 106.74664148 21.995506083 106.648197877 21.485663153 107.99122155</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>VN</ogr:ISO2>
      <ogr:ISO3>VNM</ogr:ISO3>
      <ogr:NAMEen>Vietnam</ogr:NAMEen>
      <ogr:GBD_ID>20</ogr:GBD_ID>
      <ogr:GBD_NAME>Viet Nam</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.195">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-16.2041131029 166.4188973221</gml:lowerCorner><gml:upperCorner>-14.8723151819 167.7984221323</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.195"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.195.0"><gml:exterior><gml:LinearRing><gml:posList>-15.0235336548 166.9507029374 -15.752110662 167.4236301561 -16.2041131029 167.7984221323 -16.1510339006 167.2362613373 -15.6936517802 166.9696351784 -15.496426848 166.5936558487 -14.8723151819 166.4188973221 -15.0235336548 166.9507029374</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>VU</ogr:ISO2>
      <ogr:ISO3>VUT</ogr:ISO3>
      <ogr:NAMEen>Vanuatu</ogr:NAMEen>
      <ogr:GBD_ID>30</ogr:GBD_ID>
      <ogr:GBD_NAME>Vanuatu</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Oceania</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.196">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>12.598049221 42.699229363</gml:lowerCorner><gml:upperCorner>18.995637513 53.090342644</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.196"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.196.0"><gml:exterior><gml:LinearRing><gml:posList>16.642401434 53.090342644 16.263820705 52.292735222 15.616115627 52.228363477 14.737290757 49.578135613 14.043605861 48.681488477 14.057277736 48.016856316 13.430324611 46.700694207 13.341986395 45.665782097 12.729885158 44.881358269 12.598049221 43.946299675 13.212713934 43.24887129 15.717433986 42.699229363 16.370957749 42.789480014 17.325920308 43.165044393 17.428394674 45.165387411 17.09182607 47.427574911 18.148919169 48.16194869 18.612095032 49.128814738 18.995637513 51.97861495 16.642401434 53.090342644</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>YE</ogr:ISO2>
      <ogr:ISO3>YEM</ogr:ISO3>
      <ogr:NAMEen>Yemen</ogr:NAMEen>
      <ogr:GBD_ID>157</ogr:GBD_ID>
      <ogr:GBD_NAME>Yemen</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.197">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-34.80868906 16.48707116</gml:lowerCorner><gml:upperCorner>-22.186706645 32.893077019</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.197"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.197.0"><gml:exterior><gml:LinearRing><gml:posList>-22.397339783 31.288921753 -24.423107605 31.986553589 -25.95810435 31.949243204 -25.743647155 31.426897827 -26.1715482936 30.8000898689 -26.7092737533 30.7343610625 -27.205573426 31.157043497 -27.316264343 31.968260132 -26.840014343 32.113884318 -26.846123956 32.893077019 -28.552829685 32.379405144 -29.560316665 31.186859571 -30.847100519 30.392344597 -32.56243255 28.548594597 -33.525323175 27.100922071 -33.795179946 25.679047071 -34.186944269 24.857920769 -33.997247003 22.563975457 -34.360446873 20.913422071 -34.80868906 19.953461134 -33.862969659 18.488780144 -32.814629816 17.843923373 -32.647556248 18.273122592 -31.663262628 18.172862175 -30.34775156 17.281993035 -28.5729305965 16.48707116 -28.082625834 16.892952921 -28.704293315 17.403619425 -28.959368184 19.081656535 -28.4223467 19.98165328 -24.752493184 19.981446574 -26.131323751 20.84144576 -26.891794128 20.690757283 -26.855207214 21.687182251 -25.984252625 22.719367309 -25.310805359 23.006998332 -25.829223327 24.798620239 -25.619520365 25.587254272 -24.748152364 25.868374064 -24.248131205 26.849709513 -23.645842387 27.00417037 -22.584615173 28.338559204 -22.186706645 29.350073689 -22.397339783 31.288921753</gml:posList></gml:LinearRing></gml:exterior><gml:interior><gml:LinearRing><gml:posList>-28.700469258 28.758636922 -28.908621928 27.747432495 -29.62558075 27.014867391 -30.600301615 27.743608439 -29.911144714 29.150654337 -29.342393901 29.435908244 -28.700469258 28.758636922</gml:posList></gml:LinearRing></gml:interior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>ZA</ogr:ISO2>
      <ogr:ISO3>ZAF</ogr:ISO3>
      <ogr:NAMEen>South Africa</ogr:NAMEen>
      <ogr:GBD_ID>196</ogr:GBD_ID>
      <ogr:GBD_NAME>South Africa</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.198">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-17.9580343552 21.979877563</gml:lowerCorner><gml:upperCorner>-8.2217699874 33.674202515</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.198"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.198.0"><gml:exterior><gml:LinearRing><gml:posList>-8.5890077003 31.1666602765 -9.407900086 32.920863485 -10.577027689 33.674202515 -10.818149922 33.319082072 -12.139516296 33.252729533 -13.699162699 32.80779545 -14.013872172 33.202706747 -14.981461691 30.214465373 -15.635995381 30.396263061 -15.7142335 29.406969442 -16.531943312 28.761274487 -16.8636698287 27.9135682357 -17.9580343552 27.0222407437 -17.794106547 25.259780721 -17.479500427 24.220464314 -17.641144307 23.381652466 -16.543950297 22.108138469 -16.165885519 21.983804972 -13.001479187 21.979877563 -13.001479187 24.000632772 -10.8770496782 23.9676076164 -11.381216329 24.285473266 -11.199935404 25.278797648 -11.646109721 25.351971477 -12.017456563 26.697057333 -11.569628601 27.180128621 -12.293615417 27.638188517 -12.450505066 28.339747762 -13.433855896 29.168947795 -13.424244079 29.79764327 -12.15408905 29.799296915 -12.376194356 29.030351603 -11.446948751 28.35700769 -10.519460144 28.634923543 -9.3791892099 28.5160606604 -8.7357104912 28.8001333273 -8.481407219 28.893485601 -8.2217699874 30.5698657966 -8.5890077003 31.1666602765</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>ZM</ogr:ISO2>
      <ogr:ISO3>ZMB</ogr:ISO3>
      <ogr:NAMEen>Zambia</ogr:NAMEen>
      <ogr:GBD_ID>191</ogr:GBD_ID>
      <ogr:GBD_NAME>Zambia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.199">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-22.397339783 25.259780721</gml:lowerCorner><gml:upperCorner>-15.635995381 33.034965047</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.199"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.199.0"><gml:exterior><gml:LinearRing><gml:posList>-15.635995381 30.396263061 -16.001244405 30.402567587 -16.023465271 31.259982951 -16.681616312 32.968509156 -18.33288503 33.034965047 -19.46397878 32.763353719 -20.032006124 33.00726648 -20.564583435 32.513136434 -21.290327251 32.408543335 -22.397339783 31.288921753 -22.186706645 29.350073689 -21.797893168 29.038723186 -21.577854919 28.032893107 -20.509082947 27.698133179 -19.501082459 26.130270223 -17.794106547 25.259780721 -17.9580343552 27.0222407437 -16.8636698287 27.9135682357 -16.531943312 28.761274487 -15.7142335 29.406969442 -15.635995381 30.396263061</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>ZW</ogr:ISO2>
      <ogr:ISO3>ZWE</ogr:ISO3>
      <ogr:NAMEen>Zimbabwe</ogr:NAMEen>
      <ogr:GBD_ID>198</ogr:GBD_ID>
      <ogr:GBD_NAME>Zimbabwe</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>5. Emerging region: G20</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.200">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-9.485772394 123.6361778499</gml:lowerCorner><gml:upperCorner>-8.1189695954 127.7594363482</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.200"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.200.0"><gml:exterior><gml:LinearRing><gml:posList>-9.485772394 125.061615431 -8.8668875148 124.8711070826 -8.631442967 125.144297722 -8.1189695954 126.9074455555 -8.3011635904 127.7594363482 -8.7658194452 126.5279772605 -9.485772394 125.061615431</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.200.1"><gml:exterior><gml:LinearRing><gml:posList>-9.443525747 123.6361778499 -9.1110718104 123.8793276515 -8.9708519321 124.4749225128 -9.4799588898 124.2663267197 -9.443525747 123.6361778499</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>TL</ogr:ISO2>
      <ogr:ISO3>TLS</ogr:ISO3>
      <ogr:NAMEen>East Timor</ogr:NAMEen>
      <ogr:GBD_ID>19</ogr:GBD_ID>
      <ogr:GBD_NAME>Timor-Leste</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>4. Lower middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.201">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>16.642401434 51.97861495</gml:lowerCorner><gml:upperCorner>26.1080321476 59.79175866</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.201"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.201.0"><gml:exterior><gml:LinearRing><gml:posList>16.642401434 53.090342644 18.995637513 51.97861495 19.995421448 54.978380168 21.978969625 55.637564738 22.703576559 55.186842895 24.230330912 55.755697062 24.855202332 55.788666626 24.978216864 56.383311394 23.953558661 57.151540561 23.512111721 58.786143425 22.198065497 59.79175866 20.416815497 58.5185653 20.575140692 58.103526238 19.754380601 57.698903842 19.015326239 57.840098504 18.810288804 56.915049675 17.937811591 56.344899936 17.841009833 55.445160352 17.011135158 55.026866082 16.985337632 54.023448113 16.642401434 53.090342644</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.201.1"><gml:exterior><gml:LinearRing><gml:posList>25.884154699 55.8863373911 26.1080321476 56.0805912149 26.0603791898 56.37956648 25.6274456978 56.2790552346 25.6215553345 56.1159005342 25.884154699 55.8863373911</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>OM</ogr:ISO2>
      <ogr:ISO3>OMN</ogr:ISO3>
      <ogr:NAMEen>Oman</ogr:NAMEen>
      <ogr:GBD_ID>150</ogr:GBD_ID>
      <ogr:GBD_NAME>Oman</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>Middle East &amp; North Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.202">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-1.696302993 40.965385376</gml:lowerCorner><gml:upperCorner>11.989118646 51.292698162</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.202"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.202.0"><gml:exterior><gml:LinearRing><gml:posList>10.99878713 42.923715454 11.487860419 43.240733269 10.456732489 44.274261915 10.415799432 44.962008688 10.871730861 45.753591342 10.693101304 46.447764519 11.172796942 47.366872592 11.2491299763 48.9391119999 11.589300848 50.26832116 11.989118646 50.797863585 11.833107906 51.292698162 10.562811591 51.176117384 9.456122137 50.840342644 7.962713934 49.842051629 6.144232489 49.036143425 4.4570987 47.948496941 2.438137111 46.027028842 1.559068101 44.550059441 0.620550848 43.46762129 -0.862888279 42.08090254 -1.696302993 41.535085483 -0.870798441 40.979751424 2.814144593 40.965385376 3.977226054 41.885019165 4.953962301 43.96897465 4.911484273 44.941525106 6.496736348 46.423915242 7.996567281 47.979169149 7.996567281 46.979230184 8.985525004 44.023855021 9.41301829 43.419034057 10.632220358 42.647246541 10.99878713 42.923715454</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>SO</ogr:ISO2>
      <ogr:ISO3>SOM</ogr:ISO3>
      <ogr:NAMEen>Somalia</ogr:NAMEen>
      <ogr:GBD_ID>187</ogr:GBD_ID>
      <ogr:GBD_NAME>Somalia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>7. Least developed region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>5. Low income</ogr:WB_GROUP>
      <ogr:CONTINENT>Africa</ogr:CONTINENT>
      <ogr:WB_REGION>Sub-Saharan Africa</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.203">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>42.037960385 -141.0055639467</gml:lowerCorner><gml:upperCorner>83.1165224297 -52.9412736285</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.203"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.0"><gml:exterior><gml:LinearRing><gml:posList>46.1487283334 -83.8374484026 45.9159572296 -81.8644401349 45.3902173523 -81.8843968296 46.0680297394 -83.9979469975 46.1487283334 -83.8374484026</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.1"><gml:exterior><gml:LinearRing><gml:posList>47.0533612591 -64.3375891176 46.5747171192 -63.2801238527 46.4546572682 -61.976958848 45.9718284924 -62.5452368429 46.4168549253 -64.0988149102 47.0533612591 -64.3375891176</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.2"><gml:exterior><gml:LinearRing><gml:posList>47.001206747 -60.5011287703 46.1698672293 -59.8105363223 45.6425234795 -60.3953751892 45.5247392525 -60.9063971586 46.0404726973 -61.541127142 47.001206747 -60.5011287703</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.3"><gml:exterior><gml:LinearRing><gml:posList>49.950140665 -64.1328019467 49.3544782303 -61.8826391871 49.0679385174 -62.248199083 49.3899600012 -63.5867814388 49.950140665 -64.1328019467</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.4"><gml:exterior><gml:LinearRing><gml:posList>51.3169592633 -55.7001072842 50.8595025814 -55.2276376194 50.051275124 -55.6803575311 49.4291445712 -55.6890941794 49.2987226224 -55.0700185603 49.5291201572 -54.5447485635 49.2421735433 -53.4536841107 48.5681826575 -53.9034318315 48.4884582943 -53.2331678458 47.948614837 -53.2660118378 47.6564133254 -52.9746250343 46.7913271901 -52.9412736285 46.6183535572 -53.5546769484 47.140497 -54.2600702902 46.9188499761 -55.2508846302 46.9043642991 -55.950306831 47.5397377308 -55.7556849904 47.5607770589 -59.1116430935 47.8803164468 -59.3988338485 48.6017519935 -58.6853735627 50.458448348 -57.3924321203 50.9652376568 -56.6256565783 51.3169592633 -55.7001072842</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.5"><gml:exterior><gml:LinearRing><gml:posList>53.3739544333 -81.4342143997 52.7768741291 -80.5115360192 52.6377354792 -81.0650763539 53.0227724998 -82.0626522037 53.3739544333 -81.4342143997</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.6"><gml:exterior><gml:LinearRing><gml:posList>53.9336698587 -132.6943993822 53.8507755695 -131.8502769678 53.1309897129 -130.913150502 52.4637641273 -130.0779922546 52.87623461 -131.2577535913 53.2020428559 -131.8577850039 53.9336698587 -132.6943993822</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.7"><gml:exterior><gml:LinearRing><gml:posList>63.0619333839 -81.7353521799 62.2682881353 -82.7618150666 62.4914004243 -83.9069718608 63.0855460465 -82.8138463378 63.0619333839 -81.7353521799</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.8"><gml:exterior><gml:LinearRing><gml:posList>65.7966168923 -85.4882706856 64.8792341763 -82.0286085892 63.8078066732 -80.1929826032 63.4502627275 -81.0188696472 63.6852073893 -82.4817602715 64.1915550448 -83.0137426929 63.9865028937 -84.0224570852 63.0496690999 -84.9114601432 63.6748721023 -85.6170141774 63.9297099218 -86.6932473038 64.4657194831 -85.953827438 65.7966168923 -85.4882706856</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.9"><gml:exterior><gml:LinearRing><gml:posList>68.7141818636 -75.4360674678 68.1627121698 -74.9659212501 67.6218668842 -75.0538144338 67.6862375168 -76.131513814 68.1822133526 -77.0343711557 68.8352871818 -77.0362883014 69.1464679276 -76.6274173833 68.7141818636 -75.4360674678</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.10"><gml:exterior><gml:LinearRing><gml:posList>70.1854686382 -99.030725263 69.4938726975 -95.696354009 68.9187187449 -97.4327640483 69.0148379175 -99.5908097378 70.1854686382 -99.030725263</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.11"><gml:exterior><gml:LinearRing><gml:posList>44.4965126518 -75.7914962913 43.9710496074 -77.0364127546 43.826516821 -79.0884250846 43.3293358104 -79.7747350469 43.2782003383 -79.0586315217 43.105730209 -79.0652311503 42.9457793529 -78.9269663609 42.6493546769 -81.359149961 42.037960385 -82.6227079766 42.5575330121 -82.6446981164 43.0174198005 -82.4163685665 43.3823213533 -81.7306374893 44.6065817577 -81.2801131927 45.2521222883 -81.607446102 44.4752953571 -80.108691149 44.8211732063 -79.7825356962 45.9268048675 -80.7526126769 46.3463741605 -84.1059436185 46.4576578772 -84.4910523084 47.9170007304 -84.8435381328 47.9849988438 -85.8592415533 48.7529948512 -86.4336931472 48.9896636093 -88.2711140284 47.9963331198 -89.5817526941 48.0364488477 -91.4279288888 48.5362632995 -92.6486037335 48.5165487406 -93.7567789853 48.9926695504 -95.1771057269 48.9926695504 -98.0346057254 48.9926178734 -102.4307456586 48.9926178734 -107.2666261907 48.9926178734 -110.5637117631 48.9925145204 -114.5202661233 48.9925145204 -119.3560433034 48.9925148664 -122.7530123738 48.9921467625 -122.9142665798 50.5455707193 -125.3627117424 50.4947689653 -126.229501784 51.2405177179 -127.7974031898 52.2160098052 -127.8607478972 53.3062197657 -128.8754777133 54.3486188532 -130.4582413842 55.2735049208 -129.9579158627 55.9079518201 -130.0196184876 56.5950478831 -131.5858911152 58.4038780411 -133.392240804 59.7924754522 -135.4827591939 58.9077232558 -137.4231058744 60.073388538 -139.1821457895 60.3210736778 -141.0011562046 62.6535928011 -141.0022930822 66.8811383701 -141.0042309407 69.6509463146 -141.0055639467 68.9069277616 -135.8684789906 70.1237317783 -132.9668660241 70.2373907813 -127.8350037865 69.4140078389 -125.9386287339 70.05145901 -125.0184121252 69.4541553661 -124.1685501437 69.8385683853 -122.9509985262 69.7668317314 -121.4278458572 69.0246035425 -118.0408829709 68.8751487897 -115.0349829077 68.3998069933 -113.9053849273 67.9343935829 -115.5338028965 67.6962344032 -113.8587540698 67.7219912391 -109.0745743839 66.5945498344 -107.5635473685 68.621568061 -108.2808325205 68.9069277616 -106.2721248388 68.0339215778 -104.4838761434 67.7232526321 -102.3058976289 67.7623139262 -98.6982384497 68.3676211213 -98.7180883844 68.3249447779 -97.0195879931 67.4747988564 -96.4595841559 68.5642763941 -93.5462133865 69.4529889579 -94.3221736716 69.9515319083 -96.3210157906 71.0147554146 -96.8774714643 72.0021425998 -94.5486954757 71.3571230699 -92.9906306993 70.02411526 -91.9429419007 69.481997505 -92.2248055849 68.8701032167 -90.4411515385 69.2335879172 -88.9419653416 68.8086611598 -88.0226131288 67.7759870071 -88.2486059691 67.3793805625 -86.5019425584 69.8469912363 -85.3201798604 69.6467145436 -82.6164445101 68.638739285 -81.2665096162 68.4848086212 -82.4700415169 67.4605166305 -81.2379451647 66.996039092 -81.527414566 66.1992047829 -83.6907853349 66.1628297099 -85.8566619811 65.3296979477 -87.3807674303 65.9468447571 -89.6879777147 65.3281924137 -89.0398657367 65.1419538069 -86.9382218577 64.1915143548 -87.9816788891 64.2502301748 -89.7863663884 63.6078555013 -90.2593888498 63.9109560871 -93.2171932104 63.3976504235 -90.7847387848 63.0689150718 -90.6436255041 62.793931348 -92.4539282377 61.457993849 -93.9849747882 59.9481061551 -94.8213598799 58.7238223019 -94.4368790867 58.7443708039 -93.1557511571 57.3544375367 -92.4235734229 56.9553896859 -92.704904804 57.2646344767 -91.0311173693 56.859930701 -88.879221211 56.0462099984 -87.5790096227 55.2451846078 -84.6131486216 55.1179059629 -82.2522680882 52.9675967188 -82.3011775937 51.3413760165 -80.3851212794 51.1955019926 -79.7471411365 52.3558210031 -78.4995825426 52.9802745664 -78.6829366083 54.1902990847 -79.3148411557 54.6527773821 -79.7656144429 55.0275332409 -78.3922420472 56.0729840874 -76.6586808494 57.2713076537 -76.5893449121 57.9614932003 -77.0793351458 58.6754824579 -78.5733130102 60.0049502301 -77.2363175671 60.8683535505 -78.1655167855 61.485337599 -77.5454809784 62.2973086274 -78.1548966678 62.5883242522 -77.4908341679 62.1575381195 -74.770334494 62.4821230803 -73.6707251193 61.1495221703 -71.3714494045 60.7998721045 -69.7352185854 60.3083858842 -69.7749638701 59.4926353628 -69.5824833445 58.550556111 -68.4826207125 58.1842584368 -67.0296484669 58.6071549862 -65.7514984036 60.4538003248 -64.0716187263 58.5925255409 -62.5866674811 56.9274622868 -61.1787704267 55.8003838507 -60.5208600316 54.855454791 -58.9197892528 54.5667585021 -57.3531795522 53.9419619524 -58.8531388618 54.0800505728 -57.4107462742 53.4678439134 -56.6394587583 53.3300641667 -55.8120825475 52.1116803782 -55.6848039015 51.4252383205 -56.9665421822 51.3165957425 -58.4536841098 50.3280703529 -59.8272599555 50.2301699619 -61.8631893171 50.2642275789 -66.4130753842 49.3392194413 -67.3763322201 48.7562116295 -69.0695695238 48.4992360313 -69.0615937539 49.1267269608 -66.7442315486 49.4123602068 -65.9487443479 49.5050651765 -65.2744479584 49.2473784876 -64.5574735197 48.7835369535 -64.2713299764 48.3579288342 -64.5191238422 47.9302464096 -65.1055472033 47.0228823799 -65.2360704536 46.2394066173 -64.5140275326 45.6144879975 -62.4704484309 45.915461607 -61.9167399804 45.3331566176 -60.9986466741 44.5781348121 -63.1706821992 44.5659853938 -64.3055314385 43.4709333101 -65.4748429623 43.702297243 -66.0281469983 44.5796572688 -65.9987687433 45.3165674644 -64.3446261883 45.6277936615 -64.7562557225 45.2102725026 -65.9094132743 45.1786562916 -67.1760148371 45.604585751 -67.4409387966 45.7624138603 -67.8108158023 47.035631383 -67.805184752 47.4398444359 -69.2676277858 46.7040753931 -70.0077634866 45.7315249384 -70.4073514996 45.0150542586 -71.5148352757 44.9992536226 -74.7129614499 44.4965126518 -75.7914962913</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.12"><gml:exterior><gml:LinearRing><gml:posList>73.3080508002 -114.1966446782 72.9346430434 -110.57657773 73.3039662458 -106.8336968231 73.7106386903 -106.5995987825 73.1314786228 -104.86895783 72.2076038176 -104.6016724534 70.5995140222 -103.5377091463 69.9017601162 -102.1794327807 69.0062523055 -101.8112687208 68.7902285428 -103.3530981475 69.1880556902 -106.4120987961 68.5484072541 -110.9800919593 68.591538764 -113.3504939761 69.2271652184 -114.3925709949 69.3001976171 -115.9748429322 69.9894066011 -117.4420467056 70.3157412356 -114.1847224874 70.2699241787 -111.5486547809 70.681830102 -113.4907120698 70.6063499591 -117.5854386308 71.0222028235 -118.3864640191 71.3766543199 -116.1034236552 71.6267763894 -119.0508927277 71.9914140126 -119.1307694617 72.6620514015 -116.7897335844 73.3080508002 -114.1966446782</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.13"><gml:exterior><gml:LinearRing><gml:posList>73.8466282367 -81.0629108826 73.8616129847 -78.3141473641 73.0530851504 -76.6519538394 73.1719900091 -78.0690929199 72.864305097 -80.1034832709 73.8466282367 -81.0629108826</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.14"><gml:exterior><gml:LinearRing><gml:posList>73.85529201 -86.5584204104 73.6676699404 -84.9519750991 72.8161074422 -86.6985571313 72.0314394747 -85.9874975637 73.309100602 -84.3606880345 73.7162132343 -81.5283911172 72.1807314664 -80.4828588946 72.7558047083 -77.6010229581 72.4976259978 -75.1983536892 71.825782576 -73.6397204676 71.6472842024 -72.4726049944 70.5842959232 -68.3622534352 69.7286644135 -67.1154679559 69.1845563413 -66.6687719926 68.1926204813 -65.9141736285 67.6178245723 -64.0762833881 66.8782011545 -61.8921142619 65.6640078434 -62.5941870359 65.2417942938 -64.0446058648 65.9822451471 -65.3931372306 66.5427106415 -66.8615617087 65.6452090154 -67.273142112 64.1400549773 -64.9028255375 62.9110781579 -64.6277563722 63.0075137048 -66.4184057855 63.3292600115 -68.1697598947 61.7419432723 -64.5364072009 61.5183741269 -65.3410721413 62.411763224 -68.9827907939 63.4839477052 -71.835183043 64.2157582019 -73.2014276296 64.2673229561 -74.8117179211 64.2005753129 -76.0792849983 64.4427757476 -78.0181779166 65.2330888703 -78.4823712515 65.4596214496 -77.4356583841 65.2541364238 -75.2154435421 65.4021028274 -73.9614237553 66.1485862929 -74.4721574088 67.1194521778 -72.3509008979 68.2359072545 -72.9946183446 69.2870547141 -75.6874487461 70.3773684608 -79.2259985703 70.1228701419 -81.7624406038 69.8248965103 -82.1435441201 70.1134707279 -86.2697241305 70.4650332274 -88.6940812246 71.9163271709 -90.1046850616 73.2516950063 -89.0512182616 73.85529201 -86.5584204104</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.15"><gml:exterior><gml:LinearRing><gml:posList>73.8969179869 -99.8309539723 73.8612327659 -97.2603566442 73.1570546807 -97.5092116021 72.7520205283 -96.6161102283 71.9131865031 -96.5620426078 71.2716738511 -98.7178761823 72.3944743939 -101.8611818283 72.9636904499 -100.0497039755 73.4913597198 -101.6256317076 73.8969179869 -99.8309539723</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.16"><gml:exterior><gml:LinearRing><gml:posList>74.0851097184 -92.7470597284 73.9022483909 -90.1957088188 72.8469189204 -90.8879356514 72.4767296357 -92.758727455 71.6155209577 -93.1287897391 72.2021575326 -94.626793081 74.0099144056 -95.248646641 74.0851097184 -92.7470597284</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.17"><gml:exterior><gml:LinearRing><gml:posList>74.5558535323 -121.5807999402 74.0875511244 -116.8670141389 73.4806582218 -115.3205460437 72.2418480033 -120.2612198737 71.4914411037 -120.6352433132 71.0846214424 -123.0999243032 71.9491640849 -125.7904353666 73.7656110212 -123.7737524168 74.3485781418 -124.7556860071 74.5558535323 -121.5807999402</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.18"><gml:exterior><gml:LinearRing><gml:posList>75.6042340635 -94.393910307 74.7065302908 -93.0969134471 74.6297651701 -94.3868334137 75.1391269703 -96.1769307388 76.1440117963 -97.0387644419 75.6042340635 -94.393910307</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.19"><gml:exterior><gml:LinearRing><gml:posList>76.6676699322 -98.4080297641 75.6798362763 -97.3775121923 75.0262718251 -98.0350235879 75.0140647941 -100.3301896013 75.4981142718 -102.6334122542 76.4588076279 -103.2745605378 76.8137098827 -101.3409328228 76.6676699322 -98.4080297641</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.20"><gml:exterior><gml:LinearRing><gml:posList>76.8248558037 -108.6673884784 75.6596539853 -105.3904923292 75.056626643 -106.0089005349 75.071519221 -108.8242081497 74.3959414227 -113.0237931111 74.509951478 -114.4281922967 75.2011172026 -117.5100805385 75.7511890102 -118.0391242539 76.4190512144 -115.0016400844 76.2732607535 -112.9942113945 75.5218772927 -111.2508438874 75.8041122594 -109.9318349624 76.3005231234 -110.3836156934 76.8248558037 -108.6673884784</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.21"><gml:exterior><gml:LinearRing><gml:posList>77.013657887 -95.2387589308 76.6936709091 -91.4410701339 75.8037272884 -88.4077364183 75.8266054818 -83.7362361603 75.4697939589 -79.6498103839 74.8762881015 -79.3951717141 74.4584007325 -81.7886857108 74.5327822433 -89.9486385048 74.9535452675 -91.9195733294 75.8436574926 -92.9864355511 76.5023996016 -95.7215758664 77.013657887 -95.2387589308</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.22"><gml:exterior><gml:LinearRing><gml:posList>77.4723981193 -116.1255997298 76.6984316511 -115.8928116501 75.8591168747 -119.8629451157 75.9328066535 -122.4258927041 77.28880437 -119.2897029185 77.4723981193 -116.1255997298</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.23"><gml:exterior><gml:LinearRing><gml:posList>78.073919927 -109.6036677648 77.4846840335 -109.142384057 77.3257509849 -112.1092423126 77.9049746156 -113.2412816948 78.073919927 -109.6036677648</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.24"><gml:exterior><gml:LinearRing><gml:posList>78.6366425017 -96.2252003219 77.557016316 -94.2526260994 77.852291952 -97.7527345485 78.6861283721 -98.7897621418 78.6366425017 -96.2252003219</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.25"><gml:exterior><gml:LinearRing><gml:posList>79.2631571086 -111.3647008882 78.547517753 -109.8986876154 78.2554759361 -112.9543556139 79.2631571086 -111.3647008882</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.26"><gml:exterior><gml:LinearRing><gml:posList>79.3412945961 -103.6569717946 78.5987001977 -99.5579320921 77.8937848367 -99.0172419944 78.2428245482 -103.9465225858 79.3412945961 -103.6569717946</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.27"><gml:exterior><gml:LinearRing><gml:posList>82.1202372037 -94.7475303726 79.8437564593 -87.8942830521 78.1644960986 -89.4551895926 78.2128766324 -92.0043839257 79.8849957641 -96.6161189317 81.5599890546 -97.2296384977 82.1202372037 -94.7475303726</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.28"><gml:exterior><gml:LinearRing><gml:posList>83.1165224297 -69.6875300582 82.9097760132 -64.6966853445 82.1972109168 -61.3124080157 81.3908144389 -64.6921280714 79.53880436 -73.1738582197 79.4078083091 -75.170803849 78.3754742877 -75.1076961163 77.5478868901 -78.1569486444 76.6642926532 -77.7707820596 76.8180292695 -84.6612690294 76.3204113462 -85.4162920179 76.410467785 -88.9106339395 77.1739652645 -89.9658817652 77.2670351641 -86.9129125826 78.1271832727 -87.4878637487 78.8092714867 -86.6098526767 79.3677203763 -85.9559489766 80.5289123212 -87.7429096027 81.7218490609 -90.8138278667 82.381455706 -82.7115779012 82.7912457406 -81.5761612285 83.1165224297 -69.6875300582</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.203.29"><gml:exterior><gml:LinearRing><gml:posList>50.4355984753 -127.8094230492 50.6815898071 -127.3951243651 50.1533643736 -125.9789097509 50.0054242048 -124.979035702 49.0893168626 -123.5126939215 48.3696402954 -123.0688925834 48.4667569292 -124.2476657697 48.9391293918 -125.4606906264 49.7177842145 -126.7321341943 50.4355984753 -127.8094230492</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CA</ogr:ISO2>
      <ogr:ISO3>CAN</ogr:ISO3>
      <ogr:NAMEen>Canada</ogr:NAMEen>
      <ogr:GBD_ID>101</ogr:GBD_ID>
      <ogr:GBD_NAME>Canada</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>1. Developed region: G7</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>1. High income: OECD</ogr:WB_GROUP>
      <ogr:CONTINENT>North America</ogr:CONTINENT>
      <ogr:WB_REGION>North America</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.204">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>22.1197769759 113.8940024942</gml:lowerCorner><gml:upperCorner>22.555812893 114.4435103707</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.204"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.204.0"><gml:exterior><gml:LinearRing><gml:posList>22.529364325 114.082367384 22.555812893 114.229828321 22.4145729779 114.4435103707 22.2012808964 114.3669456223 22.1197769759 114.1122597996 22.2035183029 113.8940024942 22.3685316062 113.9182854933 22.529364325 114.082367384</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>HK</ogr:ISO2>
      <ogr:ISO3>HKG</ogr:ISO3>
      <ogr:NAMEen>Hong Kong</ogr:NAMEen>
      <ogr:GBD_ID>HKG</ogr:GBD_ID>
      <ogr:GBD_NAME>Hong Kong</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.205">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>-22.5429591083 163.5240971628</gml:lowerCorner><gml:upperCorner>-20.0348134713 167.1982054025</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.205"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.205.0"><gml:exterior><gml:LinearRing><gml:posList>-20.0348134713 163.5240971628 -20.0465019718 164.0626980791 -20.7605165583 165.3329029057 -21.2885639979 165.7242265464 -22.1771310839 167.1982054025 -22.5429591083 167.0274739618 -21.4001874788 164.7832659829 -20.0348134713 163.5240971628</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>NC</ogr:ISO2>
      <ogr:ISO3>NCL</ogr:ISO3>
      <ogr:NAMEen>New Caledonia</ogr:NAMEen>
      <ogr:GBD_ID>NCL</ogr:GBD_ID>
      <ogr:GBD_NAME>New Caledonia</ogr:GBD_NAME>
      <ogr:WB_ECONOMY xsi:nil="true"/>
      <ogr:WB_GROUP xsi:nil="true"/>
      <ogr:CONTINENT xsi:nil="true"/>
      <ogr:WB_REGION xsi:nil="true"/>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.206">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>21.9988542662 113.5772768352</gml:lowerCorner><gml:upperCorner>22.173592026 113.7277723573</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.206"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.206.0"><gml:exterior><gml:LinearRing><gml:posList>21.9988542662 113.5772768352 22.173592026 113.6359924872 22.0517442071 113.7277723573 21.9988542662 113.5772768352</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>MO</ogr:ISO2>
      <ogr:ISO3>MAC</ogr:ISO3>
      <ogr:NAMEen>Macau</ogr:NAMEen>
      <ogr:GBD_ID>MAC</ogr:GBD_ID>
      <ogr:GBD_NAME>Macau</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>6. Developing region</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>2. High income: nonOECD</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
  <ogr:featureMember>
    <ogr:countries gml:id="countries.207">
      <gml:boundedBy><gml:Envelope srsName="urn:ogc:def:crs:EPSG::4326"><gml:lowerCorner>18.1084737976 73.632642049</gml:lowerCorner><gml:upperCorner>53.7331411849 134.772579387</gml:upperCorner></gml:Envelope></gml:boundedBy>
      <ogr:geometryProperty><gml:MultiSurface srsName="urn:ogc:def:crs:EPSG::4326" gml:id="countries.geom.207"><gml:surfaceMember><gml:Polygon gml:id="countries.geom.207.0"><gml:exterior><gml:LinearRing><gml:posList>22.173592026 113.6359924872 21.9988542662 113.5772768352 21.7888173936 112.9801503679 21.4985833368 111.8118584713 21.1724120316 110.5524725459 20.478257554 110.5341903 20.248480536 109.914561394 21.474025783 109.741465691 21.698553778 108.559825066 21.485663153 107.99122155 21.995506083 106.648197877 22.744349263 106.74664148 22.904649556 105.853879435 23.365810039 105.312155396 22.8292514013 103.9727918576 22.765174866 102.44271814 22.397548726 102.118655233 22.446124573 101.654549195 21.134473369 101.7179045 21.552690735 101.159023885 21.427892151 100.187145223 22.045528869 99.94240564 22.153532613 99.144469849 23.057379252 99.493079061 23.179387309 98.859009237 24.075715027 98.585227499 23.863557435 97.637068319 24.745028178 97.53609257 25.878989971 98.692404419 27.577335918 98.679278605 28.538465881 97.546221151 28.217477723 97.323495728 29.368466899 96.14196578 29.052672221 95.281553182 29.319451803 94.630430135 28.67189443 93.446213013 27.915764873 92.628485148 27.759443665 91.632887004 28.3583989308 90.2255906943 28.134640401 89.561488891 27.315543111 88.89233077 28.10583079 88.610487508 27.860884501 88.118217814 27.885172424 85.980260458 28.318789368 85.080212036 29.247001445 84.099134969 29.191707662 83.517051636 30.330087789 82.088766724 30.1969693 80.996016887 30.938371074 79.577808879 32.2487005277 78.7887122705 32.6454245 79.476419719 33.552785136 78.781371704 34.372295837 78.922138306 34.658867493 78.273185669 35.495405579 77.800346314 35.424740888 77.2516900425 35.646111714 76.777350741 35.806239319 76.166027466 36.462633362 75.976581665 37.021669006 74.542353963 37.231113587 74.892306763 38.510673727 74.776344849 38.602838644 73.797386515 39.44834259 73.632642049 40.511636861 74.835359335 40.291701966 75.681819296 40.944685364 76.766817668 41.039511617 78.074954875 41.377527161 78.359898723 42.189518941 80.21032841 43.128040467 80.787864217 45.018958639 80.061706991 45.533190817 82.291493368 47.211537985 83.150355672 46.818073629 84.767104533 47.051831971 85.498636109 48.40758901 85.783683309 49.085273743 87.323796021 49.165837301 87.816324097 48.599489441 87.942828004 47.992988994 89.045551392 47.658642477 90.344800252 46.566409404 91.047496379 45.186183574 90.873243449 44.951262513 93.525277955 44.287117005 95.379428345 42.740906474 96.350635214 42.564198914 99.474475545 42.5154422 101.637599325 42.184609681 102.034164266 41.596144308 105.014809204 42.286618958 106.767828818 42.449296367 109.485130656 42.768605041 110.40672815 43.696636251 111.933353313 44.346596578 111.396435181 45.087481588 112.011488079 44.746262105 113.635058228 45.385499573 114.533711385 45.44435903 115.637830038 46.309319153 116.603559205 46.715392558 118.238291464 46.591627503 119.680115601 47.159525655 119.699959351 47.96624644 118.542252239 47.9775440208 117.7416300051 47.776900575 117.5094003564 47.705564678 115.852700643 48.12210256 115.514530071 49.823264873 116.684277792 49.512741191 117.758837525 50.092654114 119.316210165 50.37661611 119.141233765 52.117595114 120.779169963 52.760656637 120.03296228 53.280159811 120.874254598 53.6845503734 122.581510365 53.7331411849 124.5287569115 53.1685824973 125.9497198547 52.152657572 126.531170288 51.063783468 126.931765992 49.54157664 128.092470338 49.41078359 129.502410116 48.333769226 130.822122843 47.682284445 131.023350871 47.707528381 132.524654582 48.381337383 134.386349732 47.710732321 134.772579387 47.257891744 134.154115845 46.258986308 133.90245162 45.5403816751 133.2575604354 45.2293538981 131.9919443364 44.841708476 130.933433879 43.380221456 131.28090621 42.530480042 130.530771119 42.961900736 129.851742798 42.442371724 129.703328085 41.993742778 128.034592733 41.4335661479 127.8440018872 41.66593333 126.617883342 40.909131979 126.015336142 40.098293361 124.36996504 39.5740093937 122.8631974362 38.8697883979 121.8551405562 38.727606512 121.153086785 39.371486721 121.779795769 39.528631903 121.223399285 40.502346096 122.301849806 40.922308661 121.175791863 40.194484768 120.436045769 39.890825588 119.535592388 39.189764716 119.015635613 39.114447333 117.757334832 38.62132396 117.543549024 38.138739325 118.08480879 38.15257396 118.840098504 37.280666408 118.972422722 37.120550848 119.441172722 37.833970445 120.738047722 37.503542026 121.4265765028 37.5400903409 122.1954601953 37.409816799 122.688243035 36.589504299 121.032481316 35.583929755 119.647959832 34.718207098 119.192230665 34.311835028 120.257090691 32.7891320068 121.5088334231 31.3883430972 121.9373988943 31.1979861531 120.3633424751 30.914923436 121.977382923 30.271780919 120.684941141 30.325063579 121.377384147 29.823175763 121.98303901 29.277777411 121.525889519 28.5441094814 121.626522728 28.1271249629 121.1771051866 26.927232164 120.237396681 26.5186716905 119.8443714354 25.461371161 119.628021681 25.2195132691 119.2241029997 24.568426825 118.646576368 23.5674845525 117.1185500843 22.939352413 116.494687913 22.815863348 114.784759962 22.555812893 114.229828321 22.529364325 114.082367384 22.889715887 113.35043379 22.237882255 113.587654701 22.173592026 113.6359924872</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember><gml:surfaceMember><gml:Polygon gml:id="countries.geom.207.1"><gml:exterior><gml:LinearRing><gml:posList>20.153306382 110.685069207 19.9315364706 111.2416479439 19.3306129229 110.7586603057 18.6869924383 110.6227926064 18.1084737976 109.6560017213 18.504828192 108.694997592 19.274969794 108.626963738 19.7901186756 109.3709232087 19.9946305723 110.0563213554 20.153306382 110.685069207</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></gml:surfaceMember></gml:MultiSurface></ogr:geometryProperty>
      <ogr:ISO2>CN</ogr:ISO2>
      <ogr:ISO3>CHN</ogr:ISO3>
      <ogr:NAMEen>China</ogr:NAMEen>
      <ogr:GBD_ID>6</ogr:GBD_ID>
      <ogr:GBD_NAME>China</ogr:GBD_NAME>
      <ogr:WB_ECONOMY>3. Emerging region: BRIC</ogr:WB_ECONOMY>
      <ogr:WB_GROUP>3. Upper middle income</ogr:WB_GROUP>
      <ogr:CONTINENT>Asia</ogr:CONTINENT>
      <ogr:WB_REGION>East Asia &amp; Pacific</ogr:WB_REGION>
    </ogr:countries>
  </ogr:featureMember>
</ogr:FeatureCollection>
