\documentclass{beamer}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}

\usepackage{color}
\usepackage{graphicx}
\usepackage{amsmath}
%\usepackage{mathabx}
\usepackage[framemethod=tikz]{mdframed}

%%% Package pour faire des tableaux à plusieurs lignes combinées
\usepackage{multirow}

\usepackage[french]{babel}
\frenchsetup{StandardLists=true}

%%% Package pour aligner les colonnes dans un tableau
\usepackage{tabularx}
\usepackage{array}

\usepackage{eurosym}
\usetheme{simple}
\graphicspath{{IMAGE/}}

\author{H. Commenges}
\title{Analyse de données L3}
\date{}


\begin{document}


% FRAME Intro
\begin{frame}

\includegraphics[width=12cm]{Logos.pdf}

\vfill
 
\begin{center}

\vspace*{1.5cm}

\LARGE
\textbf{Analyse de données \\ Représentations cartographiques}

\Large
\medskip
\textit{Global Burden of Disease}

\vspace*{2.5cm}

\large

\textbf{Hadrien Commenges}

{\small

\vspace*{0.1cm}

\url{hadrien.commenges@univ-paris1.fr}}

\end{center}

\end{frame}


\section{Présentation du module}

% FRAME
\begin{frame}{Objectifs d'apprentissage}

\textbf{Objectifs principaux:}

\begin{itemize}
  \item Savoir explorer et extraire des données d'une base de données en ligne
  \item Savoir préparer les données en vue de les analyser et de les représenter
  \item Connaître les principales règles de représentation cartographique
  \item Connaître les principales méthodes d'exploration et d'analyse univariée et bivariée
  \item Découvrir les méthodes d'exploration et d'analyse multivariée
\end{itemize}

\bigskip

\textbf{Objectif subsidiaire:}

\begin{itemize}
  \item Découvrir la base \textit{Global Burden of Disease}
  \item Se faire une culture de base en géographie de la santé permettant de guider l'exploration et l'analyse des données
\end{itemize}

\end{frame}


\section{Géographie de la santé}

% FRAME
\begin{frame}{Théories de la transition}

\textbf{Transition démographique} (voir la vidéo sur le site de l'Ined)

\begin{figure}
  \includegraphics[width=12cm]{transitiondemo.png}
\end{figure}

\footnotesize
\textit{Source: INED, La population mondiale} \\ \url{https://www.ined.fr/fr/tout-savoir-population}
\normalsize

\end{frame}


% FRAME
\begin{frame}{Théories de la transition}

\begin{figure}
  \includegraphics[width=11cm]{transitiondemo_map.png}
\end{figure}

\footnotesize
\textit{Source: Rekacewicz (2010) La bombe démographique}
\normalsize

\end{frame}


% FRAME
\begin{frame}{Théories de la transition}

\textbf{Transition épidémiologique} (évolution des causes de mortalité):

\begin{itemize}
  \item société pré-agricole $\rightarrow$ violence humaine et animale
  \item société agricole $\rightarrow$ maladies infectieuses
  \item société industrielle $\rightarrow$ maladies cardio-vasculaires
  \item société quaternaire $\rightarrow$ cancer
  \item futur $\rightarrow$ vieillissement
\end{itemize}

\bigskip

\footnotesize
\textit{Source: Horiuchi (1999) Epidemiological transition in human history}
\normalsize

\end{frame}


% FRAME
\begin{frame}{Théories de la transition}

\textbf{Transition épidémiologique}

Fardeau relatif de l'incapacité liée aux maladies non transmissibles. 

\begin{itemize}
  \item Les pays en bleu ont achevé leur transition: poids majeur des maladies non transmissibles (cancer, maladie cardiovasculaires, etc.)
  \item Les pays en rouge sont les moins avancés dans la transition: poids majeur des maladies infectieuses (malaria, VIH, etc.)
\end{itemize}

\begin{figure}
  \includegraphics[width=12cm]{dalyyld.png}
\end{figure}

\footnotesize
\textit{Source: GBD 2019 Diseases and Injuries Collaborators (2020)}
\normalsize

\end{frame}
  

% FRAME
\begin{frame}{Théories de la transition}

\textbf{Étapes de la transition épidemiologique}

\begin{figure}
  \includegraphics[width=12cm]{transitionepi.png}
\end{figure}

\footnotesize
CVD (maladies cardiovasculaires), IHD (maladie coronarienne), RHD (valvulopathie cardiaque), stroke (accident vasculaire cérébral) \\
\textit{Source: Jamison et al. (2006) Disease control priorities in developing countries}
\normalsize

\end{frame}



% FRAME
\begin{frame}{Théories de la transition}

\begin{block}{Théories de la transition}
Théories qui expliquent la variabilité des états observés comme une coexistence, à un moment donné, de différents stades ou étapes sur une même trajectoire. Trajectoire de la diminution de la natalité et de la mortalité (tr.démographique), trajectoire de la transformation de l'économie rurale en économie industrielle puis tertiaire (tr.économique), trajectoire de l'urbanisation progressive des sociétés (tr.urbaine).
\end{block}

Ces théories sont discutables et discutées:

\small
\begin{itemize}
  \item Présupposés idéologiques et normatifs: qui décide du sens de la flèche, qui assigne les étiquettes (avancé, moins avancé, sous-développé)?
  \item Faiblesses théoriques: quels mécanismes concrets font que tous les pays suivent la même trajectoire pour arriver au même état final? 
  \item Faiblesses empiriques: que faire des entorses à la trajectoire? Pays qui passe d'une économie rurale à tertiaire sans passer par la case industrielle, pays dont le taux de mortalité augmente de façon intempestive (SIDA). 
\end{itemize}

\footnotesize
Voir le cours de C. Grasland: \\ \url{http://grasland.script.univ-paris-diderot.fr/GO312/cours_b/cours_b2/cours_b2.html}
\normalsize
 
\end{frame}
  

% FRAME
\begin{frame}{Indicateurs de santé}

\textbf{Taux et proportions:}

\small
\begin{itemize}
  \item \textbf{Taux}: mise en rapport de deux quantités par une division. Le résultat se lit dans l'unité de mesure issue de la division: km/h, hab/km2, etc.
  \item Numérateur et dénominateur peuvent être complètement indépendants: nbr. de morts du Covid divisé par le nombre de chômeurs
  \item Numérateur et dénominateur peuvent être partiellement dépendants: nombre de naissances divisé par la population totale
  \item Numérateur et dénominateur peuvent être très dépendants: nombre de naissances divisé par le nombre de femmes en âge de procréer
  \item \textbf{Proportion}: taux dont le numérateur est inclu dans le dénominateur. Le résultat n'a pas d'unité de mesure
\end{itemize}

Les morts du Covid ont été rapportées à des choses variées: nombre de chômeurs, perte en PIB, etc. 
\url{https://www.arteradio.com/son/61664821/vous_prendrez_le_covid_ou_le_chomage}

\normalsize

\end{frame}

% FRAME
\begin{frame}{Indicateurs de santé}

\textbf{Le problème du dénominateur:} ex. du taux de létalité du Covid-19

Aux État-Unis en 2020, CFR (\textit{case fatality rate}) est de l'ordre de 3 alors que IFR (\textit{infection fatality rate}) est de l'ordre de 0,3. 

\begin{itemize}
  \item Dans les deux cas, le numérateur est le nombre de morts du Covid-19
  \item CFR: le dénominateur est le nombre de cas enregistrés
  \item IFR: le dénominateur est le nombre estimé de personnes infectées 
\end{itemize}

\begin{figure}
  \includegraphics[width=9.5cm]{CFR.pdf}
\end{figure}

\normalsize

\end{frame}


% FRAME
\begin{frame}{Indicateurs de santé}

\textbf{Morbidité et mortalité}

\begin{itemize}
  \item Population générale > Pop. à risque > Pop. morbide
  \item La population morbide est quantifiée par enregistrement (CFR) ou par estimation (IFR)
\end{itemize}

\begin{figure}
  \includegraphics[width=8.5cm]{incidenceprevalence.png}
\end{figure}

\footnotesize
\textit{Source: Noordzij et al. (2010) Measures of disease frequency}
\normalsize

\end{frame}


% FRAME
\begin{frame}{Indicateurs de santé}

\textbf{Indicateurs de vie en bonne santé (DALYs, YLLs, YLDs)}
\bigskip
\small

Le mortalité ne donne pas une image complète du fardeau des maladies sur une population. Ce fardeau est mesuré par les DALYs (\textit{disability-adjusted life year}). \\ Les DALYs sont la somme des:

\begin{itemize}
  \item \textbf{YLLs}: \textit{years of life lost due to premature mortality} (années de vie perdues prématurément) 
  \item \textbf{YLDs}: \textit{years of life lost due disability} (années de vie perdues dues à l'incapacité) 
\end{itemize}

\bigskip

Une DALY équivaut à une année de vie en pleine santé perdue. 

\footnotesize
\textit{Source: \url{https://www.who.int/data/gho/indicator-metadata-registry}}
\normalsize

\end{frame}



% FRAME
\begin{frame}{Indicateurs de santé}

\begin{figure}
  \includegraphics[width=12cm]{daly.png}
\end{figure}

\small
Les DALYs rendent le fardeau \textbf{commensurable entre maladies et entre individus}:

\begin{itemize}
  \item Point de vue individuel: vivre 20 ans de sa vie en étant grand brûlé (poids d'incapacité de 0,5) ou mourir prématurément du Covid-19 10 ans avant l'âge espéré.
  \item Point de vue populationnel: lutter contre une cause mortelle avec une prévalence faible (mort subite du nourrisson), contre une cause invalidante avec une prévalence moyenne (diabète), contre une cause faiblement invalidante avec une prévalence forte (lombalgie).
\end{itemize}

\normalsize

\end{frame}
 


\section{Évaluation économique et aide au développement}

% FRAME
\begin{frame}{Évaluation économique et aide au développement}

\footnotesize
\og It seems clear that the great reduction of mortality in underdeveloped areas since 1940 has been brought about mainly by the discovery of new methods of disease treatment applicable at reasonable cost \fg{} (Davis 1956)
\normalsize

\begin{figure}
  \includegraphics[width=8.5cm]{DAH.png}
\end{figure}

\footnotesize
HIV/AIDS (VIH/SIDA), TB (Tuberculosis), MNCH (Maternal, Newborn, and Child Health), NCD (Non Comunicable Disease) \\
\textit{Source: Dieleman et al. (2014) Global health development assistance remained steady}
\normalsize

\end{frame}


% FRAME
\begin{frame}{Évaluation économique et aide au développement}

\begin{figure}
  \includegraphics[width=11.5cm]{DAHDALY.png}
\end{figure}

\footnotesize
\textit{Source: Hanlon et al. (2014) Regional variation in the allocation of development assistance for health}
\normalsize

\end{frame}

% FRAME
\begin{frame}{Évaluation économique et aide au développement}

\begin{figure}
  \includegraphics[width=12cm]{DAHDALY_map.png}
\end{figure}

\footnotesize
\textit{Source: Hanlon et al. (2014)}
\normalsize

\end{frame}


\section{Exploration de la base GBD}

% FRAME
\begin{frame}{Exploration de la base GBD}

\textbf{Objectif: explorer la base avec GBD-compare pour essayer de retrouver les grands traits de la transition épidémiologique}

\small
\bigskip

\begin{itemize}
  \item \textbf{Outil d'exploration:} GBD-compare \\ \url{https://vizhub.healthdata.org/gbd-compare/}
  \item Dans l'onglet \texttt{Single} (en haut), tester les différents onglets (à gauche), en particulier \texttt{Map}, \texttt{Treemap}, \texttt{Pyramid} et \texttt{Patterns}
  \item Dans l'onglet \texttt{Explore} confronter \texttt{Map} avec \texttt{Treemap}: sélectionner un pays pour voir la composition, sélectionner une cause pour voir sa répartition
  \item Faire varier la mesure: \texttt{Deaths} et \texttt{DALYs}
  \item Faire varier l'unité: nombre (\texttt{\#}), taux (\texttt{rate}), pourcentage (\texttt{\%})
\end{itemize}

\normalsize

\end{frame}


% FRAME
\begin{frame}{Extraction et analyse des données GBD}

\textbf{Objectif: extraire des données, les analyser et les cartographier}

\small
\bigskip

\begin{itemize}
  \item \textbf{Outil d'extraction:} GBD-results \\ \url{https://vizhub.healthdata.org/gbd-results/}
  \item Créer un compte utilisateur sur la page de GBD-results
  \item Sélectionner les morts (\texttt{Deaths}), le nombre (\texttt{Number}), pour tous les pays (\textit{all countries and territories}), pour toute cause et pour le cancer de la prostate (voir diapo suivante)
  \item Télécharger le tableau
  \item Ouvrir le tableau (csv) dans un bloc-notes (pour voir), puis dans un tableur
  \item Faire un tableau croisé dynamique en mettant les pays (\texttt{location\_name}) en ligne, la cause en colonne (\texttt{cause\_id}), la valeur en valeur (\texttt{val})
  \item Copier le tableau obtenu et le coller dans le fichier fourni (\texttt{GBD.xlsx}) dans la feuille \textbf{F2}. Combiner avec la population 2019 (feuille \textbf{F1})
\end{itemize}

\normalsize

\end{frame}


% FRAME
\begin{frame}{Extraction et analyse des données GBD}

\textbf{Objectif: extraire des données, les analyser et les cartographier}

\begin{figure}
  \includegraphics[width=5cm]{prostatecancer.png}
\end{figure}

\end{frame}


% FRAME
\begin{frame}{Extraction et analyse des données GBD}

\textbf{Objectif 1: calculer et représenter des indicateurs simples} \\
\textbf{Objectif 2:} mettre en évidence les effets de la chlordécone

\small
\bigskip

\begin{itemize}
  \item Calculer le taux de décès dû au cancer de la prostate: rapport entre le nombre de morts de ce cancer et la population
  \item Calculer le pourcentage de décès dûs au cancer de la prostate: rapport entre le nombre de morts de ce cancer et le nombre de morts total
  \item Cartographier sur Magrit les trois variables en univarié: nombre, taux, pourcentage
  \item Détecter puis éliminer les valeurs aberrantes, puis refaire la carte
  \item Cartographier les variables en bivarié: nombre et taux, nombre et pourcentage
\end{itemize}

\medskip

\textbf{Référence:} Page Wikipédia \texttt{Chlordécone aux Antilles françaises} \\
\textbf{Question:} comment sont représentées les Antilles françaises sur la carte?

\normalsize

\end{frame}


% FRAME
\begin{frame}{Analyse bivariée quali-quali}

\textbf{Objectif 1: mettre en place une analyse quali-quali} \\
\textbf{Objectif 2:} mettre en évidence la transition épidémiologique

\small
\bigskip

\begin{itemize}
  \item Se renseigner sur la construction du SDI (\textit{socio-demographic index}) et le comparer à l'IDH (HDI - \textit{human development index})
  \item Considérer les données de la feuille \textbf{F3} (extraction du nombre de morts selon catégorie de pays et selon grandes causes de décès)
  \item Faire un tableau croisé dynamique (\textit{location} en ligne, \textit{cause} en colonne)
  \item Calculer les pourcentages en ligne et en colonne
  \item Calculer les effectifs espérés sous hypothèse d'indépendance
  \item Calculer les résidus bruts puis les résidus standardisés
  \item Calculer le $\chi^2$ puis le V de Cramer
\end{itemize}

\medskip

\textbf{Question:} les résultats confirment-ils les grandes lignes de la transition épidémiologiques?
 
\normalsize

\end{frame}


% FRAME
\begin{frame}{Analyse bivariée quanti-quanti}

\textbf{Objectif 1: mettre en place une analyse quanti-quanti} \\
\textbf{Objectif 2:} caractériser les pays vis-à-vis de la cirrhose

\small
\bigskip

\begin{itemize}
  \item Se renseigner sur la cirrhose (voir le cour d'Arnaud Fontanet sur l'hépatite)
  \item Extraire les morts (\texttt{Deaths}), le nombre (\texttt{Number}), pour tous les pays (\textit{all countries and territories}), pour toutes les catégories de cirrhoses
  \item Télécharger le tableau, l'ouvrir puis dans le coller en feuille \textbf{F4}
  \item Mettre en rapport le nombre total de morts de la cirrhose (x) avec le nombre de morts de cirrhose due à l'alcool (y)
  \item Mettre en rapport le nombre total de morts de la cirrhose (x) avec le nombre de morts de cirrhose due à l'hépatite C (y)
  \item Pour ces deux mises en relation, mettre en place toute l'analyse: nuage de points, droite de régression, calcul du y estimé, calcul des résidus, cartographie des résidus
\end{itemize}

\normalsize

\end{frame}



% FRAME
\begin{frame}{Analyse bivariée quanti-quanti}

\textbf{Objectif 1: mettre en place une analyse quanti-quanti} \\
\textbf{Objectif 2:} quantifier l'évolution de l'espérance de vie

\small
\bigskip

\begin{itemize}
  \item Se renseigner sur la construction de l'indicateur espérance de vie \\ (ressources vidéo)
  \item Avec les données présentes sur la feuille \textbf{F5}, mettre en rapport l'année (x) et l'espérance de vie à la naissance en France (y)
  \item Mettre en place toute l'analyse: nuage de points, droite de régression, calcul du y estimé, calcul des résidus (année 2003), faire une phrase avec le coefficient de la régression
  \item Mettre en rapport année (x) et espérance de vie à la naissance au Kenya (y)
  \item Caractériser la relation (linéaire ?, monotone ?)
\end{itemize}

\medskip

\textbf{Question:} les résultats nuancent-ils les grandes lignes de la transition démographique? (quid du Kenya)

\normalsize

\end{frame}


% FRAME
\begin{frame}{Effets de structure et standardisation}

\textbf{Objectif 1: mettre en place une standardisation directe par l'âge} \\
\textbf{Objectif 2:} comprendre la multi-factorialité qui génère la mortalité

\small
\bigskip

\begin{itemize}
  \item Considérer les données de la feuille \textbf{F6}
  \item Calculer pour chaque département le taux brut de mortalité (nombre de décès total pour 1000 habitants) et cartographier ce taux
  \item Cartographier le taux de mortalité spécifique pour les 0-4 ans et pour les 80-84 ans
  \item Calculer les effectifs par âge pour la France entière
  \item Comparer dans un graphique la structure par âge de Seine Saint-Denis (93) avec celle de la France entière
  \item Comparer dans un graphique la structure par âge du Lot (46) avec celle de la France entière
\end{itemize}

\normalsize

\end{frame}



% FRAME
\begin{frame}{Effets de structure et standardisation}

\textbf{Objectif 1: mettre en place une standardisation directe par l'âge} \\
\textbf{Objectif 2:} comprendre la multi-factorialité qui génère la mortalité

\small
\bigskip

\begin{itemize}
  \item Nous considérons la population française totale comme \textbf{population-type}
  \item Multiplier la population de chaque tranche d'âge de la population-type par les taux de mortalité spécifique de chaque département et calculer ainsi le nombre de décès espérés dans chaque tranche pour chaque département
  \item Sommer les décès espérés pour calculer la mortalité totale espérée par département 
  \item Calculer pour chaque département les taux de mortalité standardisés en divisant les décès espérés par la population départementale
  \item Cartographie les taux de mortalité standardisés et comparer cette carte à la première carte des taux bruts de mortalité
\end{itemize}

\medskip

\textbf{Question:} il s'agit ici d'une standardisation de la mortalité par l'âge, imaginez d'autres types de standardisation (variable cible et variable de contrôle).

\normalsize

\end{frame}

% FRAME
\begin{frame}{Analyse multivariée}

\textbf{Objectif 1: faire une analyse factorielle et une classification} \\
\textbf{Objectif 2:} caractériser la transition démographique

\small
\bigskip

\begin{itemize}
  \item Faire une classification ascendante hiérarchique sur les variables démographiques de 1985
  \item Analyser et cartographier les résultats de cette analyse
  \item Faire une analyse en composantes principales sur les variables démographiques de 1985
  \item Analyser et cartographier les résultats de cette analyse
  \item Faire une classification ascendante hiérarchique sur les coordonnées factorielles issues de l'ACP 
  \item Analyser et cartographier les résultats de cette analyse (ACP-CAH) et les comparer à la CAH seule
\end{itemize}

\normalsize

\end{frame}

% FRAME
\begin{frame}{Analyse multivariée}

\textbf{Objectif 1: faire une analyse factorielle et une classification} \\
\textbf{Objectif 2:} caractériser la transition démographique

\small
\bigskip

\begin{itemize}
  \item Faire une analyse en composantes principales sur les variables démographiques de 2000
  \item Faire une classification ascendante hiérarchique sur les coordonnées factorielles issues de l'ACP 
  \item Analyser et cartographier les résultats de cette analyse (ACP-CAH) et les comparer à la CAH seule
\end{itemize}

\medskip

\textbf{Question:} en quoi la comparaison 1985-2000 permet de nuancer les grandes lignes de la transition démographique?

\normalsize

\end{frame}


% FRAME
\begin{frame}{Projet d'analyse}

\small
  
Poursuivre l'apprentissage des méthodes dans un projet personnel d'analyse de données de santé. \\

\bigskip

\textbf{Voir le cahier des charges du projet sur l'EPI.}

\end{frame}


\end{document}
